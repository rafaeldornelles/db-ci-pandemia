import { PacienteRepositorio } from "../persistencia/PacienteRepositorio";
import { Paciente } from "../entidades/Paciente";
import { ComorbidadeRepositorio } from "../persistencia/ComorbidadeRepositorio";
import { Comorbidade } from "../entidades/Comorbidade";

export class PacienteNegocios{
    static async listar(){
        return PacienteRepositorio.listar();
    }

    static async buscarPorId(id:string){
        return PacienteRepositorio.listarPorId(id);
    }

    static async buscarPorCpf(cpf:string){
        return PacienteRepositorio.listarPorCpf(cpf);
    }

    static async inserir(paciente:Paciente){
        return PacienteRepositorio.inserir(paciente);
    }

    static async atualizarDados(id:string, paciente:Paciente){
        return PacienteRepositorio.atualizar(id, paciente);
    }

    static async deletar(id:string){
        return PacienteRepositorio.deletar(id);
    }
}