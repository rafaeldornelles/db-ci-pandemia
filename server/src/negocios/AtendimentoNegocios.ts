import { AtendimentoRepositorio } from "../persistencia/AtendimentoRepositorio";
import { Atendimento } from "../entidades/Atendimento";
import { EtapaAtendimento } from "../entidades/EtapaAtendimento";
import { EtapaAtendimentoRepositorio } from "../persistencia/EtapaAtendimentoRepositorio";
import { etapaAtendimentoDocument } from "../persistencia/etapaAtendimentoModel";
import { atendimentoDocument } from "../persistencia/atendimentoModel";

export class AtendimentoNegocios{
    static async listar(){
        return AtendimentoRepositorio.listar();
    }

    static async buscarPorId(id:string){
        return AtendimentoRepositorio.listarPorId(id);
    }

    static async inserir(atendimento:Atendimento){
        return AtendimentoRepositorio.inserir(atendimento);
    }

    static async adicionarEtapa(idAtendimento:string, etapa: EtapaAtendimento){
        let atendimento = await AtendimentoRepositorio.listarPorId(idAtendimento);
        if(atendimento){
            if (atendimento.horarioInicio > etapa.horarioInicio) {
                throw new Error('Horário inválido')
            }
            if(atendimento.ativo === false){
                throw new Error('Atendimento encerrado')
            }
            
            let etapaCriada = await EtapaAtendimentoRepositorio.inserir(etapa);
            if(atendimento.etapas){
                atendimento.etapas.push(etapaCriada);
            }else{
                atendimento.etapas = [etapaCriada];
            }
            return AtendimentoRepositorio.atualizar(idAtendimento, atendimento);
        }else{
            throw new Error('id inexistente');
        }
    }

    static async removerEtapa(idEtapa:string){
        let etapa = await EtapaAtendimentoRepositorio.listarPorId(idEtapa) as etapaAtendimentoDocument;
        if(etapa){
            let atendimento = etapa.atendimento as atendimentoDocument;
            if(atendimento){
                atendimento.etapas = (atendimento.etapas as etapaAtendimentoDocument[]).filter(etp => etp._id.toString() !== etapa._id.toString());
                await AtendimentoRepositorio.atualizar(atendimento._id, atendimento);
                return EtapaAtendimentoRepositorio.deletar(idEtapa);
            }
        }
        throw new Error('id inexistente'); 
    }

    static async encerrarAtendimento(idAtendimento:string){ //marcar atendimento como inativo
        let atendimento = await AtendimentoRepositorio.listarPorId(idAtendimento);
        if(atendimento){
            if(atendimento.ativo == false){
                throw new Error("Atendimento já está inativo");
            }
            atendimento.ativo = false;
            atendimento.horarioFim = new Date(Date.now());
            return AtendimentoRepositorio.atualizar(idAtendimento, atendimento);
        }else{
            throw new Error('id inexistente');
        }
    }
    
    static async encerrarEtapaAtendimento(idEtapa:string){
        let etapa = await EtapaAtendimentoRepositorio.listarPorId(idEtapa);
        if(etapa){
            etapa.horarioFim = new Date(Date.now());
            return EtapaAtendimentoRepositorio.atualizar(idEtapa, etapa);
        }else{
            throw new Error('id inexistente');
        }
    }

    static async atendimentosPorDia(){
        return AtendimentoRepositorio.quantidadePorDia();
    }
}