import { TesteRepositorio } from "../persistencia/TesteRepositorio";
import { Teste } from "../entidades/Teste";
import { Caso } from "../entidades/Caso";
import { AtendimentoRepositorio } from "../persistencia/AtendimentoRepositorio";
import { PacienteRepositorio } from "../persistencia/PacienteRepositorio";
import { CasoRepositorio } from "../persistencia/CasoRepositorio";
import { casoDocument } from "../persistencia/casoModel";

export class TesteNegocios{
    static async listar(){
        return TesteRepositorio.listar();
    }

    static async buscarPorId(id:string){
        return TesteRepositorio.listarPorId(id);
    }

    static async listarPorPaciente(idPaciente:string){
        return TesteRepositorio.listarPorPaciente(idPaciente);
    }

    static async listarPorUnidade(idUnidade:string){
        return TesteRepositorio.listarPorUnidade(idUnidade);
    }

    static async listarPositivos(){
        return TesteRepositorio.listarPositivos();
    }

    static async inserir(teste:Teste){
        const idAtendimento = teste.atendimento.toString();
        const atendimento = await AtendimentoRepositorio.listarPorId(idAtendimento);
        if(atendimento){
            if (atendimento.testes.length >=2) throw new Error('máximo de testes por atendimento');
            const testeInserido = await TesteRepositorio.inserir(teste);
            atendimento.testes.push(testeInserido);
            AtendimentoRepositorio.atualizar(idAtendimento, atendimento);
            return testeInserido;
        }else{
            throw new Error('id inexistente');
        }
    }

    static async atualizar(id:string, teste:Teste){
        const testeAtualizado = await TesteRepositorio.atualizar(id, teste);
        const casoVerificar = await CasoRepositorio.buscarCasoPorTeste(id);
        if(casoVerificar == null && testeAtualizado.resultado == true){
            const paciente = await PacienteRepositorio.listarPorId(testeAtualizado.paciente.toString());
            if(!paciente) throw new Error('id desconhecido');
            const caso:Caso = {
                paciente: paciente,
                dataConfirmacao: new Date(),
                ativo: true,
                teste: testeAtualizado
            }
            CasoRepositorio.inserir(caso);
        }
        return testeAtualizado;
    }

    static async deletar(idTeste: string){
        const teste = await TesteRepositorio.listarPorId(idTeste);
        if(teste){
            const idAtendimento = teste.atendimento.toString();
            const atendimento = await AtendimentoRepositorio.listarPorId(idAtendimento);
            if(atendimento){
                atendimento.testes = atendimento.testes.filter(teste=> {
                    console.log(`teste != idTeste: ${teste.toString()!==idTeste}`);
                    return teste.toString() !== idTeste;
                })
                AtendimentoRepositorio.atualizar(idAtendimento, atendimento)
            }      
            //removendo casos que possam estar atrelados ao teste
            const caso = await CasoRepositorio.buscarCasoPorTeste(idTeste) as casoDocument;
            if (caso){
                CasoRepositorio.deletar(caso._id)
            }
            return TesteRepositorio.deletar(idTeste);
        }
        throw new Error('id inexistente');
    }

    static async testesPorDia(){
        return TesteRepositorio.testesPorDia();
    }
}