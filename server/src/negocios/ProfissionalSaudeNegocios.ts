import { ProfissionalSaudeRepositorio } from "../persistencia/ProfissionalSaudeRepositorio";
import { ProfissionalSaude } from "../entidades/ProfissionalSaude";

export class profissionalSaudeNegocios{
    static async listar(){
        return ProfissionalSaudeRepositorio.listar();
    }
    
    static async listarPorUnidadeSaude(idUnidade:string){
        return ProfissionalSaudeRepositorio.listarPorUnidadeSaude(idUnidade);
    }
    
    static async buscarPorId(id:string){
        return ProfissionalSaudeRepositorio.listarPorId(id);
    }

    static async buscarPorRegistroProfissional(registro:string){
        return ProfissionalSaudeRepositorio.listarPorRegistroProfissional(registro);
    }

    static async inserir(profissional:ProfissionalSaude){
        return ProfissionalSaudeRepositorio.inserir(profissional);
    }

    static async atualizar(id:string, profissional:ProfissionalSaude){
        return ProfissionalSaudeRepositorio.atualizar(id, profissional);
    }

    static async deletar(id:string){
        return ProfissionalSaudeRepositorio.deletar(id);
    }
}