import { ComorbidadeRepositorio } from "../persistencia/ComorbidadeRepositorio";
import { Comorbidade } from "../entidades/Comorbidade";

export class ComorbidadeNegocios{
    static async listar(){
        return ComorbidadeRepositorio.listar();
    }

    static async buscarPorId(id:string){
        return ComorbidadeRepositorio.listarPorId(id);
    }

    static async inserir(comorb: Comorbidade){
        return ComorbidadeRepositorio.inserir(comorb);
    }

    static async atualizar(id:string, comorb:Comorbidade){
        return ComorbidadeRepositorio.atualizar(id, comorb);
    }

    static async deletar(id:string){
        return ComorbidadeRepositorio.deletar(id);
    }
}