import { UnidadeSaudeRepositorio } from "../persistencia/UnidadeSaudeRepositorio";
import { UnidadeSaude } from "../entidades/UnidadeSaude";
import { AtendimentoRepositorio } from "../persistencia/AtendimentoRepositorio";
import { unidadeSaudeDocument } from "../persistencia/unidadeSaudeModel";

export class UnidadeSaudeNegocios{
    static async listar(){
        const unidades = await UnidadeSaudeRepositorio.listar();
        const promisesUnidadesComEstatisticas = unidades.map(async unidade=>{
            let unidadeObj:unidadeSaudeDocument = (unidade as unidadeSaudeDocument).toObject();
            const estatisticas = await this.getEstatisticasUnidade(unidade);
            if(estatisticas){
                unidadeObj.estatisticas = estatisticas;
            }
            const atendimentoUnidades = await AtendimentoRepositorio.listarPorUnidade(unidadeObj._id);
            const ontem = new Date(Date.now()-1000*60*60*24)
            const anteeOntem = new Date(Date.now()-100*60*600*24*2)
            unidadeObj.atendimentosCount = atendimentoUnidades.length
            unidadeObj.ultimoDiaCount = atendimentoUnidades
                .filter(a=> a.horarioInicio.toISOString().slice(0,10) == ontem.toISOString().slice(0,10))
                .length;
            unidadeObj.penultimoDiaCount = atendimentoUnidades
                .filter(a=> a.horarioInicio.toISOString().slice(0,10) == anteeOntem.toISOString().slice(0,10))
                .length;
            return unidadeObj as UnidadeSaude;
        });

        return Promise.all(promisesUnidadesComEstatisticas)
    }

    static async buscarPorId(id:string){
        const unidade = await UnidadeSaudeRepositorio.listarPorId(id);
        if(!unidade) throw new Error('id inexistente');
        const estatisticas = await this.getEstatisticasUnidade(unidade);
        if(estatisticas){
            unidade.estatisticas = estatisticas;
        }
        return Promise.resolve(unidade)
    }

    static async inserir(unidadeSaude: UnidadeSaude){
        return UnidadeSaudeRepositorio.inserir(unidadeSaude);
    }

    static async atualizar(id:string, unidadeSaude:UnidadeSaude){
        return UnidadeSaudeRepositorio.atualizar(id, unidadeSaude);
    }

    static async deletar(id:string){
        return UnidadeSaudeRepositorio.deletar(id);
    }

    static async getEstatisticasUnidade(unidade:UnidadeSaude){
        const atendimentos = await AtendimentoRepositorio.listarPorUnidade((unidade as unidadeSaudeDocument)._id);
        const atendimentosEncerrados= atendimentos.filter(atendimento=>atendimento.horarioFim);
        if (atendimentosEncerrados.length>0){
            let menorTempo:number;
            let maiorTempo:number;
            let somaTempos = 0;

            atendimentosEncerrados.forEach(atendimento=>{
                if (!atendimento.horarioFim) return;
                const tempoAtendimento = atendimento.horarioFim.getTime() - atendimento.horarioInicio.getTime();

                if (menorTempo === undefined && maiorTempo === undefined) {
                    menorTempo = maiorTempo = tempoAtendimento;
                }
                if(tempoAtendimento < menorTempo){
                    menorTempo = tempoAtendimento
                }
                if(tempoAtendimento>maiorTempo){
                    maiorTempo = tempoAtendimento
                }
                somaTempos += tempoAtendimento;
            });

            return {
                tempoMinimo: menorTempo!,
                tempoMaximo: maiorTempo!,
                tempoMedio: somaTempos/atendimentosEncerrados.length
            }

        }else{
            return null
        }
    }
}