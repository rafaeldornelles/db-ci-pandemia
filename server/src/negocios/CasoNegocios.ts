import { CasoRepositorio } from "../persistencia/CasoRepositorio";

export class CasoNegocios{
    static async listar(){
        return CasoRepositorio.listar();
    }
    
    static async buscarPorId(id:string){
        return CasoRepositorio.listarPorId(id)
    }

    static async buscarPorData(data:Date){
        return CasoRepositorio.listarPorData(data);
    }

    static async quantidadePorDia(){
        return CasoRepositorio.quantidadePorDia();
    }

    static async tornarInativo(id: string){
        const caso = await CasoRepositorio.listarPorId(id);
        if(caso){
            if(caso.ativo == false) throw new Error('Caso já inativo');
            caso.ativo = false;
            return CasoRepositorio.atualizar(id, caso);
        }else{
            throw new Error('id inexistente')
        }

    }

    static async deletar(id:string){
        return CasoRepositorio.deletar(id);
    }
    
    //Inserção será feita no testeNegocios quando um caso der positivo
}