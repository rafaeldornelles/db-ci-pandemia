import { GestorRepositorio } from "../persistencia/GestorRepositorio";
import { Gestor } from "../entidades/Gestor";

export class GestorNegocios{
    static async listar(){
        return GestorRepositorio.listar();
    }

    static async listarPorId(id:string){
        return GestorRepositorio.listarPorId(id);
    }

    static async inserir(gestor:Gestor){
        return GestorRepositorio.inserir(gestor);
    }

    static async atualizar(id:string, gestor:Gestor){
        return GestorRepositorio.atualizar(id, gestor);
    }

    static async deletar(id:string){
        return GestorRepositorio.deletar(id);
    }
}