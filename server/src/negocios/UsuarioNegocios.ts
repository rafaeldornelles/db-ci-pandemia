import { UsuarioRepositorio } from "../persistencia/UsuarioRepositorio";

export class UsuarioNegocios{
    static async listar(){
        return UsuarioRepositorio.listar();
    }

    static async buscarPorId(id:string){
        return UsuarioRepositorio.listarPorId(id);
    }

    static async buscarPorEmail(email:string){
        return UsuarioRepositorio.listarPorEmail(email);
    }

    static async deletar(id:string){
        return UsuarioRepositorio.deletar(id);
    }
}