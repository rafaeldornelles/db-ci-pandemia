import { Router } from 'express';
import { getGestores, getGestorPorId, inserirGestor, atualizarGestor, deletarGestor } from '../controllers/gestor.controller';

const gestorRouter = Router();

gestorRouter.get('', getGestores);
gestorRouter.get('/:id', getGestorPorId);
gestorRouter.post('', inserirGestor);
gestorRouter.put('/:id', atualizarGestor);
gestorRouter.delete('/:id', deletarGestor);

export default gestorRouter;