import { Router } from 'express';
import { listarUnidades, buscarUnidadePorId, atualizarUnidade, deletarUnidade, inserirUnidade } from '../controllers/unidadeSaude.controller';

const unidadeSaudeRouter = Router();

unidadeSaudeRouter.get('', listarUnidades);
unidadeSaudeRouter.get('/:id', buscarUnidadePorId);
unidadeSaudeRouter.post('', inserirUnidade);
unidadeSaudeRouter.put('/:id', atualizarUnidade);
unidadeSaudeRouter.delete('/:id', deletarUnidade);

export default unidadeSaudeRouter;