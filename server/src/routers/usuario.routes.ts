import { Router } from 'express';
import { getUsuarios, getUsuarioPorId, getUsuarioPorEmail, removerUsuario } from '../controllers/usuario.controller';

const usuarioRouter = Router();

usuarioRouter.get('', getUsuarios); //query ?e=:email para getByEmail
usuarioRouter.get('/:id', getUsuarioPorId);
usuarioRouter.delete('/:id', removerUsuario);

export default usuarioRouter;