import { Router } from 'express';
import { getAtendimentos, getAtendimentoPorId, cadastrarAtendimento, cadastrarEtapa, removerEtapa, encerrarAtendimento, encerrarEtapa, atendimentosPorDia } from '../controllers/atendimento.controller';

const atendimentoRouter = Router();
atendimentoRouter.get('', getAtendimentos);
atendimentoRouter.get('/relatorio', atendimentosPorDia);
atendimentoRouter.get('/:id', getAtendimentoPorId);
atendimentoRouter.post('', cadastrarAtendimento);
atendimentoRouter.post('/etapa', cadastrarEtapa);
atendimentoRouter.delete('/etapa/:idEtapa', removerEtapa);
atendimentoRouter.put('/:id/encerrar', encerrarAtendimento);
atendimentoRouter.put('/etapa/:id/encerrar', encerrarEtapa);

export default atendimentoRouter;