import { Router } from 'express';
import { getComorbidades, getComorbidadePorId, inserirComorbidade, atualizarComorbidade, removerComorbidade } from '../controllers/comorbidade.controller';

const comorbidadeRouter = Router();
comorbidadeRouter.get('', getComorbidades);
comorbidadeRouter.get('/:id', getComorbidadePorId);
comorbidadeRouter.post('', inserirComorbidade);
comorbidadeRouter.put('/:id', atualizarComorbidade);
comorbidadeRouter.delete('/:id', removerComorbidade);

export default comorbidadeRouter;