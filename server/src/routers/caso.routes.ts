import { Router } from "express"
import { listarCasos, buscarCasoPorId, buscarCasoPorData, quantidadeCasosPorDia, tornarCasoInatvo, deletarCaso } from "../controllers/caso.controller";

const casoRouter = Router();

casoRouter.get('', listarCasos);
casoRouter.get('/relatorio', quantidadeCasosPorDia);
casoRouter.get('/:id', buscarCasoPorId);
casoRouter.get('/data/:data', buscarCasoPorData);
casoRouter.put('/encerrar/:id', tornarCasoInatvo);
casoRouter.delete('/:id', deletarCaso);

export default casoRouter;
