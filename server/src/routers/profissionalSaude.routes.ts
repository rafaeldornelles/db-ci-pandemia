import { Router } from 'express';
import { getProfissionaisSaude, getProfissionaisPorUnidade, getProfissionalPorId, getProfissionaisPorRegistro, inserirProfissionalSaude, atualizarProfissionalSaude, deletarProfissionalSaude } from '../controllers/profissionalSaude.controller';

const profissionalSaudeRouter = Router();

profissionalSaudeRouter.get('', getProfissionaisSaude);
profissionalSaudeRouter.get('/unidade/:id', getProfissionaisPorUnidade);
profissionalSaudeRouter.get('/:id', getProfissionalPorId);
profissionalSaudeRouter.get('/reg/:reg', getProfissionaisPorRegistro);
profissionalSaudeRouter.post('', inserirProfissionalSaude);
profissionalSaudeRouter.put('/:id', atualizarProfissionalSaude);
profissionalSaudeRouter.delete('/:id', deletarProfissionalSaude);

export default profissionalSaudeRouter;