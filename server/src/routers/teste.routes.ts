import { Router } from "express";
import { listarTestes, buscarTestesPorId, listarTestesPorPaciente, listarTestesPorUnidade, listarTestesPositivos, inserirTeste, atualizarTeste, deletarTeste, testesPorDia } from "../controllers/teste.controller";

const testeRouter = Router();

testeRouter.get('', listarTestes);
testeRouter.get('/positivos', listarTestesPositivos);
testeRouter.get('/relatorio', testesPorDia);
testeRouter.get('/:id', buscarTestesPorId);
testeRouter.get('/paciente/:id', listarTestesPorPaciente);
testeRouter.get('/unidade/:id', listarTestesPorUnidade);
testeRouter.post('', inserirTeste);
testeRouter.put('/:id', atualizarTeste);
testeRouter.delete('/:id', deletarTeste);

export default testeRouter;