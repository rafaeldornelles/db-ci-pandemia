import { Router } from 'express';
import { getPacientes, getPacientesPorId, getPacientesPorCpf, inserirPaciente, atualizarPaciente, deletarPaciente } from '../controllers/paciente.controller';

const pacienteRouter = Router();

pacienteRouter.get('', getPacientes);
pacienteRouter.get('/:id', getPacientesPorId);
pacienteRouter.get('/cpf/:cpf', getPacientesPorCpf);
pacienteRouter.post('', inserirPaciente);
pacienteRouter.put('/:id', atualizarPaciente);
pacienteRouter.delete('/:id', deletarPaciente);

export default pacienteRouter;