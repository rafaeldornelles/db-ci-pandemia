import { Request, Response, NextFunction } from "express";
import { UnidadeSaudeNegocios } from "../negocios/UnidadeSaudeNegocios";
import { UnidadeSaude } from "../entidades/UnidadeSaude";

export async function listarUnidades(req:Request, res:Response, next:NextFunction){
    try{
        const unidades = await UnidadeSaudeNegocios.listar();
        return res.json(unidades);
    }catch(e){
        next(e);
    }
}

export async function buscarUnidadePorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const unidade = await UnidadeSaudeNegocios.buscarPorId(id);
        return res.json(unidade);
    }catch(e){
        next(e);
    }
}

export async function inserirUnidade(req:Request, res:Response, next:NextFunction){
    try{
        const unidade = req.body as UnidadeSaude;
        const unidadeCriada = await UnidadeSaudeNegocios.inserir(unidade);
        return res.status(201).json(unidadeCriada);
    }catch(e){
        next(e);
    }
}

export async function atualizarUnidade(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const unidade = req.body as UnidadeSaude;
        const unidadeAlterada = await UnidadeSaudeNegocios.atualizar(id, unidade);
        return res.json(unidadeAlterada);
    }catch(e){
        next(e);
    }
}

export async function deletarUnidade(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const unidadeDeletada = await UnidadeSaudeNegocios.deletar(id);
        return res.json(unidadeDeletada);
    }catch(e){
        next(e);
    }
}
