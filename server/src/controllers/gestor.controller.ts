import { Request, Response, NextFunction } from "express";
import { GestorNegocios } from "../negocios/GestorNegocios";
import { Gestor } from "../entidades/Gestor";

export async function getGestores(req:Request, res:Response, next:NextFunction){
    try{
        const gestores = await GestorNegocios.listar();
        return res.json(gestores);
    }catch(e){
        next(e);
    }
}

export async function getGestorPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const gestor = await GestorNegocios.listarPorId(id);
        if(gestor){
            return res.json(gestor);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function inserirGestor(req:Request, res:Response, next:NextFunction){
    try{
        const gestor = req.body as Gestor;
        const gestorCriado = await GestorNegocios.inserir(gestor);
        return res.status(201).json(gestorCriado);
    }catch(e){
        next(e);
    }
}

export async function atualizarGestor(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const gestor = req.body as Gestor;
        const gestorAlterado = await GestorNegocios.atualizar(id, gestor);
        return res.json(gestorAlterado);
    }catch(e){
        next(e);
    }
}

export async function deletarGestor(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const gestorDeletado = await GestorNegocios.deletar(id);
        return res.json(gestorDeletado);
    }catch(e){
        next(e);
    }
}
