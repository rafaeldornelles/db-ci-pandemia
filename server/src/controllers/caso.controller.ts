import { Request, Response, NextFunction } from "express";
import { CasoNegocios } from "../negocios/CasoNegocios";

export async function listarCasos(req:Request, res:Response, next:NextFunction){
    try{
        const casos = await CasoNegocios.listar();
        return res.json(casos);
    }catch(e){
        next(e);
    }
}

export async function buscarCasoPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id
        const caso = await CasoNegocios.buscarPorId(id);
        if(caso){
            return res.json(caso);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function buscarCasoPorData(req:Request, res:Response, next:NextFunction){
    try{
        const dataString = req.params.data + "T00:00:00";
        const data = new Date(dataString);
        const casos = await CasoNegocios.buscarPorData(data);
        return res.json(casos);
    }catch(e){
        next(e);
    }
}

export async function quantidadeCasosPorDia(req:Request, res:Response, next:NextFunction){
    try{
        const relatorio = await CasoNegocios.quantidadePorDia();
        return res.json(relatorio);
    }catch(e){
        next(e);
    }
}

export async function tornarCasoInatvo(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id
        const casos = await CasoNegocios.tornarInativo(id);
        return res.json(casos);
    }catch(e){
        next(e);
    }
}

export async function deletarCaso(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const casos = await CasoNegocios.deletar(id);
        return res.json(casos);
    }catch(e){
        next(e);
    }
}