import { Response, Request, NextFunction } from "express";
import { TesteNegocios } from "../negocios/TesteNegocios";
import { Teste } from "../entidades/Teste";

export async function listarTestes(req:Request, res:Response, next:NextFunction){
    try{
        const testes = await TesteNegocios.listar();
        return res.json(testes);
    }catch(e){
        next(e);
    }
}

export async function buscarTestesPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const teste = await TesteNegocios.buscarPorId(id);
        if(teste){
            return res.json(teste);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function listarTestesPorPaciente(req:Request, res:Response, next:NextFunction){
    try{
        const idPaciente = req.params.id;
        const testes = await TesteNegocios.listarPorPaciente(idPaciente);
        return res.json(testes);
    }catch(e){
        next(e);
    }
}

export async function listarTestesPorUnidade(req:Request, res:Response, next:NextFunction){
    try{
        const idUnidade = req.params.id;
        const testes = await TesteNegocios.listarPorUnidade(idUnidade);
        return res.json(testes);
    }catch(e){
        next(e);
    }
}

export async function listarTestesPositivos(req:Request, res:Response, next:NextFunction){
    try{
        const testes = await TesteNegocios.listarPositivos();
        return res.json(testes);
    }catch(e){
        next(e);
    }
}

export async function inserirTeste(req:Request, res:Response, next:NextFunction){
    try{
        const teste = req.body as Teste;
        const testeInserido = await TesteNegocios.inserir(teste);
        return res.json(testeInserido);
    }catch(e){
        next(e);
    }
}

export async function atualizarTeste(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const teste = req.body as Teste;
        const testeAtualizado = await TesteNegocios.atualizar(id, teste);
        return res.json(testeAtualizado);
    }catch(e){
        next(e);
    }
}

export async function deletarTeste(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const testeDeletado = await TesteNegocios.deletar(id);
        return res.json(testeDeletado);
    }catch(e){
        next(e);
    }
}

export async function testesPorDia(req:Request, res:Response, next:NextFunction){
    try{
        const relatorio = await TesteNegocios.testesPorDia();
        return res.json(relatorio);
    }catch(e){
        next(e);
    }
}

