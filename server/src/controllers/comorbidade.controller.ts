import { Request, Response, NextFunction } from "express";
import { ComorbidadeNegocios } from "../negocios/ComorbidadeNegocios";
import { Comorbidade } from "../entidades/Comorbidade";

export async function getComorbidades(req:Request, res:Response, next:NextFunction){
    try{
        const comorbidades = await ComorbidadeNegocios.listar();
        return res.json(comorbidades);
    }catch(e){
        next(e);
    }
}

export async function getComorbidadePorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const comorbidade = await ComorbidadeNegocios.buscarPorId(id);
        if(comorbidade){
            return res.json(comorbidade);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function inserirComorbidade(req:Request, res:Response, next:NextFunction){
    try{
        const comorbidade = req.body as Comorbidade;
        const comorbidadeCriada = await ComorbidadeNegocios.inserir(comorbidade);
        return res.status(201).json(comorbidadeCriada);
    }catch(e){
        next(e);
    }
}

export async function atualizarComorbidade(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const comorbidade = req.body as Comorbidade;
        const comorbidadeAlterada = await ComorbidadeNegocios.atualizar(id, comorbidade);
        return res.json(comorbidadeAlterada);
    }catch(e){
        next(e);
    }
}

export async function removerComorbidade(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const comorbidade = await ComorbidadeNegocios.deletar(id);
        return res.json(comorbidade);
    }catch(e){
        next(e);
    }
}