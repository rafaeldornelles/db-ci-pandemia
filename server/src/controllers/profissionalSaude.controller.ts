import { Response, Request, NextFunction } from "express";
import { profissionalSaudeNegocios } from "../negocios/ProfissionalSaudeNegocios";
import { ProfissionalSaude } from "../entidades/ProfissionalSaude";

export async function getProfissionaisSaude(req:Request, res:Response, next:NextFunction){
    try{
        const profissionais = await profissionalSaudeNegocios.listar();
        return res.json(profissionais);
    }catch(e){
        next(e);
    }
}

export async function getProfissionaisPorUnidade(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const profissionais = await profissionalSaudeNegocios.listarPorUnidadeSaude(id);
        return res.json(profissionais);
    }catch(e){
        next(e);
    }
}

export async function getProfissionalPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const profissional = await profissionalSaudeNegocios.buscarPorId(id);
        if(profissional){
            return res.json(profissional);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function getProfissionaisPorRegistro(req:Request, res:Response, next:NextFunction){
    try{
        const registro = req.params.reg;
        const profissional = await profissionalSaudeNegocios.buscarPorRegistroProfissional(registro);
        if(profissional){
            return res.json(profissional);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function inserirProfissionalSaude(req:Request, res:Response, next:NextFunction){
    try{
        const profissionalSaude = req.body as ProfissionalSaude;
        const profissionalCriado = await profissionalSaudeNegocios.inserir(profissionalSaude);
        return res.status(201).json(profissionalCriado);
    }catch(e){
        next(e);
    }
}

export async function atualizarProfissionalSaude(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const profissional = req.body as ProfissionalSaude;
        const profissionalAtualizado = await profissionalSaudeNegocios.atualizar(id, profissional);
        return res.json(profissionalAtualizado)
    }catch(e){
        next(e);
    }
}

export async function deletarProfissionalSaude(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const profissionalDeletado = await profissionalSaudeNegocios.deletar(id);
        return res.json(profissionalDeletado);
    }catch(e){
        next(e);
    }
}
