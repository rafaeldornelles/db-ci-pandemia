import { Request, Response, NextFunction } from "express";
import { AtendimentoNegocios} from "../negocios/AtendimentoNegocios"
import { Atendimento } from "../entidades/Atendimento";
import { EtapaAtendimento } from "../entidades/EtapaAtendimento";

export async function getAtendimentos(req:Request, res:Response, next:NextFunction){
    try{
        const atendimentos = await AtendimentoNegocios.listar();
        return res.json(atendimentos)
    }catch(e){
        next(e);
    }
}

export async function getAtendimentoPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const atendimento = await AtendimentoNegocios.buscarPorId(id);
        if(atendimento){
            return res.json(atendimento);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function cadastrarAtendimento(req:Request, res:Response, next:NextFunction){
    try{
        const atendimento = req.body as Atendimento;
        const atendimentoResult = await AtendimentoNegocios.inserir(atendimento);
        return res.status(201).json(atendimentoResult);
    }catch(e){
        next(e);
    }
}

export async function cadastrarEtapa(req:Request, res:Response, next:NextFunction){
    try{
        const idAtendimento = req.body.atendimento;
        const etapa = req.body as EtapaAtendimento;
        const etapaResult = await AtendimentoNegocios.adicionarEtapa(idAtendimento, etapa);
        return res.status(201).json(etapaResult);
    }catch(e){
        next(e);
    }
}

export async function removerEtapa(req:Request, res:Response, next:NextFunction){
    try{
        const idEtapa = req.params.idEtapa;
        const idAtendimento = req.params.idAtendimento;
        const atendimentoResult = await AtendimentoNegocios.removerEtapa(idEtapa);
        return res.json(atendimentoResult);
    }catch(e){
        next(e);
    }
}

export async function encerrarAtendimento(req:Request, res:Response, next:NextFunction){
    try{
        const idAtendimento = req.params.id;
        const atendimentoResult = await AtendimentoNegocios.encerrarAtendimento(idAtendimento);
        return res.json(atendimentoResult);
    }catch(e){
        next(e);
    }
}

export async function encerrarEtapa(req:Request, res:Response, next:NextFunction){
    try{
        const idEtapa = req.params.id;
        const etapaResult = await AtendimentoNegocios.encerrarEtapaAtendimento(idEtapa);
        return res.json(etapaResult)
    }catch(e){
        next(e);
    }
}

export async function atendimentosPorDia(req:Request, res:Response, next:NextFunction){
    try{
        const relatorio = await AtendimentoNegocios.atendimentosPorDia();
        return res.json(relatorio)
    }catch(e){
        next(e);
    }
}