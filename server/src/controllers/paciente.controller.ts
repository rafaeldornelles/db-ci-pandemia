import { Request, Response, NextFunction } from "express";
import { PacienteRepositorio } from "../persistencia/PacienteRepositorio";
import { PacienteNegocios } from "../negocios/PacienteNegocios";
import { Paciente } from "../entidades/Paciente";
import { Comorbidade } from "../entidades/Comorbidade";

export async function getPacientes(req:Request, res:Response, next:NextFunction){
    try{
        const pacientes = await PacienteNegocios.listar();
        return res.json(pacientes);
    }catch(e){
        next(e);
    }
}

export async function getPacientesPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const paciente = await PacienteNegocios.buscarPorId(id);
        if(paciente){
            return res.json(paciente);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function getPacientesPorCpf(req:Request, res:Response, next:NextFunction){
    try{
        const cpf = req.params.cpf;
        const paciente = await PacienteNegocios.buscarPorCpf(cpf);
        if(paciente){
            return res.json(paciente);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function inserirPaciente(req:Request, res:Response, next:NextFunction){
    try{
        const paciente = req.body as Paciente;
        const pacienteCriado = await PacienteNegocios.inserir(paciente);
        return res.status(201).json(pacienteCriado);
    }catch(e){
        next(e);
    }
}

export async function atualizarPaciente(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const paciente = req.body as Paciente;
        const pacienteAlterado = await PacienteNegocios.atualizarDados(id, paciente);
        return res.json(pacienteAlterado);
    }catch(e){
        next(e);
    }
}

export async function deletarPaciente(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const pacienteRemovido = await PacienteNegocios.deletar(id);
        return res.json(pacienteRemovido);
    }catch(e){
        next(e);
    }
}