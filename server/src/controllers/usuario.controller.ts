import { Request, Response, NextFunction, query } from "express";
import { UsuarioNegocios } from "../negocios/UsuarioNegocios";

export async function getUsuarios(req:Request, res:Response, next:NextFunction){
    try{
        if(req.query.e){
            return getUsuarioPorEmail(req, res, next)
        }
        const usuarios = await UsuarioNegocios.listar();
        return res.json(usuarios);
    }catch(e){
        next(e);
    }
}

export async function getUsuarioPorId(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const usuario = await UsuarioNegocios.buscarPorId(id)
        if(usuario){
            return res.json(usuario);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function getUsuarioPorEmail(req:Request, res:Response, next:NextFunction){
    try{
        const email = req.query.e as string;
        const usuario = await UsuarioNegocios.buscarPorEmail(email);
        if(usuario){
            return res.json(usuario);
        }else{
            return res.sendStatus(404);
        }
    }catch(e){
        next(e);
    }
}

export async function removerUsuario(req:Request, res:Response, next:NextFunction){
    try{
        const id = req.params.id;
        const usuario = await UsuarioNegocios.deletar(id);
        return res.json(usuario);
    }catch(e){
        next(e);
    }
}

