import { Gestor } from "../../entidades/Gestor";
import { GestorRepositorio } from "../../persistencia/GestorRepositorio";
import { GestorNegocios } from "../../negocios/GestorNegocios";

const mockGestores:Gestor[] = [
    {
        nome:'Unidade 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        credencial: '1111'
    },
    {
        nome:'Unidade 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        credencial:'1112'
    },
    {
        nome:'Unidade 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        credencial:'1113'
    },
];

describe('GestorNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar GestorRepositorio.listar()', async()=>{
            const mockGestorRepositorioListar = jest
                .spyOn(GestorRepositorio, 'listar')
                .mockResolvedValue(mockGestores);
            const result = await GestorNegocios.listar();
            expect(mockGestorRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockGestores)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar GestorRepositorio.buscarPorId()', async()=>{
            const mockGestorRepositorioListarPorId = jest
                .spyOn(GestorRepositorio, 'listarPorId')
                .mockResolvedValue(mockGestores[0]);
            const result = await GestorNegocios.listarPorId('id')
            expect(mockGestorRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockGestores[0]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar GestorRepositorio.inserir()', async()=>{
            const mockGestorRepositorioInserir = jest
                .spyOn(GestorRepositorio, 'inserir')
                .mockImplementation((g:Gestor)=>Promise.resolve(g));
            const result = await GestorNegocios.inserir(mockGestores[0]);
            expect(mockGestorRepositorioInserir).toHaveBeenCalledWith(mockGestores[0]);
            expect(result).toEqual(mockGestores[0]);
        });
    });
    describe('atualizar()',()=>{
        test('Deve retornar GestorRepositorio.atualizar()', async()=>{
            const mockGestorRepositorioAtualizar = jest
                .spyOn(GestorRepositorio, 'atualizar')
                .mockImplementation((id:string, g:Gestor)=>Promise.resolve(g));
            const result = await GestorNegocios.atualizar('id', mockGestores[0]);
            expect(mockGestorRepositorioAtualizar).toHaveBeenCalledWith('id', mockGestores[0]);
            expect(result).toEqual(mockGestores[0]);
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar GestorRepositorio.deletar()', async()=>{
            const mockGestorRepositorioAtualizar = jest
                .spyOn(GestorRepositorio, 'deletar')
                .mockResolvedValue(mockGestores[0]);
            const result = await GestorNegocios.deletar('id');
            expect(mockGestorRepositorioAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockGestores[0]);
        });
    });
});