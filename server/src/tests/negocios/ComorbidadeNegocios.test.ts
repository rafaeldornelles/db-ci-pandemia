import { Comorbidade } from "../../entidades/Comorbidade"
import { ComorbidadeRepositorio } from "../../persistencia/ComorbidadeRepositorio"
import { ComorbidadeNegocios } from "../../negocios/ComorbidadeNegocios";

const mockComorbidades:Comorbidade[] = [
    {
        descricao:'Com 1',
        cid: 'CID 1'
    },
    {
        descricao:'Com 1',
        cid: 'CID 1'
    }
]

describe('ComorbidadeNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar ComorbidadeRepositorio.listar()', async()=>{
            const mockComorbidadeRepositorioListar = jest
                .spyOn(ComorbidadeRepositorio, 'listar')
                .mockResolvedValue(mockComorbidades);
            const result = await ComorbidadeNegocios.listar();
            expect(mockComorbidadeRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockComorbidades)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar ComorbidadeRepositorio.buscarPorId()', async()=>{
            const mockComorbidadeRepositorioListarPorId = jest
                .spyOn(ComorbidadeRepositorio, 'listarPorId')
                .mockResolvedValue(mockComorbidades[0]);
            const result = await ComorbidadeNegocios.buscarPorId('id')
            expect(mockComorbidadeRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockComorbidades[0]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar comorbidadeRepositorio.inserir()', async()=>{
            const mockComorbidadeRepositorioInserir = jest
                .spyOn(ComorbidadeRepositorio, 'inserir')
                .mockImplementation((c:Comorbidade)=>Promise.resolve(c));
            const result = await ComorbidadeNegocios.inserir(mockComorbidades[0]);
            expect(mockComorbidadeRepositorioInserir).toHaveBeenCalledWith(mockComorbidades[0]);
            expect(result).toEqual(mockComorbidades[0]);
        });
    });
    describe('atualizar()',()=>{
        test('Deve retornar comorbidadeRepositorio.atualizar()', async()=>{
            const mockComorbidadeRepositorioAtualizar = jest
                .spyOn(ComorbidadeRepositorio, 'atualizar')
                .mockImplementation((id:string, c:Comorbidade)=>Promise.resolve(c));
            const result = await ComorbidadeNegocios.atualizar('id', mockComorbidades[0]);
            expect(mockComorbidadeRepositorioAtualizar).toHaveBeenCalledWith('id', mockComorbidades[0]);
            expect(result).toEqual(mockComorbidades[0]);
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar comorbidadeRepositorio.deletar()', async()=>{
            const mockComorbidadeRepositorioAtualizar = jest
                .spyOn(ComorbidadeRepositorio, 'deletar')
                .mockResolvedValue(mockComorbidades[0]);
            const result = await ComorbidadeNegocios.deletar('id');
            expect(mockComorbidadeRepositorioAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockComorbidades[0]);
        });
    });
});