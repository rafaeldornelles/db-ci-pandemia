import { Atendimento } from "../../entidades/Atendimento";
import { AtendimentoRepositorio } from "../../persistencia/AtendimentoRepositorio";
import { AtendimentoNegocios } from "../../negocios/AtendimentoNegocios";
import { EtapaAtendimento } from "../../entidades/EtapaAtendimento";
import { EtapaAtendimentoRepositorio } from "../../persistencia/EtapaAtendimentoRepositorio";
import { etapaAtendimentoModel } from "../../persistencia/etapaAtendimentoModel";
import { atendimentoModel } from "../../persistencia/atendimentoModel";

const mockAtendimentos:Atendimento[] = [
    {
        ativo:true,
        //@ts-ignore
        horarioInicio: '2020-01-01T12:00:00.000Z',
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        testes: []
    },
    {
        ativo:false,
        //@ts-ignore
        horarioInicio: '2020-01-01T12:00:00.000Z',
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
];

const mockAtendimentoComEtapa:Atendimento = {
    ativo:true,
    //@ts-ignore
    horarioInicio: '2020-01-01T12:00:00.000Z',
    //@ts-ignore
    paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    etapas: [{
        //@ts-ignore
        _id:'idEtapa'
    }],
    testes: []
}

const mockRelatorio = [
    {
        _id:'2020-01-01',
        count:3
    },
    {
        _id:'2020-01-02',
        count:4
    },
];

const mockEtapa:EtapaAtendimento = {
    //@ts-ignore
    _id: 'idEtapa',
    //@ts-ignore
    horarioInicio: '2020-01-01T12:30:00.000Z',
    //@ts-ignore
    atendimento: mockAtendimentoComEtapa,
    //@ts-ignore
    profissional: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    descricao:'etapa ...'
}

const mockEtapaHorarioInvalido:EtapaAtendimento = {
    //@ts-ignore
    horarioInicio: '2020-01-01T10:00:00.000Z',
    //@ts-ignore
    atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    profissional: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    descricao:'etapa ...'  
}

describe('AtendimentoNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar AtendimentoRepositorio.listar()', async()=>{
            const mockAtendimentoRepositorioListar = jest
                .spyOn(AtendimentoRepositorio, 'listar')
                .mockResolvedValue(mockAtendimentos);
            const result = await AtendimentoNegocios.listar();
            expect(mockAtendimentoRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockAtendimentos)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar AtendimentoRepositorio.buscarPorId()', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const result = await AtendimentoNegocios.buscarPorId('id')
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockAtendimentos[0]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar AtendimentoRepositorio.inserir()', async()=>{
            const mockAtendimentoRepositorioInserir = jest
                .spyOn(AtendimentoRepositorio, 'inserir')
                .mockImplementation((a:Atendimento)=>Promise.resolve(a));
            const result = await AtendimentoNegocios.inserir(mockAtendimentos[0]);
            expect(mockAtendimentoRepositorioInserir).toHaveBeenCalledWith(mockAtendimentos[0]);
            expect(result).toEqual(mockAtendimentos[0]);
        });
    });
   describe('adicionarEtapa()', ()=>{
       test('Deve fazer verificações e adicionar primeira etapa', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const mockEtapaRepositorioInserir = jest
                .spyOn(EtapaAtendimentoRepositorio, 'inserir')
                .mockImplementation((e:EtapaAtendimento)=>Promise.resolve(e));
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=>Promise.resolve(a));
            const result = await AtendimentoNegocios.adicionarEtapa('id',mockEtapa);
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockEtapaRepositorioInserir).toHaveBeenCalledWith(mockEtapa);
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(1);
            expect(mockAtendimentos[0].etapas).toHaveLength(1);
            expect(result).toEqual(mockAtendimentos[0])
       });
       test('Deve fazer verificações e adicionar segunda etapa', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const mockEtapaRepositorioInserir = jest
                .spyOn(EtapaAtendimentoRepositorio, 'inserir')
                .mockImplementation((e:EtapaAtendimento)=>Promise.resolve(e));
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=>Promise.resolve(a));
            const result = await AtendimentoNegocios.adicionarEtapa('id',mockEtapa);
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockEtapaRepositorioInserir).toHaveBeenCalledWith(mockEtapa);
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(2);
            expect(mockAtendimentos[0].etapas).toHaveLength(2);
            expect(result).toEqual(mockAtendimentos[0])
       });
       test('Deve apresentar erro de atendimento encerrado', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[1]);
            const atualizacao = async()=>{await AtendimentoNegocios.adicionarEtapa('id', mockEtapa)}
            expect(atualizacao).rejects.toThrow('Atendimento encerrado');
       });
       test('Deve apresentar erro de horário inválido', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[1]);
            const atualizacao = async()=>{await AtendimentoNegocios.adicionarEtapa('id', mockEtapaHorarioInvalido)}
            expect(atualizacao).rejects.toThrow('Horário inválido');
       });
       test('Deve apresentar erro de id inexistente', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const atualizacao = async()=>{await AtendimentoNegocios.adicionarEtapa('id', mockEtapa)}
            expect(atualizacao).rejects.toThrow('id inexistente');
       });
   });
   describe('removerEtapa()', ()=>{
       test('Deve remover uma etapa e remover ela dos atendimentos', async()=>{
            const mockEtapaRepositorioListarPorId = jest
                .spyOn(EtapaAtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockEtapa);
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=>Promise.resolve(a))
                .mockReset();
            const mockEtapaRepositorioDeletar= jest
                .spyOn(EtapaAtendimentoRepositorio, 'deletar')
                .mockResolvedValue(mockEtapa);
            const result = await AtendimentoNegocios.removerEtapa('idEtapa');
            expect(mockEtapaRepositorioListarPorId).toHaveBeenCalledWith('idEtapa');
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(1);
            expect(mockEtapaRepositorioDeletar).toHaveBeenCalledTimes(1);
            expect(mockAtendimentoComEtapa.etapas).toHaveLength(0);      
       });
       test('Deve apresentar erro id inexistente', async()=>{
            const mockEtapaRepositorioListarPorId = jest
                .spyOn(EtapaAtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const remocao = async()=>{await AtendimentoNegocios.removerEtapa('id')}
            expect(remocao).rejects.toThrow('id inexistente');
       });
   });
   describe('encerrarAtendimento', ()=>{
       test('deve encerrar um atendimento e atribuir um horario de fim', async()=>{
           const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=>Promise.resolve(a))
                .mockReset();
            const mockData = new Date(2020, 0, 1).getTime();
            const mockDate = jest.spyOn(global.Date, 'now').mockImplementation(()=>mockData);
            const result = await AtendimentoNegocios.encerrarAtendimento('id');
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(1);
            expect(mockAtendimentos[0].ativo).toEqual(false);
            expect(mockAtendimentos[0].horarioFim?.getTime()).toEqual(mockData);
       });
       test('deve apresentar erro atendimento já inativo', async()=>{
           const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const atualizacao = async()=>{await AtendimentoNegocios.encerrarAtendimento('id')};
            expect(atualizacao).rejects.toThrow('Atendimento já está inativo')
       });
       test('deve apresentar erro de id inexistente', async()=>{
           const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const atualizacao = async ()=>{await AtendimentoNegocios.encerrarAtendimento('id')};
            expect(atualizacao).rejects.toThrow();
        });
   });
   describe('EncerrarEtapaAtendimento', ()=>{
       test('Deve atualizar etapa de atendimento e atribuir horário de fim', async()=>{
           const mockEtpaAtendimentoListarPorId = jest
                .spyOn(EtapaAtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockEtapa);
            const now = Date.now()
            const dateMock = jest
                .spyOn(global.Date, 'now')
                .mockImplementation(()=>now);
            const mockEtapaRepositiorioAtualizar = jest
                .spyOn(EtapaAtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, e:EtapaAtendimento)=>Promise.resolve(e));
            const result = await AtendimentoNegocios.encerrarEtapaAtendimento('id');
            expect(mockEtpaAtendimentoListarPorId).toHaveBeenCalledWith('id')
            expect(mockEtapaRepositiorioAtualizar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockEtapa);
            expect(mockEtapa.horarioFim?.getTime()).toEqual(now)
       });
       test('Deve apresentar erro id inexistente', async()=>{
            const mockEtpaAtendimentoListarPorId = jest
                .spyOn(EtapaAtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const atualizacao = async()=>{await AtendimentoNegocios.encerrarEtapaAtendimento('id')}
            expect(atualizacao).rejects.toThrow();
       });
   });
   describe('atendimentosPorDia()', ()=>{
       test('Deve retornar relatorio', async()=>{
           const mockAtendimentoRepositorioRelatorio = jest
               .spyOn(AtendimentoRepositorio, 'quantidadePorDia')
               .mockResolvedValue(mockRelatorio)
           const result = await AtendimentoNegocios.atendimentosPorDia();
           expect(result).toEqual(mockRelatorio);
           expect(mockAtendimentoRepositorioRelatorio).toHaveBeenCalledTimes(1)
       });
   })
});