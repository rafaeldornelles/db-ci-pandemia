import { Teste } from "../../entidades/Teste";
import { TesteRepositorio } from "../../persistencia/TesteRepositorio";
import { TesteNegocios } from "../../negocios/TesteNegocios";
import { AtendimentoRepositorio } from "../../persistencia/AtendimentoRepositorio";
import { Atendimento } from "../../entidades/Atendimento";
import { Test } from "supertest";
import { CasoRepositorio } from "../../persistencia/CasoRepositorio";
import { Caso } from "../../entidades/Caso";
import { PacienteRepositorio } from "../../persistencia/PacienteRepositorio";

const mockTestes:Teste[] = [
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-02',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-03',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-01',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    }
];

const mockAtendimento:Atendimento = {
    ativo:true,
    //@ts-ignore
    horarioInicio: '2020-01-01T12:00:00.000Z',
    //@ts-ignore
    paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    etapas: [],
    testes: []
}
const mockAtendimentoTestes:Atendimento = {
    ativo:true,
    //@ts-ignore
    horarioInicio: '2020-01-01T12:00:00.000Z',
    //@ts-ignore
    paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    etapas: [],
    //@ts-ignore
    testes: ['a','b']
}

const mockCaso:Caso={
    //@ts-ignore
    paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    dataConfirmacao: '2020-05-01',
    ativo:true,
    //@ts-ignore
    teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
}

const mockPaciente={
    nome:'Paciente 3',
    endereco: 'rua c, 3',
    telefone: '(51) 3333-3335',
    email:'c@a.com',
    cpf:'000.000.000-75',
    dataNascimento: new Date(1990,1,1)
}

const mockRelatorio = [
    {
        _id:'2020-01-01',
        count:3
    },
    {
        _id:'2020-01-02',
        count:4
    },
]

describe('TesteNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar TesteRepositorio.listar()', async()=>{
            const mockTesteRepositorioListar = jest
                .spyOn(TesteRepositorio, 'listar')
                .mockResolvedValue(mockTestes);
            const result = await TesteNegocios.listar();
            expect(mockTesteRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockTestes)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar TesteRepositorio.buscarPorId()', async()=>{
            const mockTesteRepositorioListarPorId = jest
                .spyOn(TesteRepositorio, 'listarPorId')
                .mockResolvedValue(mockTestes[0]);
            const result = await TesteNegocios.buscarPorId('id')
            expect(mockTesteRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockTestes[0]);
        });
    });
    describe('listarPorPaciente()', ()=>{
        test('deve retornar TesteRepositorio.listarPorPaciente()', async()=>{
            const mockTesteRepositorioListarPorPaciente = jest
                .spyOn(TesteRepositorio, 'listarPorPaciente')
                .mockResolvedValue(mockTestes);
            const result = await TesteNegocios.listarPorPaciente('id')
            expect(mockTesteRepositorioListarPorPaciente).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockTestes);
        });
    });
    describe('listarPorUnidade()', ()=>{
        test('deve retornar TesteRepositorio.listarPorUnidade()', async()=>{
            const mockTesteRepositorioListarPorUnidade = jest
                .spyOn(TesteRepositorio, 'listarPorUnidade')
                .mockResolvedValue(mockTestes);
            const result = await TesteNegocios.listarPorUnidade('id')
            expect(mockTesteRepositorioListarPorUnidade).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockTestes);
        });
    });
    describe('listarPositivos()', ()=>{
        test('deve retornar TesteRepositorio.listarPositivos()', async()=>{
            const mockTesteRepositorioListarPorUnidade = jest
                .spyOn(TesteRepositorio, 'listarPositivos')
                .mockResolvedValue(mockTestes);
            const result = await TesteNegocios.listarPositivos()
            expect(mockTesteRepositorioListarPorUnidade).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockTestes);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar TesteRepositorio.inserir()', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimento)
            const mockTesteRepositorioInserir = jest
                .spyOn(TesteRepositorio, 'inserir')
                .mockImplementation((t:Teste)=>Promise.resolve(t));
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=> Promise.resolve(a));

            const result = await TesteNegocios.inserir(mockTestes[0]);
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('aaaaaaaaaaaaaaaaaaaaaaaa');
            expect(mockTesteRepositorioInserir).toHaveBeenCalledWith(mockTestes[0]);
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(1);
            expect(mockAtendimento.testes).toHaveLength(1);
            expect(result).toEqual(mockTestes[0]);
        });
        test('Deve apresentar erro Id Inexistente', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const insercao = async ()=>{await TesteNegocios.inserir(mockTestes[0])};
            await expect(insercao).rejects.toThrow('id inexistente');
        });
        test('Deve apresentar erro máximo de testes', async()=>{
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimentoTestes);
            const insercao = async ()=>{await TesteNegocios.inserir(mockTestes[0])};
            await expect(insercao).rejects.toThrow('máximo de testes por atendimento');
        });
    });
    describe('atualizar()',()=>{
        test('Deve retornar TesteRepositorio.atualizar() e cadastrar caso', async()=>{
            const mockTesteRepositorioAtualizar = jest
                .spyOn(TesteRepositorio, 'atualizar')
                .mockImplementation((id:string, t:Teste)=>Promise.resolve(t));
            const mockCasoRepositorioBuscarPorTeste = jest
                .spyOn(CasoRepositorio, 'buscarCasoPorTeste')
                .mockResolvedValue(null)
            const pacienteRepositorioListarPorId = jest
                .spyOn(PacienteRepositorio, 'listarPorId')
                .mockResolvedValue(mockPaciente)
            const mockCasoRepositorioInserir = jest
                .spyOn(CasoRepositorio, 'inserir')
                .mockResolvedValue(mockCaso)
            const result = await TesteNegocios.atualizar('id', mockTestes[0]);
            expect(mockTesteRepositorioAtualizar).toHaveBeenCalledWith('id', mockTestes[0]);
            expect(mockCasoRepositorioBuscarPorTeste).toHaveBeenCalledWith('id');
            expect(pacienteRepositorioListarPorId).toHaveBeenCalledWith('aaaaaaaaaaaaaaaaaaaaaaaa');
            expect(mockCasoRepositorioInserir).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockTestes[0]);
        });
        test('Deve retornar testeRepositorio.atualizar(), mas nao cadastrar caso', async()=>{
            const mockTesteRepositorioAtualizar = jest
                .spyOn(TesteRepositorio, 'atualizar')
                .mockImplementation((id:string, t:Teste)=>Promise.resolve(t));
            const mockCasoRepositorioBuscarPorTeste = jest
                .spyOn(CasoRepositorio, 'buscarCasoPorTeste')
                .mockResolvedValue(mockCaso)
            const result = await TesteNegocios.atualizar('id', mockTestes[0]);
            expect(mockTesteRepositorioAtualizar).toHaveBeenCalledWith('id', mockTestes[0]);
            expect(mockCasoRepositorioBuscarPorTeste).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockTestes[0]);
        });
        test('Deve apresentar erro se nao encontrar o paciente', async()=>{
            const mockTesteRepositorioAtualizar = jest
                .spyOn(TesteRepositorio, 'atualizar')
                .mockImplementation((id:string, t:Teste)=>Promise.resolve(t));
            const mockCasoRepositorioBuscarPorTeste = jest
                .spyOn(CasoRepositorio, 'buscarCasoPorTeste')
                .mockResolvedValue(null)
            const pacienteRepositorioListarPorId = jest
                .spyOn(PacienteRepositorio, 'listarPorId')
                .mockRejectedValue(null)
            const atualizacao = async()=>{await TesteNegocios.atualizar('id', mockTestes[0])};
            expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar TesteRepositorio.deletar()', async()=>{
            const mockTesteRepositorioListarPorId = jest
                .spyOn(TesteRepositorio, 'listarPorId')
                .mockResolvedValue(mockTestes[0]);
            const mockAtendimentoRepositorioListarPorId = jest
                .spyOn(AtendimentoRepositorio, 'listarPorId')
                .mockResolvedValue(mockAtendimento)
            const mockAtendimentoRepositorioAtualizar = jest
                .spyOn(AtendimentoRepositorio, 'atualizar')
                .mockImplementation((id:string, a:Atendimento)=>Promise.resolve(a))
                .mockReset()
            const mockCasoRepositorioBuscarPorTeste = jest
                .spyOn(CasoRepositorio, 'buscarCasoPorTeste')
                .mockResolvedValue(mockCaso)
            const mockCasoRepositorioDeletar = jest
                .spyOn(CasoRepositorio, 'deletar')
                .mockResolvedValue(mockCaso)
            const mockTesteRepositorioDeletar = jest
                .spyOn(TesteRepositorio, 'deletar')
                .mockResolvedValue(mockTestes[0]);
            const result = await TesteNegocios.deletar('id');
            expect(mockTesteRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockAtendimentoRepositorioListarPorId).toHaveBeenCalledWith('aaaaaaaaaaaaaaaaaaaaaaaa');
            expect(mockAtendimentoRepositorioAtualizar).toHaveBeenCalledTimes(1);
            expect(mockCasoRepositorioBuscarPorTeste).toHaveBeenCalledWith('id');
            expect(mockCasoRepositorioDeletar).toHaveBeenCalledTimes(1);
            expect(mockTesteRepositorioDeletar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockTestes[0]);
        });
        test('Deve Apresentar erro', async()=>{
            const mockTesteRepositorioListarPorId = jest
                .spyOn(TesteRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const exclusao = async()=>{await TesteNegocios.deletar('id')};
            expect(exclusao).rejects.toThrow();
        });
    });
    describe('testesPorDia()', ()=>{
        test('Deve retornar TesteRepositorio.testePorDia()', async()=>{
            const mockTesteRepositorioRelatorio = jest
                .spyOn(TesteRepositorio, 'testesPorDia')
                .mockResolvedValue(mockRelatorio)
            const result = await TesteNegocios.testesPorDia();
            expect(mockTesteRepositorioRelatorio).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockRelatorio);
        })
    })
});