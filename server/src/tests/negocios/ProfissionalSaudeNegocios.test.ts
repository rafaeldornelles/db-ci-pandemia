import { ProfissionalSaude } from "../../entidades/ProfissionalSaude";
import { ProfissionalSaudeRepositorio } from "../../persistencia/ProfissionalSaudeRepositorio";
import { profissionalSaudeNegocios } from "../../negocios/ProfissionalSaudeNegocios";

const mockProfissionais:ProfissionalSaude[] = [
    {
        nome:'Profissional 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0000'
    },
    {
        nome:'Profissional 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'med',
        registroProfissional: 'aa0001'
    },
    {
        nome:'Profissional 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0001'
    },
];

describe('profissionalSaudeNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar ProfissionalSaudeRepositorio.listar()', async()=>{
            const mockProfissionalSaudeListar = jest
                .spyOn(ProfissionalSaudeRepositorio, 'listar')
                .mockResolvedValue(mockProfissionais);
            const result = await profissionalSaudeNegocios.listar();
            expect(mockProfissionalSaudeListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockProfissionais)
        });
    });
    describe('listarPorUnidadeSaude()', ()=>{
        test('deve retornar ProfissionalSaudeRepositorio.listarPorUnidadeSaude()', async()=>{
            const mockProfissionalSaudeListarPorUnidadeSaude = jest
                .spyOn(ProfissionalSaudeRepositorio, 'listarPorUnidadeSaude')
                .mockResolvedValue(mockProfissionais);
            const result = await profissionalSaudeNegocios.listarPorUnidadeSaude('id');
            expect(mockProfissionalSaudeListarPorUnidadeSaude).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockProfissionais)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar ProfissionalSaudeRepositorio.buscarPorId()', async()=>{
            const mockProfissionalSaudeListarPorId = jest
                .spyOn(ProfissionalSaudeRepositorio, 'listarPorId')
                .mockResolvedValue(mockProfissionais[0]);
            const result = await profissionalSaudeNegocios.buscarPorId('id')
            expect(mockProfissionalSaudeListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockProfissionais[0]);
        });
    });
    describe('buscarPorRegistroProfissional()', ()=>{
        test('deve retornar ProfissionalSaudeRepositorio.buscarPorRegistroProfissional()', async()=>{
            const mockProfissionalSaudeListarPorRegistroProfissional = jest
                .spyOn(ProfissionalSaudeRepositorio, 'listarPorRegistroProfissional')
                .mockResolvedValue(mockProfissionais[0]);
            const result = await profissionalSaudeNegocios.buscarPorRegistroProfissional('reg')
            expect(mockProfissionalSaudeListarPorRegistroProfissional).toHaveBeenCalledWith('reg');
            expect(result).toEqual(mockProfissionais[0]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar ProfissionalSaudeRepositorio.inserir()', async()=>{
            const mockProfissionalSaudeInserir = jest
                .spyOn(ProfissionalSaudeRepositorio, 'inserir')
                .mockImplementation((p:ProfissionalSaude)=>Promise.resolve(p));
            const result = await profissionalSaudeNegocios.inserir(mockProfissionais[0]);
            expect(mockProfissionalSaudeInserir).toHaveBeenCalledWith(mockProfissionais[0]);
            expect(result).toEqual(mockProfissionais[0]);
        });
    });
    describe('atualizar()',()=>{
        test('Deve retornar ProfissionalSaudeRepositorio.atualizar()', async()=>{
            const mockProfissionalSaudeAtualizar = jest
                .spyOn(ProfissionalSaudeRepositorio, 'atualizar')
                .mockImplementation((id:string, p:ProfissionalSaude)=>Promise.resolve(p));
            const result = await profissionalSaudeNegocios.atualizar('id', mockProfissionais[0]);
            expect(mockProfissionalSaudeAtualizar).toHaveBeenCalledWith('id', mockProfissionais[0]);
            expect(result).toEqual(mockProfissionais[0]);
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar ProfissionalSaudeRepositorio.deletar()', async()=>{
            const mockProfissionalSaudeAtualizar = jest
                .spyOn(ProfissionalSaudeRepositorio, 'deletar')
                .mockResolvedValue(mockProfissionais[0]);
            const result = await profissionalSaudeNegocios.deletar('id');
            expect(mockProfissionalSaudeAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockProfissionais[0]);
        });
    });
});