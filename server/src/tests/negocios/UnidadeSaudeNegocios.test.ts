import { UnidadeSaudeRepositorio } from "../../persistencia/UnidadeSaudeRepositorio";
import { UnidadeSaudeNegocios } from "../../negocios/UnidadeSaudeNegocios";
import { UnidadeSaude } from "../../entidades/UnidadeSaude";
import { Atendimento } from "../../entidades/Atendimento";
import { AtendimentoRepositorio } from "../../persistencia/AtendimentoRepositorio";

const mockUnidades:UnidadeSaude[] = [
    {
        //@ts-ignore
        _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        nome:'Unidade 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333',
        //@ts-ignore
        toObject: ()=>mockUnidades[0]
    },
    {
        //@ts-ignore
        _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        nome:'Unidade 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334',
        //@ts-ignore
        toObject: ()=>mockUnidades[1]
    },
    {
        //@ts-ignore
        _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        nome:'Unidade 3',
        endereco: 'rua c, 3',
        telefone: '(51) 3333-3335',
        //@ts-ignore
        toObject: ()=>mockUnidades[2]
    },
];

const mockEstatisticas = {
    tempoMinimo: 3000,
    tempoMaximo: 10000,
    tempoMedio: 5000
}

const mockAtendimentos:Atendimento[] = [
    {
        ativo:false,
        horarioInicio: new Date('2020-01-01T12:00:00.000Z'),
        horarioFim: new Date('2020-01-01T12:30:00.000Z'),  //30 min = 1800000ms
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
    {
        ativo:false,
        //@ts-ignore
        horarioInicio: new Date('2020-01-01T12:00:00.000Z'),
        horarioFim: new Date('2020-01-01T13:00:00.000Z'), //1h = 3600000ms
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
    {
        ativo:false,
        //@ts-ignore
        horarioInicio: new Date('2020-01-01T12:00:00.000Z'),
        horarioFim: new Date('2020-01-01T12:15:00.000Z'), //15min = 900000ms
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
    {
        ativo:true,
        //@ts-ignore
        horarioInicio: new Date('2020-01-01T12:00:00.000Z'),
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
];

describe('UnidadeSaudeNegocios',()=>{
    describe('listar()', ()=>{
        test('deve retprar uma lista de unidades com estatisticas caso haja atendimentos', async()=>{
            const mockUnidadeRepositorioListar = jest
                .spyOn(UnidadeSaudeRepositorio, 'listar')
                .mockResolvedValue(mockUnidades);
            const mockGetEstatisticasUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'getEstatisticasUnidade')
                .mockResolvedValueOnce(mockEstatisticas)
                .mockResolvedValue(null)
            const mockAtendimentoRepositorioListarPorUnidade = jest
                .spyOn(AtendimentoRepositorio, 'listarPorUnidade')
                .mockResolvedValueOnce(mockAtendimentos)
                .mockResolvedValue([]);
            const result = await UnidadeSaudeNegocios.listar();
            expect(mockUnidadeRepositorioListar).toHaveBeenCalledTimes(1);
            expect(mockGetEstatisticasUnidade).toHaveBeenCalledTimes(mockUnidades.length);
            expect(result[0]).toHaveProperty('nome');
            expect(result[0]).toHaveProperty('endereco');
            expect(result[0]).toHaveProperty('telefone');
            expect(result[0]).toHaveProperty('estatisticas');
            expect(result[1]).not.toHaveProperty('estatisticas');
            delete mockUnidades[0].estatisticas;
            mockGetEstatisticasUnidade.mockRestore();
            mockAtendimentoRepositorioListarPorUnidade.mockRestore();
        });
    });
    describe('buscarPorId()', ()=>{
        test('Deve retornar unidade com estatisticas', async()=>{
            const mockUnidadeRepositorioListarPorId = jest
            .spyOn(UnidadeSaudeRepositorio, 'listarPorId')
            .mockResolvedValue(mockUnidades[0]);
            const mockGetEstatisticasUnidade = jest
            .spyOn(UnidadeSaudeNegocios, 'getEstatisticasUnidade')
            .mockResolvedValue(mockEstatisticas);
            const result = await UnidadeSaudeNegocios.buscarPorId('id');
            expect(mockUnidadeRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockGetEstatisticasUnidade).toHaveBeenCalledWith(mockUnidades[0]);
            expect(result).toHaveProperty('nome');
            expect(result).toHaveProperty('endereco');
            expect(result).toHaveProperty('telefone');
            expect(result).toHaveProperty('estatisticas');
            delete mockUnidades[0].estatisticas;
            mockGetEstatisticasUnidade.mockRestore();
        });
        test('Deve retornar unidade sem estatisticas', async()=>{
            const mockUnidadeRepositorioListarPorId = jest
            .spyOn(UnidadeSaudeRepositorio, 'listarPorId')
            .mockResolvedValue(mockUnidades[0]);
            const mockGetEstatisticasUnidade = jest
            .spyOn(UnidadeSaudeNegocios, 'getEstatisticasUnidade')
            .mockResolvedValue(null);
            const result = await UnidadeSaudeNegocios.buscarPorId('id');
            expect(mockUnidadeRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockGetEstatisticasUnidade).toHaveBeenCalledWith(mockUnidades[0]);
            expect(result).toEqual(mockUnidades[0]);
            expect(result).not.toHaveProperty('estatisticas');
            delete mockUnidades[0].estatisticas;
            mockGetEstatisticasUnidade.mockRestore();
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar UnidadeSaudeRepositorio.inserir()', async()=>{
            const mockUnidadeRepositorioInserir = jest
                .spyOn(UnidadeSaudeRepositorio, 'inserir')
                .mockImplementation((u:UnidadeSaude)=>Promise.resolve(u));
            const result = await UnidadeSaudeNegocios.inserir(mockUnidades[0]);
            expect(result).toEqual(mockUnidades[0])
            expect(mockUnidadeRepositorioInserir).toBeCalledWith(mockUnidades[0])
        });
    });
    describe('atualizar()', ()=>{
        test('Deve retornar UnidadeSaudeRepositorio.atualizar()', async()=>{
            const mockUnidadeRepositorioAtualizar = jest
                .spyOn(UnidadeSaudeRepositorio, 'atualizar')
                .mockImplementation((id:string, u:UnidadeSaude)=>Promise.resolve(u));
            const result = await UnidadeSaudeNegocios.atualizar('id', mockUnidades[0]);
            expect(result).toEqual(mockUnidades[0])
            expect(mockUnidadeRepositorioAtualizar).toBeCalledWith('id', mockUnidades[0])
        });
    });
    describe('deletar()', ()=>{
        test('Deve retornar UnidadeSaudeRepositorio.deletar()', async()=>{
            const mockUnidadeRepositorioDeletar = jest
                .spyOn(UnidadeSaudeRepositorio, 'deletar')
                .mockResolvedValue(mockUnidades[0]);
            const result = await UnidadeSaudeNegocios.deletar('id');
            expect(result).toEqual(mockUnidades[0])
            expect(mockUnidadeRepositorioDeletar).toHaveBeenCalledWith('id');
        });
    });
    describe('getEstatisticasUnidade()', ()=>{
        test('Deve retornar estatisticas', async()=>{
            const mockAtendimentoRepositorioListarPorUnidade = jest
                .spyOn(AtendimentoRepositorio, 'listarPorUnidade')
                .mockResolvedValue(mockAtendimentos)
            const result = await UnidadeSaudeNegocios.getEstatisticasUnidade(mockUnidades[0]);
            expect(mockAtendimentoRepositorioListarPorUnidade).toHaveBeenCalledTimes(1);
            expect(result).toBeTruthy();
            expect(result!.tempoMaximo).toEqual(3600000);
            expect(result!.tempoMinimo).toEqual(900000);
            expect(result!.tempoMedio).toEqual(2100000);
        });
        test('Deve retornar null', async()=>{
            const mockAtendimentoRepositorioListarPorUnidade = jest
                .spyOn(AtendimentoRepositorio, 'listarPorUnidade')
                .mockResolvedValue([]);
            const result = await UnidadeSaudeNegocios.getEstatisticasUnidade(mockUnidades[0]);
            expect(result).toBeNull();
        });
    });
});