import { PacienteRepositorio } from "../../persistencia/PacienteRepositorio";
import { PacienteNegocios } from "../../negocios/PacienteNegocios";
import { Paciente } from "../../entidades/Paciente";

const mockPacientes:Paciente[] = [
    {
        nome:'Paciente 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333',
        email:'a@a.com',
        cpf:'000.000.000-77',
        dataNascimento: new Date(1990,1,1)
    },
    {
        nome:'Paciente 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334',
        email:'b@a.com',
        cpf:'000.000.000-76',
        dataNascimento: new Date(1990,1,1)
    },
    {
        nome:'Paciente 3',
        endereco: 'rua c, 3',
        telefone: '(51) 3333-3335',
        email:'c@a.com',
        cpf:'000.000.000-75',
        dataNascimento: new Date(1990,1,1)
    },
];

describe('PacienteNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar PacienteRepositorio.listar()', async()=>{
            const mockPacienteRepositorioListar = jest
                .spyOn(PacienteRepositorio, 'listar')
                .mockResolvedValue(mockPacientes);
            const result = await PacienteNegocios.listar();
            expect(mockPacienteRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockPacientes)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar PacienteRepositorio.buscarPorId()', async()=>{
            const mockPacienteRepositorioListarPorId = jest
                .spyOn(PacienteRepositorio, 'listarPorId')
                .mockResolvedValue(mockPacientes[0]);
            const result = await PacienteNegocios.buscarPorId('id')
            expect(mockPacienteRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockPacientes[0]);
        });
    });
    describe('buscarPorCpf()', ()=>{
        test('deve retornar PacienteRepositorio.buscarPorCpf()', async()=>{
            const mockPacienteRepositorioListarPorId = jest
                .spyOn(PacienteRepositorio, 'listarPorCpf')
                .mockResolvedValue(mockPacientes[0]);
            const result = await PacienteNegocios.buscarPorCpf('cpf')
            expect(mockPacienteRepositorioListarPorId).toHaveBeenCalledWith('cpf');
            expect(result).toEqual(mockPacientes[0]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve retornar PacienteRepositorio.inserir()', async()=>{
            const mockPacienteRepositorioInserir = jest
                .spyOn(PacienteRepositorio, 'inserir')
                .mockImplementation((p:Paciente)=>Promise.resolve(p));
            const result = await PacienteNegocios.inserir(mockPacientes[0]);
            expect(mockPacienteRepositorioInserir).toHaveBeenCalledWith(mockPacientes[0]);
            expect(result).toEqual(mockPacientes[0]);
        });
    });
    describe('atualizar()',()=>{
        test('Deve retornar PacienteRepositorio.atualizar()', async()=>{
            const mockPacienteRepositorioAtualizar = jest
                .spyOn(PacienteRepositorio, 'atualizar')
                .mockImplementation((id:string, p:Paciente)=>Promise.resolve(p));
            const result = await PacienteNegocios.atualizarDados('id', mockPacientes[0]);
            expect(mockPacienteRepositorioAtualizar).toHaveBeenCalledWith('id', mockPacientes[0]);
            expect(result).toEqual(mockPacientes[0]);
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar PacienteRepositorio.deletar()', async()=>{
            const mockPacienteRepositorioAtualizar = jest
                .spyOn(PacienteRepositorio, 'deletar')
                .mockResolvedValue(mockPacientes[0]);
            const result = await PacienteNegocios.deletar('id');
            expect(mockPacienteRepositorioAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockPacientes[0]);
        });
    });
});