import { CasoRepositorio } from "../../persistencia/CasoRepositorio";
import { CasoNegocios } from "../../negocios/CasoNegocios";
import { Caso } from "../../entidades/Caso";
import { casoModel } from "../../persistencia/casoModel";

const mockCasos:Caso[] = [
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-01',
        ativo:true,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-01',
        ativo:true,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-02',
        ativo:false,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
];

const mockRelatorio = [
    {
        _id:'2020-01-01',
        count:3
    },
    {
        _id:'2020-01-02',
        count:5
    }
];

describe('CasoNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar CasoRepositorio.listar()', async()=>{
            const mockCasoRepositorioListar = jest
                .spyOn(CasoRepositorio, 'listar')
                .mockResolvedValue(mockCasos);
            const result = await CasoNegocios.listar();
            expect(mockCasoRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockCasos)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar CasoRepositorio.buscarPorId()', async()=>{
            const mockCasoRepositorioListarPorId = jest
                .spyOn(CasoRepositorio, 'listarPorId')
                .mockResolvedValue(mockCasos[0]);
            const result = await CasoNegocios.buscarPorId('id')
            expect(mockCasoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockCasos[0]);
        });
    });
    describe('buscarPorData()', ()=>{
        test('deve retornar CasoRepositorio.buscarPorData()', async()=>{
            const mockCasoRepositorioBuscarPorData = jest
                .spyOn(CasoRepositorio, 'listarPorData')
                .mockResolvedValue(mockCasos);
            const data = new Date()
            const result = await CasoNegocios.buscarPorData(data)
            expect(mockCasoRepositorioBuscarPorData).toHaveBeenCalledWith(data);
            expect(result).toEqual(mockCasos);
        });
    });
    describe('quantidadePorDia()', ()=>{
        test('deve retornar CasoRepositorio.quantidadePorDia()', async()=>{
            const mockCasoRepositorioQuantidadePorDia = jest
                .spyOn(CasoRepositorio, 'quantidadePorDia')
                .mockResolvedValue(mockRelatorio);
            const result = await CasoNegocios.quantidadePorDia()
            expect(mockCasoRepositorioQuantidadePorDia).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockRelatorio);
        });
    });
    describe('tornarInatico()', ()=>{
        test('deve retornar CasoRepositorio.quantidadePorDia()', async()=>{
            const mockCasoRepositorioListarPorId = jest
                .spyOn(CasoRepositorio, 'listarPorId')
                .mockResolvedValue(mockCasos[0]);
            const mockCasoRepositorioAtualizar = jest
                .spyOn(CasoRepositorio, 'atualizar')
                .mockImplementation((id:string, c:Caso)=>Promise.resolve(c));
            const result = await CasoNegocios.tornarInativo('id')
            expect(mockCasoRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(mockCasoRepositorioAtualizar).toHaveBeenCalledWith('id', mockCasos[0]);
            expect(result).toEqual(mockCasos[0]);
        });
        test('deve apresentar erro idInexistente', async()=>{
            const mockCasoRepositorioListarPorId = jest
                .spyOn(CasoRepositorio, 'listarPorId')
                .mockResolvedValue(null);
            const atualizacao = async()=>{await CasoNegocios.tornarInativo('id')};
            await expect(atualizacao).rejects.toThrow('id inexistente');
        });
        test('deve apresentar erro caso já inativo', async()=>{
            const mockCasoRepositorioListarPorId = jest
                .spyOn(CasoRepositorio, 'listarPorId')
                .mockResolvedValue(mockCasos[2]);
            const atualizacao = async()=>{await CasoNegocios.tornarInativo('id')};
            await expect(atualizacao).rejects.toThrow('Caso já inativo');
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar CasoRepositorio.deletar()', async()=>{
            const mockCasoRepositorioAtualizar = jest
                .spyOn(CasoRepositorio, 'deletar')
                .mockResolvedValue(mockCasos[0]);
            const result = await CasoNegocios.deletar('id');
            expect(mockCasoRepositorioAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockCasos[0]);
        });
    });
});