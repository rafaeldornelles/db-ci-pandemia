import { Gestor } from "../../entidades/Gestor";
import { Paciente } from "../../entidades/Paciente";
import { ProfissionalSaude } from "../../entidades/ProfissionalSaude";
import { UsuarioRepositorio } from "../../persistencia/UsuarioRepositorio";
import { UsuarioNegocios } from "../../negocios/UsuarioNegocios";

const mockUsuarios:(Gestor|Paciente|ProfissionalSaude)[] = [
    {
        nome:'Unidade 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        credencial: '1111'
    },
    {
        nome:'Unidade 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        credencial:'1112'
    },
    {
        nome:'Unidade 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        credencial:'1113'
    },
    {
        nome:'Paciente 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333',
        email:'a2@a.com',
        cpf:'000.000.000-77',
        // @ts-ignore
        dataNascimento: '1990-05-01'
    },
    {
        nome:'Paciente 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334',
        email:'b2@a.com',
        cpf:'000.000.000-76',
        // @ts-ignore
        dataNascimento: '1990-05-01'
    },
    {
        nome:'Profissional 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0000'
    },
    {
        nome:'Profissional 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'med',
        registroProfissional: 'aa0001'
    }
];


describe('UsuarioNegocios', ()=>{
    describe('listar()', ()=>{
        test('deve retornar UsuarioRepositorio.listar()', async()=>{
            const mockUsuarioRepositorioListar = jest
                .spyOn(UsuarioRepositorio, 'listar')
                .mockResolvedValue(mockUsuarios);
            const result = await UsuarioNegocios.listar();
            expect(mockUsuarioRepositorioListar).toHaveBeenCalledTimes(1);
            expect(result).toEqual(mockUsuarios)
        });
    });
    describe('buscarPorId()', ()=>{
        test('deve retornar UsuarioRepositorio.buscarPorId()', async()=>{
            const mockUsuarioRepositorioListarPorId = jest
                .spyOn(UsuarioRepositorio, 'listarPorId')
                .mockResolvedValue(mockUsuarios[0]);
            const result = await UsuarioNegocios.buscarPorId('id')
            expect(mockUsuarioRepositorioListarPorId).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockUsuarios[0]);
        });
    });
    describe('buscarPorEmail()', ()=>{
        test('deve retornar UsuarioRepositorio.buscarPorId()', async()=>{
            const mockUsuarioRepositorioListarPorEmail = jest
                .spyOn(UsuarioRepositorio, 'listarPorEmail')
                .mockResolvedValue(mockUsuarios[0]);
            const result = await UsuarioNegocios.buscarPorEmail('a@a.com')
            expect(mockUsuarioRepositorioListarPorEmail).toHaveBeenCalledWith('a@a.com');
            expect(result).toEqual(mockUsuarios[0]);
        });
    });
    describe('deletar()',()=>{
        test('Deve retornar UsuarioRepositorio.deletar()', async()=>{
            const mockUsuarioRepositorioAtualizar = jest
                .spyOn(UsuarioRepositorio, 'deletar')
                .mockResolvedValue(mockUsuarios[0]);
            const result = await UsuarioNegocios.deletar('id');
            expect(mockUsuarioRepositorioAtualizar).toHaveBeenCalledWith('id');
            expect(result).toEqual(mockUsuarios[0]);
        });
    });
});