import * as dbhandler from '../dbhandler';
import { atendimentoModel } from '../../persistencia/atendimentoModel';
import { Paciente } from '../../entidades/Paciente';
import { pacienteModel } from '../../persistencia/pacienteModel';
import { unidadeSaudeModel, unidadeSaudeDocument } from '../../persistencia/unidadeSaudeModel';
import { AtendimentoRepositorio } from '../../persistencia/AtendimentoRepositorio';
import { Atendimento } from '../../entidades/Atendimento';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { etapaAtendimentoModel } from '../../persistencia/etapaAtendimentoModel';
import { comorbidadeModel } from '../../persistencia/comorbidadeModel';
import { testeModel } from '../../persistencia/testeModel';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Atendimento Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar uma lista vazia', async ()=>{
            let atendimentos = await AtendimentoRepositorio.listar();
            expect(atendimentos).toEqual([]);
        });
        test('Deve retornar uma lista com tamanho 2', async ()=>{
            await seedDatabase();
            let atendimentos = await AtendimentoRepositorio.listar();
            etapaAtendimentoModel.find();
            expect(atendimentos).toHaveLength(2);
        });
    });

    describe('listarPorUnidade()', ()=>{
        test('Deve retornar lista de atendimentos com unidade', async()=>{
            const ids = await seedDatabase();
            const umAtendimento = await AtendimentoRepositorio.listarPorId(ids[0]);
            const idUnidade = (umAtendimento!.unidadeSaude as unidadeSaudeDocument)._id;
            const atendimentos = await AtendimentoRepositorio.listarPorUnidade(idUnidade);
            expect(atendimentos).toHaveLength(2);
            expect(atendimentos[0].unidadeSaude).toEqual(umAtendimento!.unidadeSaude);
        });
        test('Deve retornar lista vazia', async()=>{
            const atendimentos = await AtendimentoRepositorio.listar();
            expect(atendimentos).toEqual([]);
        });
    })

    describe('listarPorId()',()=>{
        test('Deve apresentar erro ao buscar id inválido', async()=>{
            const idInvalido = 'a';
            expect(async()=>{await AtendimentoRepositorio.listarPorId(idInvalido)}).rejects.toThrow();
        });
        test('Deve retornar um atendimento', async()=>{
            const atendimentos = await seedDatabase();
            const atendimentoBuscado = await AtendimentoRepositorio.listarPorId(atendimentos[0]);
            expect(atendimentoBuscado).toHaveProperty('horarioInicio');
            expect(atendimentoBuscado).toHaveProperty('ativo');
            expect(atendimentoBuscado).toHaveProperty('unidadeSaude');
            expect(atendimentoBuscado).toHaveProperty('paciente');
            expect((atendimentoBuscado as any)._id).toEqual(atendimentos[0]);
        })
    })

    describe('inserir()', ()=>{
        test('Deve retornar um atendimento com id e propriedades', async ()=> {
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            const atendimento = await criarAtendimento(unidadeSaude, paciente);
            const atendimentoCriado = await AtendimentoRepositorio.inserir(atendimento);
            expect(atendimentoCriado).toHaveProperty('_id');
            expect(atendimentoCriado.ativo).toEqual(atendimento.ativo);
            expect(atendimentoCriado.horarioInicio).toEqual(atendimento.horarioInicio);
            expect(atendimentoCriado.paciente).toEqual(atendimento.paciente);
            expect(atendimentoCriado.unidadeSaude).toEqual(atendimento.unidadeSaude);
        });
        
        test('Deve retornar um atendimento ativo como default', async ()=>{
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            let atendimento = await criarAtendimento(unidadeSaude, paciente);
            delete atendimento.ativo;
            let atendimentoCriado = await AtendimentoRepositorio.inserir(atendimento);
            expect(atendimentoCriado.ativo).toEqual(true)
        });

        test('Deve retornar um atendimento com o horario atual como default', async() => {
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            let atendimento = await criarAtendimento(unidadeSaude, paciente);
            delete atendimento.horarioInicio;
            const now = new Date();
            let atendimentoCriado = await AtendimentoRepositorio.inserir(atendimento);
            expect(atendimentoCriado.horarioInicio.getDate).toEqual(now.getDate);
            expect(atendimentoCriado.horarioInicio.getMonth).toEqual(now.getMonth);
            expect(atendimentoCriado.horarioInicio.getFullYear).toEqual(now.getFullYear);
            expect(atendimentoCriado.horarioInicio.getHours).toEqual(now.getHours);
            expect(atendimentoCriado.horarioInicio.getMinutes).toEqual(now.getMinutes);
        });

        test('Deve apresentar erro ao inserir atendimento sem paciente', async()=>{
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            let atendimento = await criarAtendimento(unidadeSaude, paciente);
            delete atendimento.paciente;
            expect(async()=>{await AtendimentoRepositorio.inserir(atendimento)}).rejects.toThrow();
        });

        test('Deve apresentar erro ao inserir atendimento sem unidade de saude', async()=>{
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            let atendimento = await criarAtendimento(unidadeSaude, paciente);
            delete atendimento.unidadeSaude;
            expect(async()=>{await AtendimentoRepositorio.inserir(atendimento)}).rejects.toThrow();
        });
    });

    describe('atualizar()', ()=> {
        test('Deve retornar um atendimento alterado', async ()=>{
            const atendimentos = await seedDatabase();
            const novoPaciente:Paciente = await pacienteModel.create({
                nome: 'Novo Paciente',
                endereco: 'rua nova, -',
                cpf: '111.000.000-77',
                dataNascimento: new Date(1912,3,14),
                email: 'a@novop.com',
                telefone: '99999999'
            });
            const novaUnidade:UnidadeSaude = await unidadeSaudeModel.create({
                nome: 'UBS Nova',
                endereco: 'rua nova, 1s',
                telefone: '77777777'
            });
            const atendimentoNovo:Atendimento = {
                horarioInicio: new Date(2019, 4, 19, 19, 20),
                horarioFim: new Date(2019, 4, 19, 19, 21),
                ativo: false,
                paciente: novoPaciente,
                unidadeSaude: novaUnidade,
                testes: []
            };
            const atendimentoAlterado = await AtendimentoRepositorio.atualizar(atendimentos[0], atendimentoNovo);
            expect(atendimentoAlterado.ativo).toEqual(atendimentoNovo.ativo);
            expect(atendimentoAlterado.horarioInicio).toEqual(atendimentoNovo.horarioInicio);
            expect(atendimentoAlterado.paciente).toEqual(atendimentoNovo.paciente);
            expect(atendimentoAlterado.unidadeSaude).toEqual(atendimentoNovo.unidadeSaude);
        });

        test('Deve apresentar erro ao atualizar para um objeto sem paciente', async ()=>{
            const atendimentos = await seedDatabase();
            const novaUnidade:UnidadeSaude = await unidadeSaudeModel.create({
                nome: 'UBS Nova',
                endereco: 'rua nova, 1s',
                telefone: '77777777'
            });
            const atendimentoNovo = {
                horarioInicio: new Date(2019, 4, 19, 19, 20),
                ativo: false,
                unidadeSaude: novaUnidade
            };
            expect(async ()=>{await AtendimentoRepositorio.atualizar(atendimentos[0], atendimentoNovo as any)}).rejects.toThrow();
        });

        test('Deve apresentar erro ao atualizar para um objeto sem unidade de saude', async()=>{
            const atendimentos = await seedDatabase();
            const novoPaciente:Paciente = await pacienteModel.create({
                nome: 'Novo Paciente',
                endereco: 'rua nova, -',
                cpf: '111.000.000-77',
                dataNascimento: new Date(1912,3,14),
                email: 'a@novop.com',
                telefone: '99999999'
            });
            const atendimentoNovo = {
                horarioInicio: new Date(2019, 4, 19, 19, 20),
                ativo: false,
                paciente: novoPaciente
            };
            expect(async()=> {await AtendimentoRepositorio.atualizar(atendimentos[0], atendimentoNovo as any)}).rejects.toThrow();
        });
        test('Deve apresentar erro ao atualizar um id inválido', async()=>{
            const unidadeSaude = await criarUnidadeSaude();
            const paciente = await criarPaciente();
            const atendimento = await criarAtendimento(unidadeSaude, paciente);
            const idInvalido = '2'
            expect(async()=>{await AtendimentoRepositorio.atualizar(idInvalido, atendimento)}).rejects.toThrow();
        });
    });

    describe('deletar()', ()=>{
        test('Deve remover item do banco', async()=>{
            const atendimentos = await seedDatabase();
            const idRemovido = atendimentos[0];
            const atendimentoRemovido = await AtendimentoRepositorio.deletar(idRemovido);
            expect(await AtendimentoRepositorio.listar()).toHaveLength(1);
        });
        test('Deve retornar atendimento removido', async()=>{
            const atendimentos = await seedDatabase();
            const idRemovido = atendimentos[0];
            const atendimentoRemovido = await AtendimentoRepositorio.deletar(idRemovido);
            expect((atendimentoRemovido as any)._id).toEqual(idRemovido);
        });
        test('Deve apresentar erro ao inserir um id inválido', ()=>{
            const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            expect(async() => {await AtendimentoRepositorio.deletar(idInexistente)}).rejects.toThrow();
        });
    });
    describe('quantidadePorDia()', ()=>{
        test('Deve retornar relatorio com casos por dia', async()=>{
            await seedDatabase();
            const relatorio = await AtendimentoRepositorio.quantidadePorDia() as any;
            expect(relatorio[0]).toHaveProperty('_id')
            expect(relatorio[0]).toHaveProperty('count')
            expect(relatorio[0]._id).toEqual('2020-05-19');
            expect(relatorio[0].count).toEqual(2);
        })
    })
});


async function seedDatabase():Promise<string[]>{
    testeModel.find()
    comorbidadeModel.find();
    etapaAtendimentoModel.find();
    const unidadeSaude = await criarUnidadeSaude();
    const paciente:Paciente = await criarPaciente();
    let a1 = await atendimentoModel.create(await criarAtendimento(unidadeSaude, paciente));
    let a2 = await atendimentoModel.create(await criarAtendimento(unidadeSaude, paciente));
    
    return [a1,a2].map(a=>a._id);
}

async function criarAtendimento(unidadeSaude:UnidadeSaude, paciente:Paciente):Promise<Atendimento>{
    return {
        horarioInicio: new Date(2020, 4, 19, 19, 20),
        ativo: true,
        paciente: paciente,
        unidadeSaude: unidadeSaude,
        testes: []
    }
}

async function criarPaciente(){
    const dataNascimento = new Date(1995, 0, 1);
    return pacienteModel.create({
        nome: 'Rafael',
        endereco: 'rua a, 0',
        cpf: `000.000.000-77`,
        dataNascimento: dataNascimento,
        email: `a@a.com`,
        telefone: '33333333'
    });
}

async function criarUnidadeSaude(){
    return unidadeSaudeModel.create({
        nome: 'UBS 1',
        endereco: 'rua b, 1s',
        telefone: '33333333'
    })
}
