import * as dbhandler from '../dbhandler';
import { atendimentoModel } from '../../persistencia/atendimentoModel';
import { Paciente } from '../../entidades/Paciente';
import { pacienteModel } from '../../persistencia/pacienteModel';
import { unidadeSaudeModel } from '../../persistencia/unidadeSaudeModel';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { etapaAtendimentoModel, etapaAtendimentoDocument } from '../../persistencia/etapaAtendimentoModel';
import { profissionalSaudeModel } from '../../persistencia/profissionalSaudeModel';
import { EtapaAtendimentoRepositorio } from '../../persistencia/EtapaAtendimentoRepositorio';
import { Atendimento } from '../../entidades/Atendimento';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
const idInvalido = 'a';

describe('EtapaAtendimento Repositorio', ()=>{
    describe('listar()',()=>{
        test('Deve retornar lista vazia', async()=>{
            const etapas = await EtapaAtendimentoRepositorio.listar();
            expect(etapas).toEqual([]);
        });
        test('Deve retornar 2 etapas', async()=>{
            await seedDatabase();
            const etapas = await EtapaAtendimentoRepositorio.listar();
            expect(etapas).toHaveLength(2);
        });
    });
    describe('listarPorId()',()=>{
        test('Deve retornar etapa com o id pesquisado', async()=>{
            const ids = await seedDatabase();
            const etapa = await EtapaAtendimentoRepositorio.listarPorId(ids[0]) as etapaAtendimentoDocument;
            expect(etapa._id).toEqual(ids[0]);
            expect(etapa).toHaveProperty('descricao');
            expect(etapa).toHaveProperty('horarioInicio');
            expect(etapa).toHaveProperty('horarioFim');
            expect(etapa).toHaveProperty('profissional');
            expect(etapa).toHaveProperty('atendimento');
        });
        test('Deve retornar null para id inexistente', async()=>{
            const etapa = await EtapaAtendimentoRepositorio.listarPorId(idInexistente)
            expect(etapa).toBeNull();
        });
        test('Deve apresentar erro para id invalido', async()=>{
            const busca = async()=>{await EtapaAtendimentoRepositorio.listarPorId(idInvalido)};
            await expect(busca).rejects.toThrow();
        });
    });
    describe('inserir()',()=>{
        test('Deve inserir uma etapa', async()=>{
            const e = await etapaValida()
            const etapa = await EtapaAtendimentoRepositorio.inserir(e);
            const etapas = await EtapaAtendimentoRepositorio.listar();
            expect(etapas).toHaveLength(1);
            expect(etapa).toHaveProperty('_id');
            expect(etapa.descricao).toEqual(e.descricao);
            expect(etapa.horarioInicio).toEqual(e.horarioInicio);
            expect(etapa.horarioFim).toEqual(e.horarioFim);
            expect(etapa.profissional).toEqual(e.profissional);
            expect(etapa.atendimento).toEqual(e.atendimento);
        });
    });
    describe('atualizar()',()=>{
        test('Deve atualizar etapa', async()=>{
            const ids = await seedDatabase();
            const e = await etapaValida();
            const etapa = await EtapaAtendimentoRepositorio.atualizar(ids[0], e) as etapaAtendimentoDocument;
            expect(etapa._id).toEqual(ids[0]);
            expect(etapa.descricao).toEqual(e.descricao);
            expect(etapa.horarioInicio).toEqual(e.horarioInicio);
            expect(etapa.horarioFim).toEqual(e.horarioFim);
            expect(etapa.profissional).toEqual(e.profissional);
            expect(etapa.atendimento).toEqual(e.atendimento);
        });
        test('Deve apresentar erro para id inexistente', async()=>{
            const e = await etapaValida();
            const atualizacao = async()=>{await EtapaAtendimentoRepositorio.atualizar(idInexistente, e)};
            await expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()',()=>{
        test('Deve remover etapa', async()=>{
            const ids = await seedDatabase();
            const etapa = await EtapaAtendimentoRepositorio.deletar(ids[0]) as etapaAtendimentoDocument;
            const etapas = await EtapaAtendimentoRepositorio.listar();
            expect(etapas).toHaveLength(1);
            expect(etapa._id).toEqual(ids[0])
        });
        test('Deve apresentar erro para id inválido', async()=>{
            const exclusao = async()=>{await EtapaAtendimentoRepositorio.deletar(idInexistente)};
            expect(exclusao).rejects.toThrow();
        });
    });
});


async function seedDatabase():Promise<string[]>{
    const u = await criarUnidadeSaude();
    const pro = await criarProfissional(u);
    const p = await criarPaciente();
    const a = await criarAtendimento(u,p);
    const e1 = await etapaAtendimentoModel.create({
        descricao:'Triagem',
        horarioInicio: new Date(2020,4,19,20,20),
        profissional: pro,
        atendimento: a
    });
    const e2 = await etapaAtendimentoModel.create({
        descricao:'atendimento',
        horarioInicio: new Date(2020,4,19,21,20),
        profissional: pro,
        atendimento: a
    });
    return[e1,e2].map(e=>e._id)
}

async function etapaValida(){
    const u = await criarUnidadeSaude('UBS 2');
    const pro = await criarProfissional(u, 'Pro 2', 'y@au.com', 'us0000');
    const p = await criarPaciente('paciente 2','999.999.999-90', 't@at.com')
    const a = await criarAtendimento(u,p);
    return etapaAtendimentoModel.create({
        descricao:'exames',
        horarioInicio: new Date(2020,4,19,21,20),
        profissional: pro,
        atendimento: a
    });
}

async function criarAtendimento(unidadeSaude:UnidadeSaude, paciente:Paciente):Promise<Atendimento>{
    return atendimentoModel.create({
        horarioInicio: new Date(2020, 4, 19, 19, 20),
        ativo: true,
        paciente: paciente,
        unidadeSaude: unidadeSaude,
        testes: []
    });
}

async function criarPaciente(nome:string='paciente 1',cpf:string = '000.000.000-77', email:string='a@a.com'){
    const dataNascimento = new Date(1995, 0, 1);
    return pacienteModel.create({
        nome: nome,
        endereco: 'rua a, 0',
        cpf: cpf,
        dataNascimento: dataNascimento,
        email: email,
        telefone: '33333333'
    });
}

async function criarUnidadeSaude(nome:string='UBS 1'){
    return unidadeSaudeModel.create({
        nome: nome,
        endereco: 'rua b, 1s',
        telefone: '33333333'
    });
}

async function criarProfissional(u:UnidadeSaude, nome:string='Prof 1', email:string='b@a.com', registro:string='aa0000'){
    return profissionalSaudeModel.create({
        unidadeSaude:u,
        nome:nome,
        telefone: '(51) 9999-9999',
        email:email,
        cargo:"enf",
        registroProfissional:registro
    })
}