import * as dbhandler from '../dbhandler';
import { Paciente } from '../../entidades/Paciente';
import { comorbidadeModel } from '../../persistencia/comorbidadeModel';
import { pacienteModel, pacienteDocument } from '../../persistencia/pacienteModel';
import { PacienteRepositorio } from '../../persistencia/PacienteRepositorio';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa'

const idInvalido = 'a';

const pacienteValido:Paciente = {
    nome:'Paciente Valido',
    email:'pacient@pac.com',
    telefone:'(51) 3222-2222',
    endereco:'rua a,3',
    cpf:'111.111.111-33',
    dataNascimento:new Date(1990,1,1),
    comorbidades: []
}

const pacienteCpfInalido:Paciente = {
    nome:'Paciente Valido',
    email:'pacien@pac.com',
    telefone:'(51) 3222-2222',
    endereco:'rua a,3',
    cpf:'1.111-33',
    dataNascimento:new Date(1990,1,1),
    comorbidades: []
}

describe('Paciente Repositorio', ()=>{
   describe('listar()', ()=>{
       test('Deve retornar uma lista vazia', async()=>{
           const pacientes = await PacienteRepositorio.listar();
           expect(pacientes).toEqual([]);
       });
       test('Deve retornar uma lista de pacientes com tamanho 2', async()=>{
           await seedDatabase();
           const pacientes = await PacienteRepositorio.listar();
           expect(pacientes).toHaveLength(2);
       });
       test('Deve popular comorbidades', async()=>{
           await seedDatabase();
           const pacientes = await PacienteRepositorio.listar();
           expect(pacientes[0].comorbidades![0]).toHaveProperty('descricao');
           expect(pacientes[0].comorbidades![0]).toHaveProperty('cid');
       });
   });
   describe('listarPorId()', ()=>{
        test('deve retornar um paciente', async()=>{
            const ids = await seedDatabase();
            const paciente = await PacienteRepositorio.listarPorId(ids[0]) as pacienteDocument;
            expect(paciente._id).toEqual(ids[0]);
            expect(paciente).toHaveProperty("nome");
            expect(paciente).toHaveProperty("nome");
            expect(paciente).toHaveProperty("email");
            expect(paciente).toHaveProperty("telefone");
            expect(paciente).toHaveProperty("endereco");
            expect(paciente).toHaveProperty("cpf");
            expect(paciente).toHaveProperty("dataNascimento");
            expect(paciente).toHaveProperty("comorbidades");
        });
        test('Deve retornar null ao buscar id inexistente', async()=>{
            const paciente = await PacienteRepositorio.listarPorId(idInexistente);
            expect(paciente).toBeNull();
        });
        test('Deve apresentar erro ao buscar id inválido', async()=>{
            const busca = async()=>{await PacienteRepositorio.listarPorId(idInvalido)}
            await expect(busca).rejects.toThrow();
        });
        test('Deve popular comorbidades', async()=>{
            const ids = await seedDatabase();
            const paciente = await PacienteRepositorio.listarPorId(ids[0]);
            expect(paciente).not.toBeNull();
            expect(paciente!.comorbidades![0]).toHaveProperty('descricao');
            expect(paciente!.comorbidades![0]).toHaveProperty('cid');
        });
   });
   describe('listarPorCpf()', ()=>{
        test('Deve retornar um paciente', async()=>{
            await seedDatabase();
            const paciente = await PacienteRepositorio.listarPorCpf('111.111.111-31');
            expect(paciente).toBeTruthy();
            expect(paciente).toHaveProperty('_id');
            expect(paciente!.nome).toEqual('Paciente Seed');
        });
        test('Deve retornar null ao buscar cpf inexistente', async()=>{
            await seedDatabase();
            const paciente = await PacienteRepositorio.listarPorCpf('999.999.999-99');
            expect(paciente).toBeNull();
        });
        test('Deve popular comorbidades', async()=>{
            await seedDatabase();
            const paciente = await PacienteRepositorio.listarPorCpf('111.111.111-31');
            expect(paciente).toBeTruthy();
            expect(paciente!.comorbidades![0]).toHaveProperty('descricao');
            expect(paciente!.comorbidades![0]).toHaveProperty('cid');
        });
   });
   describe('inserir()', ()=>{
        test('Deve inserir um objeto', async()=>{
            const paciente = await PacienteRepositorio.inserir(pacienteValido);
            const pacientes = await PacienteRepositorio.listar();
            expect(pacientes).toHaveLength(1);
            expect(paciente).toHaveProperty('_id');
            expect(paciente.nome).toEqual(pacienteValido.nome);
            expect(paciente.email).toEqual(pacienteValido.email);
            expect(paciente.endereco).toEqual(pacienteValido.endereco);
            expect(paciente.telefone).toEqual(pacienteValido.telefone);
            expect(paciente.cpf).toEqual(pacienteValido.cpf);
            expect(paciente.dataNascimento).toEqual(pacienteValido.dataNascimento);
            expect(paciente.comorbidades!.length).toEqual(pacienteValido.comorbidades!.length);
        });
        test('Deve apresentar erro ao inserir cpf invalido', async()=>{
            const insercao = async()=>{await PacienteRepositorio.inserir(pacienteCpfInalido)}
            await expect(insercao).rejects.toThrow();
        });
        test('Deve apresentar erro ao inserir cpf duplicado', async()=>{
            await PacienteRepositorio.inserir(pacienteValido);
            const insercaoRepetida = async()=>{await PacienteRepositorio.inserir(pacienteValido)}
            await expect(insercaoRepetida).rejects.toThrow();
        });
   });
   describe('atualizar()', ()=>{
        test('Deve atualizar um objeto', async()=>{
            const ids = await seedDatabase();
            const paciente = await PacienteRepositorio.atualizar(ids[0], pacienteValido) as pacienteDocument;
            expect(paciente._id).toEqual(ids[0]);
            expect(paciente.nome).toEqual(pacienteValido.nome);
            expect(paciente.email).toEqual(pacienteValido.email);
            expect(paciente.telefone).toEqual(pacienteValido.telefone);
            expect(paciente.endereco).toEqual(pacienteValido.endereco);
            expect(paciente.cpf).toEqual(pacienteValido.cpf);
            expect(paciente.dataNascimento).toEqual(pacienteValido.dataNascimento);
            expect(paciente.comorbidades?.length).toEqual(pacienteValido.comorbidades?.length);
        });
        test('Deve apresentar erro ao atualizar id inexistente', async()=>{
            await seedDatabase();
            const atualizacao = async()=>{await PacienteRepositorio.atualizar(idInexistente, pacienteValido)}
            await expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()', ()=>{
        test('Deve remover um objeto e retorna-lo', async()=>{
            const ids = await seedDatabase();
            const paciente = await PacienteRepositorio.deletar(ids[0]) as pacienteDocument;
            const pacientes = await PacienteRepositorio.listar();
            expect(pacientes).toHaveLength(1);
            expect(paciente._id).toEqual(ids[0]);
        });
        test('Deve apresentar erro ao remover id inexistente', async()=>{
            const remocao = async()=>{await PacienteRepositorio.deletar(idInexistente)};
            await expect(remocao).rejects.toThrow();
        });
   });
});


async function seedDatabase():Promise<string[]>{
    const c1 = await comorbidadeModel.create({
        descricao:'Asma',
        cid:"CID 10 - J45"
    });
    const c2 = await comorbidadeModel.create({
        descricao:'Diabetes',
        cid:"CID 10 - E11"
    });

    const p1 = await pacienteModel.create({
        nome:'Paciente Seed',
        email:'seed@pac.com',
        telefone:'(51) 3222-2223',
        endereco:'rua b,3',
        cpf:'111.111.111-31',
        dataNascimento:new Date(1990,1,1),
        comorbidades:[c1, c2]
    });

    const p2 = await pacienteModel.create({
        nome:'Paciente Seeeed',
        email:'paciente@pac.com',
        telefone:'(51) 3222-2220',
        endereco:'rua b,3',
        cpf:'111.111.111-32',
        dataNascimento:new Date(1990,1,1),
        comorbidades:[c1]
    });


    return [p1, p2].map(p=>p._id)
}
