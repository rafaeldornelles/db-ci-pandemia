import * as dbhandler from '../dbhandler';
import { unidadeSaudeModel } from '../../persistencia/unidadeSaudeModel';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { ProfissionalSaudeRepositorio } from '../../persistencia/ProfissionalSaudeRepositorio';
import { profissionalSaudeModel, profissionalSaudeDocument } from '../../persistencia/profissionalSaudeModel';
import { ProfissionalSaude } from '../../entidades/ProfissionalSaude';


jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInvalido = 'a';

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';


describe('ProfissionalSaude Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar uma lista vazia', async()=>{
            const profissionais = await ProfissionalSaudeRepositorio.listar();
            expect(profissionais).toEqual([]);
        });
        test('Deve retornar um array com dois elementos', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissionais = await ProfissionalSaudeRepositorio.listar();
            expect(profissionais).toHaveLength(2);
        });
        test('Deve popular a unidade de saude', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissionais = await ProfissionalSaudeRepositorio.listar();
            expect(profissionais[0].unidadeSaude).toHaveProperty('nome');
            expect(profissionais[0].unidadeSaude).toHaveProperty('endereco');
            expect(profissionais[0].unidadeSaude).toHaveProperty('telefone');
        });
    });
    describe('listarPorUnidadeSaude()', ()=>{
        test('Deve retornar uma lista de usuarios com a unidade pesquisada', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissionais = await ProfissionalSaudeRepositorio.listarPorUnidadeSaude(u._id);
            expect(profissionais).toHaveLength(2);
            expect(profissionais[0].unidadeSaude.nome).toEqual(u.nome);
            expect(profissionais[0].unidadeSaude.endereco).toEqual(u.endereco);
            expect(profissionais[0].unidadeSaude.telefone).toEqual(u.telefone);
        });
        test('Deve retornar uma lista vazia para unidades sem profissionais', async()=>{
            const u = await criarUnidadeSaude();
            const profissionais = await ProfissionalSaudeRepositorio.listarPorUnidadeSaude(u._id);
            expect(profissionais).toEqual([]);
        });
        test('Deve popular a unidade de saude', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissionais = await ProfissionalSaudeRepositorio.listarPorUnidadeSaude(u._id);
            expect(profissionais[0].unidadeSaude).toHaveProperty('nome');
            expect(profissionais[0].unidadeSaude).toHaveProperty('endereco');
            expect(profissionais[0].unidadeSaude).toHaveProperty('telefone');
        });
    });
    describe('listarPorId()', ()=>{
        test('Deve retornar uma unidade de saude', async()=>{
            const u = await criarUnidadeSaude();
            const ids = await seedDatabase(u);
            const profissional = await ProfissionalSaudeRepositorio.listarPorId(ids[0]) as profissionalSaudeDocument;
            expect(profissional).toBeTruthy();
            expect(profissional._id).toEqual(ids[0]);
            expect(profissional).toHaveProperty('nome');
            expect(profissional).toHaveProperty('email');
            expect(profissional).toHaveProperty('telefone');
            expect(profissional).toHaveProperty('cargo');
            expect(profissional).toHaveProperty('registroProfissional');
            expect(profissional).toHaveProperty('unidadeSaude');
        });
        test('Deve retornar null ao buscar um id inexistente', async()=>{
            const profissional = await ProfissionalSaudeRepositorio.listarPorId(idInexistente);
            expect(profissional).toBeNull();
        });
        test('Deve apresentar erro ao buscar id inválido', async()=>{
            const busca = async()=>{await ProfissionalSaudeRepositorio.listarPorId(idInvalido)}
            await expect(busca).rejects.toThrow();
        });
        test('Deve popular a unidade de saude', async()=>{
            const u = await criarUnidadeSaude();
            const ids = await seedDatabase(u);
            const profissional = await ProfissionalSaudeRepositorio.listarPorId(ids[0]);
            expect(profissional).toBeTruthy();
            expect(profissional!.unidadeSaude).toHaveProperty('nome');
            expect(profissional!.unidadeSaude).toHaveProperty('endereco');
            expect(profissional!.unidadeSaude).toHaveProperty('telefone');
        });
    });
    describe('listarPorRegistroProfissional()', ()=>{
        test('Deve retornar um objeto com o registro buscado', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissional = await ProfissionalSaudeRepositorio.listarPorRegistroProfissional('aa0001');
            expect(profissional).toBeTruthy();
            expect(profissional).toHaveProperty('nome');
            expect(profissional).toHaveProperty('email');
            expect(profissional).toHaveProperty('telefone');
            expect(profissional).toHaveProperty('cargo');
            expect(profissional).toHaveProperty('registroProfissional');
            expect(profissional).toHaveProperty('unidadeSaude');
        });
        test('Deve retornar vazio para um registro inexistente', async()=>{
            const profissional = await ProfissionalSaudeRepositorio.listarPorRegistroProfissional('aa');
            expect(profissional).toBeNull();
        });
        test('Deve popular a unidade de saude', async()=>{
            const u = await criarUnidadeSaude();
            await seedDatabase(u);
            const profissional = await ProfissionalSaudeRepositorio.listarPorRegistroProfissional('aa0001');
            expect(profissional).toBeTruthy();
            expect(profissional!.unidadeSaude).toHaveProperty('nome');
            expect(profissional!.unidadeSaude).toHaveProperty('endereco');
            expect(profissional!.unidadeSaude).toHaveProperty('telefone');
        });
    });
    describe('inserir()', ()=>{
        test('Deve inserir um profissional', async()=>{
            const u = await criarUnidadeSaude();
            const profissionalIns = profissionalValido(u);
            const profissional = await ProfissionalSaudeRepositorio.inserir(profissionalIns);
            const profissionais = await ProfissionalSaudeRepositorio.listar();
            expect(profissional).toHaveProperty('_id');
            expect(profissionais).toHaveLength(1);
        });
        test('Deve apresentar erro ao inserir registro duplicado', async()=>{
            const u = await criarUnidadeSaude();
            await ProfissionalSaudeRepositorio.inserir(profissionalValido(u));
            const insercaoDuplicada = async()=>{await ProfissionalSaudeRepositorio.inserir(profissionalValido(u))}
            await expect(insercaoDuplicada).rejects.toThrow();
        });
        test('Deve apresentar erro ao inserir cargo invalido', async()=>{
            const u = await criarUnidadeSaude();
            const insercao = async()=>{await ProfissionalSaudeRepositorio.inserir(profissionalCargoInvalido(u))}
            await expect(insercao).rejects.toThrow();
        });
    });
    describe('atualizar()', ()=>{
        test('Deve atualizar um profissional', async()=>{
            const u = await criarUnidadeSaude();
            const ids = await seedDatabase(u);
            const profissionalAt = profissionalValido(u)
            const profissional = await ProfissionalSaudeRepositorio.atualizar(ids[0], profissionalAt) as profissionalSaudeDocument;
            expect(profissional._id).toEqual(ids[0]);
            expect(profissional.nome).toEqual(profissionalAt.nome);
            expect(profissional.email).toEqual(profissionalAt.email);
            expect(profissional.telefone).toEqual(profissionalAt.telefone);
            expect(profissional.cargo).toEqual(profissionalAt.cargo);
            expect(profissional.registroProfissional).toEqual(profissionalAt.registroProfissional);
        });
        test('Deve apresentar erro ao atualizar id inexistente', async()=>{
            const u = await criarUnidadeSaude();
            const atualizacao = async()=>{await ProfissionalSaudeRepositorio.atualizar(idInexistente, profissionalValido(u))}
            expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()', ()=>{
        test('Deve remover um item e retorná-lo', async()=>{
            const u = await criarUnidadeSaude();
            const ids = await seedDatabase(u);
            const profissional = await ProfissionalSaudeRepositorio.deletar(ids[0]) as profissionalSaudeDocument;
            expect(profissional).toBeTruthy();
            expect(profissional._id).toEqual(ids[0]);
            expect(profissional).toHaveProperty('nome');
            expect(profissional).toHaveProperty('email');
            expect(profissional).toHaveProperty('telefone');
            expect(profissional).toHaveProperty('cargo');
            expect(profissional).toHaveProperty('registroProfissional');
            expect(profissional).toHaveProperty('unidadeSaude');
        });
        test('Deve apresentar erro ao excluir id inexistente', async()=>{
            const exclusao = async ()=>{await ProfissionalSaudeRepositorio.deletar(idInexistente)}
            await expect(exclusao).rejects.toThrow();
        })
    });
});


async function seedDatabase(u:UnidadeSaude):Promise<string[]>{
    const p1 = await profissionalSaudeModel.create({
        nome:"Profissional 1",
        email:'a@a.com',
        telefone:'(51) 9999-9999',
        registroProfissional: 'aa0000',
        cargo: 'med',
        unidadeSaude: u
    });
    const p2 = await profissionalSaudeModel.create({
        nome:"Profissional 2",
        email:'b@a.com',
        telefone:'(51) 9999-9991',
        registroProfissional: 'aa0001',
        cargo: 'enf',
        unidadeSaude: u
    });
    return [p1, p2].map(p=>p._id);
}

async function criarUnidadeSaude(){
    return unidadeSaudeModel.create({
        nome:'Unidade Teste',
        endereco:'rua a,12',
        telefone:'(51) 3434-3434'
    })
}

function profissionalValido(u:UnidadeSaude):ProfissionalSaude{
    return {
        nome:'Profissional Novo',
        email:'a@n.com',
        telefone:'(51)3333-2323',
        cargo: 'enf',
        registroProfissional: 'aa1111',
        unidadeSaude: u
    }
}

function profissionalCargoInvalido(u:UnidadeSaude):ProfissionalSaude{
    return {
        nome:'Profissional Novo',
        email:'a@n.com',
        telefone:'(51)3333-2323',
        cargo: 'dasoijdjaos',
        registroProfissional: 'aa1112',
        unidadeSaude: u
    }
}