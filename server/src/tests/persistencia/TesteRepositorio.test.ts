
import * as dbhandler from '../dbhandler';
import { pacienteModel } from '../../persistencia/pacienteModel';
import { Paciente } from '../../entidades/Paciente';
import { Teste } from '../../entidades/Teste';
import { testeModel, testeDocument } from '../../persistencia/testeModel';
import { atendimentoModel } from '../../persistencia/atendimentoModel';
import { unidadeSaudeModel } from '../../persistencia/unidadeSaudeModel';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { TesteRepositorio } from '../../persistencia/TesteRepositorio';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
const idInvalido = 'a';

describe('Teste Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar uma lista vazia', async()=>{
            const testes = await TesteRepositorio.listar();
            expect(testes).toEqual([]);
        });
        test('Deve retornar uma lista com 4 testes', async()=>{
            await seedDatabase();
            const testes = await TesteRepositorio.listar();
            expect(testes).toHaveLength(4);
        });
    });
    describe('listarPorId()', ()=>{
        test('Deve retornar um paciente', async()=>{
            const ids = await seedDatabase();
            const teste = await TesteRepositorio.listarPorId(ids[0]) as testeDocument;
            expect(teste._id).toEqual(ids[0]);
            expect(teste).toHaveProperty('tipo');
            expect(teste).toHaveProperty('resultado');
            expect(teste).toHaveProperty('paciente');
            expect(teste).toHaveProperty('data');    
            expect(teste).toHaveProperty('unidade');
            expect(teste).toHaveProperty('atendimento');
        });
        test('Deve retornar null para um id inexistente', async()=>{
            const teste = await TesteRepositorio.listarPorId(idInexistente);
            expect(teste).toBeNull();
        });
        test('Deve apresentar erro para um id inválido', async()=>{
            const busca = async()=>{await TesteRepositorio.listarPorId(idInvalido)}
            await expect(busca).rejects.toThrow();
        });
    });
    describe('listarPorPaciente()', ()=>{
        test('Deve retornar uma lista de pacientes', async()=>{
            const ids = await seedDatabase();
            const umTeste = await TesteRepositorio.listarPorId(ids[0]);
            const testes = await TesteRepositorio.listarPorPaciente(umTeste!.paciente.toString())
            expect(testes).toHaveLength(2);
            expect(testes[0].paciente).toEqual(umTeste!.paciente);
            expect(testes[1].paciente).toEqual(umTeste!.paciente);
        });
        test('Deve retornar null para um id inexistente', async()=>{
            const testes = await TesteRepositorio.listarPorPaciente(idInexistente);
            expect(testes).toEqual([]);
        });
    });
    describe('listarPorUnidade()', ()=>{
        test('Deve retornar uma lista de testes com a unidade pesquisada', async()=>{
            const ids = await seedDatabase();
            const teste = await TesteRepositorio.listarPorId(ids[0]);
            const idUnidade = teste!.unidade.toString();
            const unidades = await TesteRepositorio.listarPorUnidade(idUnidade);
            expect(unidades).toHaveLength(2);
            expect(unidades[0].unidade.toString()).toEqual(idUnidade);
            expect(unidades[1].unidade.toString()).toEqual(idUnidade);
        });
        test('Deve retornar um array vazio para unidade sem testes', async()=>{
            const unidade = await criarUnidade('Unidade Nova');
            const testes = await TesteRepositorio.listarPorUnidade(unidade._id);
            expect(testes).toEqual([]);
        });
    });
    describe('listarPositivos()', ()=>{
        test('Deve retornar testes positivos', async()=>{
            const ids = await seedDatabase();
            const testes = await TesteRepositorio.listarPositivos();
            expect(testes).toHaveLength(2);
            expect(testes[0].resultado).toEqual(true);
        });
        test('Deve retornar vazio', async()=>{
            const testes = await TesteRepositorio.listarPositivos();
            expect(testes).toEqual([]);
        });
    });
    describe('inserir()', ()=>{
        test('Deve inserir um objeto', async()=>{
            const teste = await testeValido();
            const testeInserido = await TesteRepositorio.inserir(teste);
            const testes = await TesteRepositorio.listar();
            expect(testes).toHaveLength(1)
            expect(testeInserido).toHaveProperty('_id');
            expect(testeInserido.tipo).toEqual(teste.tipo);
            expect(testeInserido.resultado).toEqual(teste.resultado);
            expect(testeInserido.paciente).toEqual(teste.paciente);
            expect(testeInserido.data).toEqual(teste.data);
            expect(testeInserido.unidade).toEqual(teste.unidade);
            expect(testeInserido.atendimento).toEqual(teste.atendimento);
        });
        test('Deve apresentar erro ao inserir teste com tipo inválido', async()=>{
            const testeInvalido = await testeTipoInvalido();
            const insercao = async()=>{await TesteRepositorio.inserir(testeInvalido)};
            await expect(insercao).rejects.toThrow();
        });
    });
    describe('atualizar()', ()=>{
        test('Deve atualizar um objeto', async()=>{
            const ids = await seedDatabase();
            const teste = await testeValido();
            const testeAtualizado = await TesteRepositorio.atualizar(ids[0], teste) as testeDocument;
            expect(testeAtualizado._id).toEqual(ids[0]);
            expect(testeAtualizado.tipo).toEqual(teste.tipo);
            expect(testeAtualizado.resultado).toEqual(teste.resultado);
            expect(testeAtualizado.paciente).toEqual(teste.paciente);
            expect(testeAtualizado.data).toEqual(teste.data);
            expect(testeAtualizado.unidade).toEqual(teste.unidade);
            expect(testeAtualizado.atendimento).toEqual(teste.atendimento);
        });
        test('Deve apresentar erro ao pesquisar id inexistente', async()=>{
            const teste = await testeValido()
            const atualizacao = async()=>{await TesteRepositorio.atualizar(idInexistente, teste)}
            await expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()', ()=>{
        test('Deve remover um objeto', async()=>{
            const ids = await seedDatabase();
            const teste = await TesteRepositorio.deletar(ids[0]) as testeDocument;
            const testes = await TesteRepositorio.listar();
            expect(teste._id).toEqual(ids[0]);
            expect(testes).toHaveLength(3);
        });
        test('Deve apresentar erro ao pesquisar id inexistente', async()=>{
            const remocao = async()=>{await TesteRepositorio.deletar(idInexistente)}
            await expect(remocao).rejects.toThrow();
        });
    });
    describe('testesPorDia()', ()=>{
        test('Deve apresentar relatorio de testes por dia', async()=>{
            await seedDatabase();
            const relatorio = await TesteRepositorio.testesPorDia();
            expect(relatorio).toHaveLength(2);
            expect(relatorio[0]).toHaveProperty('_id');
            expect(relatorio[0]).toHaveProperty('count');
        });
    });
});


async function seedDatabase():Promise<string[]>{
    const data = new Date(2020,1,1);
    const p1 = await criarPaciente('Paciente 1', '000.000.000-11', 'a@a.com');
    const p2 = await criarPaciente('Paciente 2', '000.000.000-12', 'b@b.com');
    const u1 = await criarUnidade('Unidade 1');
    const u2 = await criarUnidade('Unidade 2');
    const a1 = await criarAtendimento(p1, u1, 1*1000*60*60);
    const a2 = await criarAtendimento(p2, u2, 2*1000*60*60);
    const t1:testeDocument = await testeModel.create({
        tipo:"RT-PCR",
        resultado: false,
        paciente: p1,
        data: data,
        unidade: u1,
        atendimento: a1
    });
    const t2:testeDocument = await testeModel.create({
        tipo:"IgM/IgG",
        resultado: true,
        paciente: p1,
        data: data,
        unidade: u1,
        atendimento: a1
    });
    const t3:testeDocument = await testeModel.create({
        tipo:"RT-PCR",
        resultado: false,
        paciente: p2,
        data: data,
        unidade: u2,
        atendimento: a2
    });
    const t4:testeDocument = await testeModel.create({
        tipo:"RT-PCR",
        resultado: true,
        paciente: p2,
        data: new Date(data.getTime() + 1000*60*60*24),
        unidade: u2,
        atendimento: a2
    });
    return [t1,t2,t3,t4].map(t=>t._id);
}

function criarPaciente(nome:string, cpf:string, email:string){
    return pacienteModel.create({
        nome: nome,
        endereco: "Rua a, 2",
        cpf:cpf,
        telefone:"(51) 9999-9999",
        email:email,
        dataNascimento: new Date(1990,1,1)
    });
}

function criarUnidade(nome:string){
    return unidadeSaudeModel.create({
        nome:nome,
        endereco:"rua a, 0",
        telefone:"(41) 3333-3333"
    });
}

function criarAtendimento(p:Paciente,u:UnidadeSaude,t:number){
    const horarioFim = new Date();
    const horarioInicio = new Date(horarioFim.getTime() - t);
    return atendimentoModel.create({
        paciente:p,
        unidadeSaude: u,
        horarioInicio: horarioInicio,
        horarioFim: horarioFim,
        testes: [],
        ativo:false
    })
}

async function testeValido():Promise<Teste>{
    const p = await criarPaciente('Novo Paciente', '111.111.111-00', 'z@a.com');
    const u = await criarUnidade('Nova Unidade');
    const a = await criarAtendimento(p,u,1000*60*60)
    return {
        tipo:'RT-PCR',
        paciente: p,
        unidade:u,
        atendimento:a,
        resultado:true,
        data:new Date()
    };
}

async function testeTipoInvalido():Promise<Teste>{
    const p = await criarPaciente('Novo Paciente', '111.111.111-00', 'z@a.com');
    const u = await criarUnidade('Nova Unidade');
    const a = await criarAtendimento(p,u,1000*60*60)
    return {
        tipo:'a',
        paciente: p,
        unidade:u,
        atendimento:a,
        data:new Date()
    };
}