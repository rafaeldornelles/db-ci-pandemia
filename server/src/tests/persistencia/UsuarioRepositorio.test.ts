import * as dbhandler from '../dbhandler';
import { profissionalSaudeModel, profissionalSaudeDocument } from '../../persistencia/profissionalSaudeModel';
import { pacienteModel, pacienteDocument } from '../../persistencia/pacienteModel';
import { unidadeSaudeModel } from '../../persistencia/unidadeSaudeModel';
import { gestorModel, gestorDocument } from '../../persistencia/gestorModel';
import { usuarioDocument } from '../../persistencia/usuarioModel';
import { UsuarioRepositorio } from '../../persistencia/UsuarioRepositorio';
import { Usuario } from '../../entidades/Usuario';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Usuario Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar uma lista vazia', async ()=>{
            const usuarios = await UsuarioRepositorio.listar();
            expect(usuarios).toEqual([]);
        });
        test('Deve retornar uma lista com tamanho 6', async ()=>{
            await seedDatabase();
            const usuarios = await UsuarioRepositorio.listar();
            expect(usuarios).toHaveLength(6);
        });
    });

    describe('listarPorId()',()=>{
        test('Deve retornar usuário do tipo paciente com id correto', async ()=>{
            const ids = await seedDatabase();
            const idPaciente = ids[0]
            const paciente = await UsuarioRepositorio.listarPorId(idPaciente) as pacienteDocument;
            expect(paciente).not.toBeNull();
            expect(paciente._id).toEqual(idPaciente);
            expect((paciente as any).__t).toEqual("Paciente");
        });

        test('Deve retornar usuário do tipo profissionalSaude com id correto', async ()=>{
            const ids = await seedDatabase();
            const idProfissional = ids[2]
            const profissional = await UsuarioRepositorio.listarPorId(idProfissional) as profissionalSaudeDocument;
            expect(profissional).not.toBeNull();
            expect(profissional._id).toEqual(idProfissional);
            expect((profissional as any).__t).toEqual("ProfissionalSaude");
        });

        test('Deve retornar usuário do tipo Gestor com id correto', async ()=>{
            const ids = await seedDatabase();
            const idGestor = ids[4]
            const gestor = await UsuarioRepositorio.listarPorId(idGestor) as gestorDocument;
            expect(gestor).not.toBeNull();
            expect(gestor._id).toEqual(idGestor);
            expect((gestor as any).__t).toEqual("Gestor");
        });

        test('Deve apresentar erro ao buscar id invalido', async()=>{
            const idInvalido = 'a';
            await expect(async ()=>{await UsuarioRepositorio.listarPorId(idInvalido)}).rejects.toThrow();
        });
    });

    describe('listarPorEmail()', ()=>{
        test('Deve retornar um usuário com o email apresentado', async()=>{
            await seedDatabase();
            const email = 'a@a.com';
            const usuario = await UsuarioRepositorio.listarPorEmail(email) as Usuario;
            expect(usuario).not.toBeNull();
            expect(usuario.email).toEqual(email);
        });

        test('Deve retornar vazio', async()=>{
            await seedDatabase();
            const email = "sdhsaiudhauihdashdiasu@asuhidah.com";
            const usuario = await UsuarioRepositorio.listarPorEmail(email);
            expect(usuario).toBeNull();
        });
    });

    describe('deletar()', ()=>{
        test('deve excluir e retornar usuario', async()=>{
            const ids = await seedDatabase();
            const usuario = await UsuarioRepositorio.deletar(ids[0]) as usuarioDocument;
            const usuarios = await UsuarioRepositorio.listar();
            expect(usuarios).toHaveLength(5);
            expect(usuario._id).toEqual(ids[0])
        });
        test('deve apresentar erro ao excluir id inexistente', async()=>{
            await seedDatabase();
            const remocao = async() => {await UsuarioRepositorio.deletar('aaaaaaaaaaaaaaaaaaaaaaaa')};
            await expect(remocao).rejects.toThrow();
        })
    });

});


async function seedDatabase():Promise<string[]>{
    const unidadeSaude = await unidadeSaudeModel.create({
        nome: "US 01",
        endereco: "Rua h, 5",
        telefone: "(31) 5133-4431"
    })

    const p1 = await pacienteModel.create({
        nome: "John Doe",
        endereco: "rua a, 0",
        cpf: "000.321.123-22",
        email: "a@a.com",
        dataNascimento: new Date(1990, 1, 1),
        telefone: "(51) 3333-3333"
    });

    const p2 = await pacienteModel.create({
        nome: "John Doe II",
        endereco: "rua b, 0",
        cpf: "000.321.123-23",
        email: "b@a.com",
        dataNascimento: new Date(1990, 5, 1),
        telefone: "(51) 3333-3334"
    });

    const ps1 = await profissionalSaudeModel.create({
        nome: "John Doe II",
        email: "b@f.com",
        telefone: "(51) 3333-3334",
        cargo: "enf",
        registroProfissional: "aa 0011",
        unidadeSaude: unidadeSaude
    });

    const ps2 = await profissionalSaudeModel.create({
        nome: "John Doe III",
        email: "b@y.com",
        telefone: "(51) 3333-3335",
        cargo: "med",
        registroProfissional: "aa 0015",
        unidadeSaude: unidadeSaude
    });

    const g1 = await gestorModel.create({
        nome: "Gestor 1",
        telefone: "(51) 4349-3439",
        email: "gestor@hotmail.com",
        credencial: "512345"
    });

    const g2 = await gestorModel.create({
        nome: "Gestor 1",
        telefone: "(51) 4349-3439",
        email: "gestor2@hotmail.com",
        credencial: "512345"
    });

    return [p1, p2, ps1, ps2, g1, g2].map(u=> u._id);
}
