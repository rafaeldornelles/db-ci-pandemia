import * as dbhandler from '../dbhandler';
import { casoDocument, casoModel } from '../../persistencia/casoModel';
import { pacienteModel } from '../../persistencia/pacienteModel';
import { Paciente } from '../../entidades/Paciente';
import { testeModel } from '../../persistencia/testeModel';
import { atendimentoModel } from '../../persistencia/atendimentoModel';
import { unidadeSaudeModel } from '../../persistencia/unidadeSaudeModel';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { Atendimento } from '../../entidades/Atendimento';
import { CasoRepositorio } from '../../persistencia/CasoRepositorio';
import { Caso } from '../../entidades/Caso';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
const idInvalido = 'a';

describe('Caso Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar uma lista vazia', async()=>{
            const casos = await CasoRepositorio.listar();
            expect(casos).toEqual([]);
        });
        test('Deve retornar uma lista com 2 casos', async()=>{
            const params = await getParams();
            await seedDatabase(params);
            const casos = await CasoRepositorio.listar();
            expect(casos).toHaveLength(2);
        });
    });
    describe('listarPorId()', ()=>{
        test('Deve retornar um caso', async()=>{
            const params = await getParams();
            const ids = await seedDatabase(params);
            const caso = await CasoRepositorio.listarPorId(ids[0]) as casoDocument;
            expect(caso._id).toEqual(ids[0]);
            expect(caso).toHaveProperty("paciente");
            expect(caso).toHaveProperty("dataConfirmacao");
            expect(caso).toHaveProperty("ativo");
            expect(caso).toHaveProperty("teste");
        });
        test('Deve retornar null para id inexistente', async()=>{
            const caso = await CasoRepositorio.listarPorId(idInexistente);
            expect(caso).toBeNull();
        });
        test('Deve apresentar erro para id inválido', async()=>{
            const busca = async()=>{await CasoRepositorio.listarPorId(idInvalido);}
            await expect(busca).rejects.toThrow();
        });
    });
    describe('listarPorData()', ()=>{
        test('Deve retornar lista com 1 caso', async()=>{
            const params = await getParams();
            await seedDatabase(params);
            const dataBase = new Date(2020, 7,18);
            const casos = await CasoRepositorio.listarPorData(dataBase);
            expect(casos).toHaveLength(1);
            expect(casos[0].dataConfirmacao).toEqual(dataBase);
        });
        test('Deve retornar lista vazia', async()=>{
            const casos = await CasoRepositorio.listarPorData(new Date());
            expect(casos).toEqual([]);
        });
    });
    describe('quantidadePorDia()', ()=>{
        test('Deve retornar lista com data e quantidade de casos', async()=>{
            const params = await getParams();
            await seedDatabase(params);
            const relatorio = await CasoRepositorio.quantidadePorDia();
            expect(relatorio).toHaveLength(2);
            expect(relatorio[0]).toHaveProperty('_id');
            expect(relatorio[0]).toHaveProperty('count');
        });
    });
    describe('inserir()', ()=>{
        test('Deve inserir um item no banco de dados', async()=>{
            const params = await getParams();
            const caso:Caso= {
                paciente: params.paciente,
                dataConfirmacao: new Date(),
                teste: params.testes[0],
                ativo:true
            }
            const casoInserido = await CasoRepositorio.inserir(caso);
            expect(casoInserido).toHaveProperty('_id');
            expect(casoInserido.paciente).toEqual(caso.paciente);
            expect(casoInserido.dataConfirmacao).toEqual(caso.dataConfirmacao);
            expect(casoInserido.teste).toEqual(caso.teste);
            expect(casoInserido.ativo).toEqual(caso.ativo);
        });
        test('Deve apresentar erro ao inserir teste duplicado', async()=>{
            const params = await getParams();
            const caso:Caso= {
                paciente: params.paciente,
                dataConfirmacao: new Date(),
                teste: params.testes[0],
                ativo:true
            }
            await CasoRepositorio.inserir(caso);
            const insercao = async()=>{await CasoRepositorio.inserir(caso)}
            await expect(insercao).rejects.toThrow();
        })
    });
    describe('atualizar()', ()=>{
        test('Deve atualizar um caso', async()=>{
            const params = await getParams();
            const ids = await seedDatabase(params);
            const casoNovo:Caso = {
                paciente: params.paciente,
                dataConfirmacao: new Date(),
                teste: params.testes[0],
                ativo:false
            }
            const caso = await CasoRepositorio.atualizar(ids[0], casoNovo) as casoDocument;
            expect(caso).toBeTruthy();
            expect(caso._id).toEqual(ids[0]);
            expect(caso.ativo).toEqual(casoNovo.ativo);
        });
        test('Deve apresentar erro ao atualizar id inexistente', async()=>{
            const params = await getParams();
            const casoNovo:Caso = {
                paciente: params.paciente,
                dataConfirmacao: new Date(),
                teste: params.testes[0],
                ativo:false
            }
            const atualizacao = async()=> {await CasoRepositorio.atualizar(idInexistente, casoNovo)};
            await expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()', ()=>{
        test('Deve deletar um caso', async()=>{
            const params = await getParams();
            const ids = await seedDatabase(params);
            const caso = await CasoRepositorio.deletar(ids[0]);
            const casos = await CasoRepositorio.listar();
            expect(casos).toHaveLength(1);
            //@ts-ignore
            expect(caso._id).toEqual(ids[0]);
        });
        test('Deve apresentar erro ao deletar id inexistente', async()=>{
            const exclusao = async ()=>{await CasoRepositorio.deletar(idInexistente)};
            expect(exclusao).rejects.toThrow();
        });
    });
    describe('buscarCasoPorTeste()', ()=>{
        test('deve retornar um caso com o teste pesquisado', async()=>{
            const params = await getParams();
            await seedDatabase(params);
            const caso = await CasoRepositorio.buscarCasoPorTeste(params.testes[0]._id);
            expect(caso).toBeTruthy();
            expect(caso!.teste).toEqual(params.testes[0]._id);
        });
        test('deve retornar null ao pesquisar teste não existente', async()=>{
            const caso = await CasoRepositorio.buscarCasoPorTeste(idInexistente);
            expect(caso).toBeNull(); 
        });
    });
});


async function seedDatabase(params:any):Promise<string[]>{
    const dataBase = new Date(2020, 7,18);
    const c1 = await casoModel.create({
        paciente: params.paciente,
        dataConfirmacao: dataBase,
        ativo:true,
        teste:params.testes[0]
    });
    const c2 = await casoModel.create({
        paciente: params.paciente,
        dataConfirmacao: new Date(dataBase.getTime() - 1000*60*60*24),
        ativo:true,
        teste:params.testes[1]
    });
    return [c1,c2].map(c=>c._id)
}

async function getParams(){
    const paciente = await criarPaciente();
    const unidade = await criarUnidadeSaude();
    const atendimento = await criarAtendimento(unidade, paciente);
    const t1 = await criarTeste(paciente, atendimento, unidade);
    const t2 = await criarTeste(paciente, atendimento, unidade);
    const t3 = await criarTeste(paciente, atendimento, unidade);
    return {
        paciente: paciente,
        atendimento: atendimentoModel,
        testes: [t1, t2, t3]
    }
}

async function criarPaciente(){
    return pacienteModel.create({
        nome:'Paciente Seed',
        email:'seed@pac.com',
        telefone:'(51) 3222-2223',
        endereco:'rua b,3',
        cpf:'111.111.111-31',
        dataNascimento:new Date(1990,1,1),
        comorbidades:[]
    });
}

function criarTeste(paciente:Paciente, atendimento:Atendimento, unidade:UnidadeSaude){
    return testeModel.create({
        paciente: paciente,
        atendimento: atendimento,
        data: new Date(),
        resultado: true,
        unidade:unidade,
        tipo:'RT-PCR'
    });
}

async function criarAtendimento(unidadeSaude:UnidadeSaude, paciente:Paciente):Promise<Atendimento>{
    return atendimentoModel.create({
        horarioInicio: new Date(2020, 4, 19, 19, 20),
        ativo: true,
        paciente: paciente,
        unidadeSaude: unidadeSaude,
        testes: []
    });
}

async function criarUnidadeSaude(){
    return unidadeSaudeModel.create({
        nome: 'UBS 1',
        endereco: 'rua b, 1s',
        telefone: '33333333'
    })
}