import * as dbhandler from '../dbhandler';
import { UnidadeSaudeRepositorio } from '../../persistencia/UnidadeSaudeRepositorio';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { unidadeSaudeModel, unidadeSaudeDocument } from '../../persistencia/unidadeSaudeModel';
import { pacienteModel } from '../../persistencia/pacienteModel';
import { atendimentoModel } from '../../persistencia/atendimentoModel';


jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const unidadeValida:UnidadeSaude = {
    nome: 'unidade nova',
    endereco: 'rua A, 321',
    telefone: '51 3230998'
}

const unidadeEndInvalido:UnidadeSaude = {
    nome: 'unidade nova',
    endereco: 'ru',
    telefone: '51 3230998'
}

const unidadeTelInvalido:UnidadeSaude = {
    nome: 'unidade nova',
    endereco: 'rua A, 321',
    telefone: 'adsduai'
}

const unidadeNomeInvalido:UnidadeSaude = {
    nome: 'u1',
    endereco: 'rua A, 321',
    telefone: 'adsduai'
}

describe('Unidade Saude Repositorio', ()=>{
    describe('listar()', ()=>{
        test('deve retornar um array vazio', async ()=>{
            const unidades = await UnidadeSaudeRepositorio.listar();
            expect(unidades).toEqual([]);
        });
        test('deve retornar um array com tamanho 2', async()=>{
            await seedDatabase();
            const unidades = await UnidadeSaudeRepositorio.listar();
            expect(unidades).toHaveLength(2);
        });
    });

    describe('listarPorId()', ()=>{
        test('deve retornar uma unidade com o id informado', async()=>{
            const ids = await seedDatabase();
            const id= ids[0];
            const unidade = await UnidadeSaudeRepositorio.listarPorId(id) as unidadeSaudeDocument;
            expect(unidade._id).toEqual(id);
        });
        test('deve apresentar erro ao buscar id inválido', async()=>{
            await seedDatabase();
            const idInválido = 'a';
            await expect(async()=>{await UnidadeSaudeRepositorio.listarPorId(idInválido)}).rejects.toThrow();
        });
        test('deve retornar vazio ao buscar um id inexistente', async()=>{
            await seedDatabase();
            const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            const unidade = await UnidadeSaudeRepositorio.listarPorId(idInexistente)
            expect(unidade).toBeNull();
        });
    });

    describe('inserir()', ()=>{
        test('Deve inserir um objeto no banco de dados', async()=>{
            const unidade = await UnidadeSaudeRepositorio.inserir(unidadeValida);
            expect(unidade).toHaveProperty('_id');
            expect(unidade).toHaveProperty('nome');
            expect(unidade).toHaveProperty('endereco');
            expect(unidade).toHaveProperty('telefone');
        });
        test('Deve apresentar erro ao inserir objeto com telefone invalido', async()=>{
            const insercao = async() => {await UnidadeSaudeRepositorio.inserir(unidadeTelInvalido)};
            await expect(insercao).rejects.toThrow();
        });
        test('Deve apresentar erro ao inserir objeto com nome invalido', async()=>{
            const insercao = async() => {await UnidadeSaudeRepositorio.inserir(unidadeNomeInvalido)};
            await expect(insercao).rejects.toThrow();
        });
        test('Deve apresentar erro ao inserir objeto com endereco invalido', async()=>{
            const insercao = async() => {await UnidadeSaudeRepositorio.inserir(unidadeEndInvalido)};
            await expect(insercao).rejects.toThrow();
        });
    });

    describe('atualizar()', ()=>{
        test('Deve atualizar um objeto', async()=>{
            const ids = await seedDatabase();
            const unidade = await UnidadeSaudeRepositorio.atualizar(ids[0], unidadeValida) as unidadeSaudeDocument;
            expect(unidade._id).toEqual(ids[0]);
            expect(unidade.nome).toEqual(unidadeValida.nome);
            expect(unidade.endereco).toEqual(unidadeValida.endereco);
            expect(unidade.telefone).toEqual(unidadeValida.telefone);
        });
        test('Deve apresentar erro ao atualizar com id invalido', async()=>{
            await seedDatabase();
            const idInvalido = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            const atualização = async () =>{ await UnidadeSaudeRepositorio.atualizar(idInvalido, unidadeValida)}
            await expect(atualização).rejects.toThrow();
        });
        test('Deve apresentar erro ao atualizar para objeto invalido', async()=>{
            const ids = await seedDatabase();
            const atualização = async()=>{ await UnidadeSaudeRepositorio.atualizar(ids[0], unidadeNomeInvalido)}
            await expect(atualização).rejects.toThrow();
        });
    });

    describe('deletar()', ()=>{
        test('Deve remover e retornar item', async()=>{
            const ids = await seedDatabase();
            const unidade = await UnidadeSaudeRepositorio.deletar(ids[0]) as unidadeSaudeDocument;
            const unidades = await UnidadeSaudeRepositorio.listar();
            expect(unidades).toHaveLength(1);
            expect(unidade._id).toEqual(ids[0]);
        });
        test('Deve apresentar erro ao inserir id inexistente', async()=>{
            await seedDatabase();
            const remocao = async()=>{await UnidadeSaudeRepositorio.deletar('aaaaaaaaaaaaaaaaaaaaaaaa')};
            await expect(remocao).rejects.toThrow();
        })
    });

});


async function seedDatabase():Promise<string[]>{
    let u1 = await unidadeSaudeModel.create({
        nome: "Unidade 1",
        endereco: "rua u, 0",
        telefone: "(51) 3444-9999"
    });

    const u2 = await unidadeSaudeModel.create({
        nome: "Unidade 2",
        endereco: "rua u, 0",
        telefone: "(51) 3444-9999"
    });
    inserirAtendimentos(u1)
    return [u1, u2].map(u=>u._id);
}

async function inserirAtendimentos(unidade:unidadeSaudeDocument){
    const p = await pacienteModel.create({
        nome: "John Doe",
        endereco: "rua a, 0",
        cpf: "000.321.123-22",
        email: "a@a.com",
        dataNascimento: new Date(1990, 1, 1),
        telefone: "(51) 3333-3333"
    });
    const dataFim = new Date();
    const a1 = await atendimentoModel.create({
        horarioInicio: new Date(dataFim.getTime() - 2*60*60*1000), //2 horas
        horarioFim: dataFim,
        paciente: p,
        ativo: true,
        unidadeSaude: unidade,
        testes: []
    });
    const a2 = await atendimentoModel.create({
        horarioInicio: new Date(dataFim.getTime() - 1*60*60*1000), //1 hora
        horarioFim: dataFim,
        paciente: p,
        ativo: true,
        unidadeSaude: unidade,
        testes: []
    });
}