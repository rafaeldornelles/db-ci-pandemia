import * as dbhandler from '../dbhandler';
import { ComorbidadeRepositorio } from '../../persistencia/ComorbidadeRepositorio';
import { comorbidadeModel, comorbidadeDocument } from '../../persistencia/comorbidadeModel';
import { Comorbidade } from '../../entidades/Comorbidade';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
const idInvalido = 'a';
const comorbidadeValida:Comorbidade = {
    descricao: 'Hipertensão',
    cid: 'CID 10 - I15'
}

describe('Comorbidade Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar array vazio', async()=>{
            const comorbidades = await ComorbidadeRepositorio.listar();
            expect(comorbidades).toEqual([]);
        });
        test('Deve retornar um array de comorbidades', async()=>{
            await seedDatabase();
            const comorbidades = await ComorbidadeRepositorio.listar();
            expect(comorbidades).toHaveLength(2);
            expect(comorbidades[0]).toHaveProperty('descricao');
            expect(comorbidades[0]).toHaveProperty('cid');
        });
    });

    describe('listarPorId()', ()=>{
        test('Deve retornar um objeto com o id inserido', async()=>{
            const ids = await seedDatabase();
            const comorbidade = await ComorbidadeRepositorio.listarPorId(ids[0]) as comorbidadeDocument;
            expect(comorbidade._id).toEqual(ids[0]);
        });
        test('Deve retornar null ao buscar id inexistente', async()=>{
            const comorbidade = await ComorbidadeRepositorio.listarPorId(idInexistente);
            expect(comorbidade).toBeNull();
        });
        test('Deve apresentar erro ao buscar id inválido', async()=>{
            const busca = async()=>{await ComorbidadeRepositorio.listarPorId(idInvalido)};
            expect(busca).rejects.toThrow();
        });
    });

    describe('inserir()', ()=>{
        test('Deve inserir um item', async()=>{
            await seedDatabase();
            const comorbidade = await ComorbidadeRepositorio.inserir(comorbidadeValida);
            expect(comorbidade).toHaveProperty('_id');
            expect(comorbidade.descricao).toEqual(comorbidadeValida.descricao);
            expect(comorbidade.cid).toEqual(comorbidadeValida.cid);
            expect(await ComorbidadeRepositorio.listar()).toHaveLength(3);
        });
        test('Deve apresentar erro ao inserir comorbidade com mesmo cid', async()=>{
            await ComorbidadeRepositorio.inserir(comorbidadeValida);
            const insercaoRepetida = async ()=>{await ComorbidadeRepositorio.inserir(comorbidadeValida);}
            await expect(insercaoRepetida).rejects.toThrow();
        });
    });

    describe('atualizar()', ()=>{
        test('Deve astualizar um objeto', async()=>{
            const ids = await seedDatabase();
            const comorbidade = await ComorbidadeRepositorio.atualizar(ids[0], comorbidadeValida) as comorbidadeDocument;
            expect(comorbidade._id).toEqual(ids[0]);
            expect(comorbidade.descricao).toEqual(comorbidadeValida.descricao);
            expect(comorbidade.cid).toEqual(comorbidadeValida.cid);
        });
        test('Deve apresentar erro ao inserir id inexistente', async()=>{
            const insercao = async()=>{await ComorbidadeRepositorio.atualizar(idInexistente, comorbidadeValida)}
            await expect(insercao).rejects.toThrow();
        });
    });

    describe('deletar()', ()=>{
        test('Deve remover e retornar o objeto', async()=>{
            const ids = await seedDatabase();
            const comorbidade = await ComorbidadeRepositorio.deletar(ids[0]) as comorbidadeDocument;
            const comorbidades = await ComorbidadeRepositorio.listar();
            expect(comorbidades).toHaveLength(1);
            expect(comorbidade._id).toEqual(ids[0]);
            expect(comorbidade).toHaveProperty('descricao');
            expect(comorbidade).toHaveProperty('cid');
        });
        test('Deve apresentar erro ao remover id inexistente', async()=>{
            const remocao = async()=>{await ComorbidadeRepositorio.deletar(idInexistente)}
            await expect(remocao).rejects.toThrow();
        });
    });
});


async function seedDatabase():Promise<string[]>{
    const c1 = await comorbidadeModel.create({
        descricao:'Asma',
        cid:"CID 10 - J45"
    });
    const c2 = await comorbidadeModel.create({
        descricao:'Diabetes',
        cid:"CID 10 - E11"
    });
    
    return [c1,c2].map(c=>c._id);
}
