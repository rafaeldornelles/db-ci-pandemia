import * as dbhandler from '../dbhandler';
import { gestorModel, gestorDocument } from '../../persistencia/gestorModel';
import { GestorRepositorio } from '../../persistencia/GestorRepositorio';
import { Gestor } from '../../entidades/Gestor';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const gestorValido:Gestor = {
    nome:'Gestor novo',
    email:"gestorNovo@aa.com",
    telefone:"(51) 4443-2232",
    credencial:"GS0013"
}

describe('Gestor Repositorio', ()=>{
    describe('listar()', ()=>{
        test('Deve retornar um array vazio', async()=>{
            const gestores = await GestorRepositorio.listar();
            expect(gestores).toEqual([]);
        });
        test('Deve retornar um array com gestores', async()=>{
            await seedDatabase();
            const gestores = await GestorRepositorio.listar();
            expect(gestores).toHaveLength(2);
            expect(gestores[0]).toHaveProperty('nome');
            expect(gestores[0]).toHaveProperty('email');
            expect(gestores[0]).toHaveProperty('telefone');
            expect(gestores[0]).toHaveProperty('credencial');
        });
    });

    describe('listarPorId()', ()=>{
        test('deve retornar um gestor', async()=>{
            const ids = await seedDatabase();
            const gestor = await GestorRepositorio.listarPorId(ids[0]) as gestorDocument;
            expect(gestor._id).toEqual(ids[0]);
            expect(gestor).toHaveProperty('nome');
            expect(gestor).toHaveProperty('email');
            expect(gestor).toHaveProperty('telefone');
            expect(gestor).toHaveProperty('credencial');
        });
        test('deve apresentar erro ao buscar id inexistente', async()=>{
            const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            const gestor = await GestorRepositorio.listarPorId(idInexistente);
            expect(gestor).toBeNull();
        });
        test('deve apresentar erro ao buscar id inválido', async()=>{
            const idInvalido = 'a';
            const busca = async()=>{await GestorRepositorio.listarPorId(idInvalido)}
            await expect(busca).rejects.toThrow();
        });
    });
    describe('inserir()', ()=>{
        test('Deve inserir um gestor', async()=>{
            const gestor = await GestorRepositorio.inserir(gestorValido);
            expect(gestor).toHaveProperty('_id');
            expect(gestor.nome).toEqual(gestorValido.nome);
            expect(gestor.telefone).toEqual(gestorValido.telefone);
            expect(gestor.credencial).toEqual(gestorValido.credencial);
            expect(gestor.email).toEqual(gestorValido.email);
        });
    });
    describe('atualizar()', ()=>{
        test('Deve atualizar um gestor', async()=>{
            const ids = await seedDatabase();
            const gestor = await GestorRepositorio.atualizar(ids[0], gestorValido)as gestorDocument;
            expect(gestor._id).toEqual(ids[0]);
            expect(gestor.nome).toEqual(gestorValido.nome);
            expect(gestor.email).toEqual(gestorValido.email);
            expect(gestor.telefone).toEqual(gestorValido.telefone);
            expect(gestor.credencial).toEqual(gestorValido.credencial);
        });
        test('Deve apresentar erro ao atualizar id inexistente', async()=>{
            await seedDatabase();
            const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            const atualizacao = async()=>{await GestorRepositorio.atualizar(idInexistente, gestorValido)}
            await expect(atualizacao).rejects.toThrow();
        });
    });
    describe('deletar()', ()=>{
        test('Deve remover e retornar gestor', async()=>{
            const ids = await seedDatabase();
            const gestor = await GestorRepositorio.deletar(ids[0]);
            const gestores = await GestorRepositorio.listar();
            expect(gestores).toHaveLength(1);
            expect(gestor).toBeTruthy();
        });
        test('Deve apresentar erro ao inserir id inexistente', async()=>{
            const idInexistente = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            const remocao = async()=>{await GestorRepositorio.deletar(idInexistente)};
            await expect(remocao).rejects.toThrow();
        })
    })
});


async function seedDatabase():Promise<string[]>{
    const g1 = await gestorModel.create({
        nome:'Gestor1',
        credencial:"aa0111",
        email:"a@a.com",
        telefone:"(51) 3222-2222",
    });
    const g2 = await gestorModel.create({
        nome:'Gestor2',
        credencial:"aa0112",
        email:"b@a.com",
        telefone:"(51) 3222-2223",
    });
    return [g1,g2].map(g=>g._id);
}
