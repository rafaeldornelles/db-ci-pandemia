import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { UsuarioNegocios } from '../../negocios/UsuarioNegocios';
import { ProfissionalSaude } from '../../entidades/ProfissionalSaude';
import { Gestor } from '../../entidades/Gestor';
import { Usuario } from '../../entidades/Usuario';
import { Paciente } from '../../entidades/Paciente';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const mockUsuarios:(Gestor|Paciente|ProfissionalSaude)[] = [
    {
        nome:'Unidade 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        credencial: '1111'
    },
    {
        nome:'Unidade 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        credencial:'1112'
    },
    {
        nome:'Unidade 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        credencial:'1113'
    },
    {
        nome:'Paciente 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333',
        email:'a2@a.com',
        cpf:'000.000.000-77',
        // @ts-ignore
        dataNascimento: '1990-05-01'
    },
    {
        nome:'Paciente 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334',
        email:'b2@a.com',
        cpf:'000.000.000-76',
        // @ts-ignore
        dataNascimento: '1990-05-01'
    },
    {
        nome:'Profissional 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0000'
    },
    {
        nome:'Profissional 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'med',
        registroProfissional: 'aa0001'
    }
]

describe('Usuario controller', ()=>{
    describe('getUsuarios()', ()=>{
        test('Deve retornar lista de usuarios', async()=>{
            const mockUsuarioNegociosListar = jest
                .spyOn(UsuarioNegocios, 'listar')
                .mockResolvedValue(mockUsuarios);
            const result = await request(app).get('/api/usuarios');
            expect(mockUsuarioNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockUsuarios);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUsuarioNegociosListar = jest
                .spyOn(UsuarioNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/usuarios');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getUsuarioPorId()', ()=>{
        test('Deve retornar gestor com o id buscado', async()=>{
            const mockUsuarioNegociosBuscarPorId = jest
                .spyOn(UsuarioNegocios, 'buscarPorId')
                .mockResolvedValue(mockUsuarios[0]);
            const result = await request(app).get('/api/usuarios/id');
            expect(mockUsuarioNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockUsuarioNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockUsuarios[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUsuarioNegociosBuscarPorId = jest
                .spyOn(UsuarioNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/usuarios/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockUsuarioNegociosBuscarPorId = jest
                .spyOn(UsuarioNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/usuarios/id');
            expect(result.status).toEqual(404);
        });
    });
    describe('getUsuarioPorEmail()', ()=>{
        test('Deve retornar gestor com o email buscado', async()=>{
            const mockUsuarioNegociosBuscarPorEmail = jest
                .spyOn(UsuarioNegocios, 'buscarPorEmail')
                .mockResolvedValue(mockUsuarios[0]);
            const result = await request(app).get('/api/usuarios?e=email');
            expect(mockUsuarioNegociosBuscarPorEmail).toHaveBeenCalledTimes(1);
            expect(mockUsuarioNegociosBuscarPorEmail).toHaveBeenCalledWith('email');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockUsuarios[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUsuarioNegociosBuscarPorId = jest
                .spyOn(UsuarioNegocios, 'buscarPorEmail')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/usuarios?e=email');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockUsuarioNegociosBuscarPorId = jest
                .spyOn(UsuarioNegocios, 'buscarPorEmail')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/usuarios?e=email');
            expect(result.status).toEqual(404);
        });
    });
    describe('removerUsuario()', ()=>{
        test('Deve remover um gestor e retorná-lo', async()=>{
            const mockUsuarioNegociosRemoverUnidade = jest
                .spyOn(UsuarioNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockUsuarios[0]));
            const result = await request(app).delete('/api/usuarios/id');
            expect(result.body).toEqual(mockUsuarios[0]);
            expect(result.status).toEqual(200);
            expect(mockUsuarioNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUsuarioNegociosDeletarUnidade = jest
                .spyOn(UsuarioNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/usuarios/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})