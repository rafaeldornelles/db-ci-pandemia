import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { UnidadeSaude } from '../../entidades/UnidadeSaude';
import { UnidadeSaudeNegocios } from '../../negocios/UnidadeSaudeNegocios';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});


const mockUnidades:UnidadeSaude[] = [
    {
        nome:'Unidade 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333'
    },
    {
        nome:'Unidade 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334'
    },
    {
        nome:'Unidade 3',
        endereco: 'rua c, 3',
        telefone: '(51) 3333-3335'
    },
]

describe('UnidadeSaude controller', ()=>{
    describe('listarUnidades', ()=>{
        test('Deve retornar lista de unidades', async()=>{
            const mockUnidadeNegociosListar = jest
                .spyOn(UnidadeSaudeNegocios, 'listar')
                .mockResolvedValue(mockUnidades);
            const result = await request(app).get('/api/unidades');
            expect(mockUnidadeNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockUnidades);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosListar = jest
                .spyOn(UnidadeSaudeNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/unidades');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('buscarUnidadePorId()', ()=>{
        test('Deve retornar unidade com o id buscado', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(UnidadeSaudeNegocios, 'buscarPorId')
                .mockResolvedValue(mockUnidades[0]);
            const result = await request(app).get('/api/unidades/id');
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.body).toEqual(mockUnidades[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(UnidadeSaudeNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/unidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });

    });
    describe('inserirUnidade()', ()=>{
        test('Deve retornar a unidade inserida e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(UnidadeSaudeNegocios, 'inserir')
                .mockImplementation((unidade:UnidadeSaude) =>Promise.resolve(unidade));
            const result = await request(app).post('/api/unidades').send(mockUnidades[0]);
            expect(result.status).toEqual(201);
            expect(mockUnidaadeNegociosInserir).toHaveBeenCalledWith(mockUnidades[0]);
            expect(result.body).toEqual(mockUnidades[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosInserirUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/unidades');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atualizarUnidade()', ()=>{
        test('Deve atualizar uma unidade e retorna-la', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'atualizar')
                .mockImplementation((id:string, u:UnidadeSaude)=>Promise.resolve(u));
            const result = await request(app).put('/api/unidades/id').send(mockUnidades[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockUnidades[0]);
            expect(mockUnidadeNegociosAtualizarUnidade).toHaveBeenCalledWith('id', mockUnidades[0]);
        })
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'atualizar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/unidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarUnidade()', ()=>{
        test('Deve remover um objeto e retorná-lo', async()=>{
            const mockUnidadeNegociosRemoverUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockUnidades[0]));
            const result = await request(app).delete('/api/unidades/id');
            expect(result.body).toEqual(mockUnidades[0]);
            expect(result.status).toEqual(200);
            expect(mockUnidadeNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosDeletarUnidade = jest
                .spyOn(UnidadeSaudeNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/unidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})