import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { CasoNegocios } from '../../negocios/CasoNegocios';
import { Caso } from '../../entidades/Caso';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const mockCasos:Caso[] = [
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-01',
        ativo:true,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-02',
        ativo:true,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        dataConfirmacao: '2020-05-03',
        ativo:false,
        //@ts-ignore
        teste: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
];

const mockRelatorio = [{
    _id:'2020-01-01',
    count:12
},
{
    _id:'2020-01-02',
    count:13
}]

describe('Gestor controller', ()=>{
    describe('getCasos()', ()=>{
        test('Deve retornar lista de casos', async()=>{
            const mockCasosNegociosListar = jest
                .spyOn(CasoNegocios, 'listar')
                .mockResolvedValue(mockCasos);
            const result = await request(app).get('/api/casos');
            expect(mockCasosNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockCasos);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockCasosNegociosListar = jest
                .spyOn(CasoNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/casos');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('buscarCasoPorId()', ()=>{
        test('Deve retornar caso com o id buscado', async()=>{
            const mockCasosNegociosBuscarPorId = jest
                .spyOn(CasoNegocios, 'buscarPorId')
                .mockResolvedValue(mockCasos[0]);
            const result = await request(app).get('/api/casos/id');
            expect(mockCasosNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockCasosNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockCasos[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockCasosNegociosBuscarPorId = jest
                .spyOn(CasoNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/casos/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockCasosNegociosBuscarPorId = jest
                .spyOn(CasoNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/casos/id');
            expect(result.status).toEqual(404);
        });
    });  
    describe('quantidadeCasosPorDia()', ()=>{
        test('Deve retornar um relatorio', async()=>{
            const mockCasosNegociosRelatorio = jest
                .spyOn(CasoNegocios, 'quantidadePorDia')
                .mockResolvedValue(mockRelatorio);
            const result = await request(app).get('/api/casos/relatorio');
            expect(mockCasosNegociosRelatorio).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockRelatorio);
        });
        test('Deve retornar um status de erro', async()=>{
            const mockCasosNegociosBuscarPorData = jest
                .spyOn(CasoNegocios, 'quantidadePorDia')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/casos/relatorio');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('tornarCasoInativo()', ()=>{
        test('Deve retornar um caso', async()=>{
            const mockCasosNegociosTornarInativo = jest
                .spyOn(CasoNegocios, 'tornarInativo')
                .mockResolvedValue(mockCasos[0]);
            const result = await request(app).put('/api/casos/encerrar/id');
            expect(mockCasosNegociosTornarInativo).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockCasos[0]);
        });
        test('Deve retornar um status de erro', async()=>{
            const mockCasosNegociosTornarInativo = jest
                .spyOn(CasoNegocios, 'tornarInativo')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/casos/encerrar/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('bucarCasoPorData', ()=>{
        test('Deve retornar uma lista de casos', async()=>{
            const mockCasosNegociosBuscarPorData = jest
                .spyOn(CasoNegocios, 'buscarPorData')
                .mockResolvedValue(mockCasos);
            const result = await request(app).get('/api/casos/data/2020-01-01');
            expect(mockCasosNegociosBuscarPorData).toBeCalledWith(new Date('2020-01-01T00:00:00'));
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockCasos);
        });
        test('Deve retornar um status de erro', async()=>{
            const mockCasosNegociosBuscarPorData = jest
                .spyOn(CasoNegocios, 'buscarPorData')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/casos/data/2020-01-01');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarCaso', ()=>{
        test('Deve remover um gestor e retorná-lo', async()=>{
            const mockCasosNegociosRemoverUnidade = jest
                .spyOn(CasoNegocios, 'deletar')
                .mockResolvedValue(mockCasos[0]);
            const result = await request(app).delete('/api/casos/id');
          //  expect(result.body).toEqual(mockCasos[0]);
           // expect(result.status).toEqual(200);
            expect(mockCasosNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockCasosNegociosDeletarUnidade = jest
                .spyOn(CasoNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/casos/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
});