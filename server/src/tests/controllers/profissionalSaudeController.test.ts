import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { profissionalSaudeNegocios } from '../../negocios/ProfissionalSaudeNegocios';
import { ProfissionalSaude } from '../../entidades/ProfissionalSaude';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const mockProfissionais:ProfissionalSaude[] = [
    {
        nome:'Profissional 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0000'
    },
    {
        nome:'Profissional 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'med',
        registroProfissional: 'aa0001'
    },
    {
        nome:'Profissional 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        // @ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        cargo: 'enf',
        registroProfissional: 'aa0001'
    },
];

describe('ProfissionalSaude controller', ()=>{
    describe('getProfissionaisSaude()', ()=>{
        test('Deve retornar lista de profissionais', async()=>{
            const mockProfissionalSaudeNegociosListar = jest
                .spyOn(profissionalSaudeNegocios, 'listar')
                .mockResolvedValue(mockProfissionais);
            const result = await request(app).get('/api/profissionais');
            expect(mockProfissionalSaudeNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockProfissionais);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosListar = jest
                .spyOn(profissionalSaudeNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/profissionais');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getProfissionaisPorUnidade()', ()=>{
        test('Deve retornar profissionalis com a unidade buscada', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'listarPorUnidadeSaude')
                .mockResolvedValue(mockProfissionais);
            const result = await request(app).get('/api/profissionais/unidade/unidade');
            expect(mockProfissionalSaudeNegociosBuscarPorUnidade).toHaveBeenCalledTimes(1);
            expect(mockProfissionalSaudeNegociosBuscarPorUnidade).toHaveBeenCalledWith('unidade');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockProfissionais);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'listarPorUnidadeSaude')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/profissionais/unidade/unidade');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getProfissionaisPorRegistro()', ()=>{
        test('Deve retornar profissionalis com o registro buscado', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorRegistro = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorRegistroProfissional')
                .mockResolvedValue(mockProfissionais[0]);
            const result = await request(app).get('/api/profissionais/reg/reg');
            expect(mockProfissionalSaudeNegociosBuscarPorRegistro).toHaveBeenCalledTimes(1);
            expect(mockProfissionalSaudeNegociosBuscarPorRegistro).toHaveBeenCalledWith('reg');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockProfissionais[0]);
        });
        test('Deve retornar status 404', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorRegistro = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorRegistroProfissional')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/profissionais/reg/reg');
            expect(result.status).toEqual(404);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorRegistro = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorRegistroProfissional')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/profissionais/reg/reg');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getProfissionalPorId()', ()=>{
        test('Deve retornar profissional com o id buscado', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorId = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorId')
                .mockResolvedValue(mockProfissionais[0]);
            const result = await request(app).get('/api/profissionais/id');
            expect(mockProfissionalSaudeNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockProfissionalSaudeNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockProfissionais[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorId = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/profissionais/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockProfissionalSaudeNegociosBuscarPorId = jest
                .spyOn(profissionalSaudeNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/profissionais/id');
            expect(result.status).toEqual(404);
        });

    });
    describe('inserirProfissionalSaude()', ()=>{
        test('Deve retornar o profissional inserido e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(profissionalSaudeNegocios, 'inserir')
                .mockImplementation((p:ProfissionalSaude) =>Promise.resolve(p));
            const result = await request(app).post('/api/profissionais').send(mockProfissionais[0]);
            expect(result.status).toEqual(201);
            expect(mockUnidaadeNegociosInserir).toHaveBeenCalledWith(mockProfissionais[0]);
            expect(result.body).toEqual(mockProfissionais[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosInserirUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/profissionais');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atualizarUnidade()', ()=>{
        test('Deve atualizar um profissional e retorna-la', async()=>{
            const mockProfissionalSaudeNegociosAtualizarUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'atualizar')
                .mockImplementation((id:string, p:ProfissionalSaude)=>Promise.resolve(p));
            const result = await request(app).put('/api/profissionais/id').send(mockProfissionais[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockProfissionais[0]);
            expect(mockProfissionalSaudeNegociosAtualizarUnidade).toHaveBeenCalledWith('id', mockProfissionais[0]);
        })
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosAtualizarUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'atualizar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/profissionais/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarUnidade()', ()=>{
        test('Deve remover um profissional e retorná-lo', async()=>{
            const mockProfissionalSaudeNegociosRemoverUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockProfissionais[0]));
            const result = await request(app).delete('/api/profissionais/id');
            expect(result.body).toEqual(mockProfissionais[0]);
            expect(result.status).toEqual(200);
            expect(mockProfissionalSaudeNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockProfissionalSaudeNegociosDeletarUnidade = jest
                .spyOn(profissionalSaudeNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/profissionais/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})