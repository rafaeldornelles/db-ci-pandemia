import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { GestorNegocios } from '../../negocios/GestorNegocios';
import { ProfissionalSaude } from '../../entidades/ProfissionalSaude';
import { Gestor } from '../../entidades/Gestor';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const mockgestores:Gestor[] = [
    {
        nome:'Unidade 1',
        telefone: '(51) 3333-3333',
        email: 'a@a.com',
        credencial: '1111'
    },
    {
        nome:'Unidade 2',
        telefone: '(51) 3333-3332',
        email: 'v@a.com',
        credencial:'1112'
    },
    {
        nome:'Unidade 3',
        telefone: '(51) 3333-3333',
        email: 'c@a.com',
        credencial:'1113'
    },
];

describe('Gestor controller', ()=>{
    describe('getGestores()', ()=>{
        test('Deve retornar lista de gestores', async()=>{
            const mockGestorNegociosListar = jest
                .spyOn(GestorNegocios, 'listar')
                .mockResolvedValue(mockgestores);
            const result = await request(app).get('/api/gestores');
            expect(mockGestorNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockgestores);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockGestorNegociosListar = jest
                .spyOn(GestorNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/gestores');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getGestorPorId()', ()=>{
        test('Deve retornar gestor com o id buscado', async()=>{
            const mockGestorNegociosBuscarPorId = jest
                .spyOn(GestorNegocios, 'listarPorId')
                .mockResolvedValue(mockgestores[0]);
            const result = await request(app).get('/api/gestores/id');
            expect(mockGestorNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockGestorNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockgestores[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockGestorNegociosBuscarPorId = jest
                .spyOn(GestorNegocios, 'listarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/gestores/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockGestorNegociosBuscarPorId = jest
                .spyOn(GestorNegocios, 'listarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/gestores/id');
            expect(result.status).toEqual(404);
        });

    });
    describe('inserirGestor()', ()=>{
        test('Deve retornar o gestor inserido e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(GestorNegocios, 'inserir')
                .mockImplementation((g:Gestor) =>Promise.resolve(g));
            const result = await request(app).post('/api/gestores').send(mockgestores[0]);
            expect(result.status).toEqual(201);
            expect(mockUnidaadeNegociosInserir).toHaveBeenCalledWith(mockgestores[0]);
            expect(result.body).toEqual(mockgestores[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockGestorNegociosInserirUnidade = jest
                .spyOn(GestorNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/gestores');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atualizarGestor()', ()=>{
        test('Deve atualizar um gestor e retorna-l0', async()=>{
            const mockGestorNegociosAtualizarUnidade = jest
                .spyOn(GestorNegocios, 'atualizar')
                .mockImplementation((id:string, g:Gestor)=>Promise.resolve(g));
            const result = await request(app).put('/api/gestores/id').send(mockgestores[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockgestores[0]);
            expect(mockGestorNegociosAtualizarUnidade).toHaveBeenCalledWith('id', mockgestores[0]);
        })
        test('Deve enviar um status de erro', async()=>{
            const mockGestorNegociosAtualizarUnidade = jest
                .spyOn(GestorNegocios, 'atualizar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/gestores/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarGestor()', ()=>{
        test('Deve remover um gestor e retorná-lo', async()=>{
            const mockGestorNegociosRemoverUnidade = jest
                .spyOn(GestorNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockgestores[0]));
            const result = await request(app).delete('/api/gestores/id');
            expect(result.body).toEqual(mockgestores[0]);
            expect(result.status).toEqual(200);
            expect(mockGestorNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockGestorNegociosDeletarUnidade = jest
                .spyOn(GestorNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/gestores/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})