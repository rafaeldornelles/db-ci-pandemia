import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { ComorbidadeNegocios } from '../../negocios/ComorbidadeNegocios';
import { Paciente } from '../../entidades/Paciente';
import { Comorbidade } from '../../entidades/Comorbidade';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});


const mockComorbidades:Comorbidade[] = [
    {
        descricao:'Comorbidade 1',
        cid:'cid aa'
    },
    {
        descricao:'Comorbidade 2',
        cid:'cid bb'
    },
]

describe('Comorbidade controller', ()=>{
    describe('getComorbidades()', ()=>{
        test('Deve retornar lista de comorbidades', async()=>{
            const mockComorbidadeNegociosListar = jest
                .spyOn(ComorbidadeNegocios, 'listar')
                .mockResolvedValue(mockComorbidades);
            const result = await request(app).get('/api/comorbidades');
            expect(mockComorbidadeNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockComorbidades);
    
        });
        test('Deve enviar um status de erro', async()=>{
            const mockComorbidadeNegociosListar = jest
                .spyOn(ComorbidadeNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/comorbidades');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getComorbidadePorId()', ()=>{
        test('Deve retornar comorbidade com o id buscado', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(ComorbidadeNegocios, 'buscarPorId')
                .mockResolvedValue(mockComorbidades[0]);
            const result = await request(app).get('/api/comorbidades/id');
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.body).toEqual(mockComorbidades[0]);
        });
        test('Deve enviar um status 404', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(ComorbidadeNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/comorbidades/id');
            expect(result.status).toEqual(404);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(ComorbidadeNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/comorbidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('inserirComorbidade()', ()=>{
        test('Deve retornar a comorbidade inserida e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(ComorbidadeNegocios, 'inserir')
                .mockImplementation((c:Comorbidade) =>Promise.resolve(c));
            const result = await request(app).post('/api/comorbidades').send(mockComorbidades[0]);
            expect(result.status).toEqual(201);
            expect(result.body).toEqual(mockComorbidades[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosInserirUnidade = jest
                .spyOn(ComorbidadeNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/comorbidades');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atualizarComorbidade()', ()=>{
        test('Deve atualizar uma comorbidade e retorna-la', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(ComorbidadeNegocios, 'atualizar')
                .mockImplementation((id:string, c:Comorbidade)=>Promise.resolve(c));
            const result = await request(app).put('/api/comorbidades/id').send(mockComorbidades[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockComorbidades[0]);
        })
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(ComorbidadeNegocios, 'atualizar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/comorbidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarPaciente()', ()=>{
        test('Deve remover um objeto e retorná-lo', async()=>{
            const mockUnidadeNegociosRemoverUnidade = jest
                .spyOn(ComorbidadeNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockComorbidades[0]));
            const result = await request(app).delete('/api/comorbidades/id');
            expect(result.body).toEqual(mockComorbidades[0]);
            expect(result.status).toEqual(200);
            expect(mockUnidadeNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosDeletarUnidade = jest
                .spyOn(ComorbidadeNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/comorbidades/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})