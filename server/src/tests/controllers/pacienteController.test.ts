import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { PacienteNegocios } from '../../negocios/PacienteNegocios';
import { Paciente } from '../../entidades/Paciente';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});


const mockPacientes:Paciente[] = [
    {
        nome:'Paciente 1',
        endereco: 'rua a, 1',
        telefone: '(51) 3333-3333',
        email:'a@a.com',
        cpf:'000.000.000-77',
        dataNascimento: new Date(1990,1,1)
    },
    {
        nome:'Paciente 2',
        endereco: 'rua b, 2',
        telefone: '(51) 3333-3334',
        email:'b@a.com',
        cpf:'000.000.000-76',
        dataNascimento: new Date(1990,1,1)
    },
    {
        nome:'Paciente 3',
        endereco: 'rua c, 3',
        telefone: '(51) 3333-3335',
        email:'c@a.com',
        cpf:'000.000.000-75',
        dataNascimento: new Date(1990,1,1)
    },
]

describe('pacienteController controller', ()=>{
    describe('getPacientes()', ()=>{
        test('Deve retornar lista de pacientes', async()=>{
            const mockPacienteNegociosListar = jest
                .spyOn(PacienteNegocios, 'listar')
                .mockResolvedValue(mockPacientes);
            const result = await request(app).get('/api/pacientes');
            result.body.forEach((p:Paciente)=>{p.dataNascimento = new Date(p.dataNascimento)})
            expect(mockPacienteNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockPacientes);
    
        });
        test('Deve enviar um status de erro', async()=>{
            const mockPacienteNegociosListar = jest
                .spyOn(PacienteNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/pacientes');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getPacientePorId()', ()=>{
        test('Deve retornar paciente com o id buscado', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(PacienteNegocios, 'buscarPorId')
                .mockResolvedValue(mockPacientes[0]);
            const result = await request(app).get('/api/pacientes/id');
            result.body.dataNascimento = new Date(result.body.dataNascimento);
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockUnidadeNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.body).toEqual(mockPacientes[0]);
        });
        test('Deve enviar um status 404', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(PacienteNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/pacientes/id');
            expect(result.status).toEqual(404);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosBuscarPorId = jest
                .spyOn(PacienteNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/pacientes/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getPacientePorCpf()', ()=>{
        test('Deve retornar um paciente', async()=>{
            const mockUnidadeNegociosBuscarPorCpf = jest
                .spyOn(PacienteNegocios, 'buscarPorCpf')
                .mockResolvedValue(mockPacientes[0]);
            const result = await request(app).get('/api/pacientes/cpf/cpf');
            result.body.dataNascimento = new Date(result.body.dataNascimento);
            expect(mockUnidadeNegociosBuscarPorCpf).toHaveBeenCalledTimes(1);
            expect(mockUnidadeNegociosBuscarPorCpf).toHaveBeenCalledWith('cpf');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockPacientes[0]);
        });
        test('Deve retornar vazio para cpf inexistente', async()=>{
            const mockUnidadeNegociosBuscarPorCpf = jest
                .spyOn(PacienteNegocios, 'buscarPorCpf')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/pacientes/cpf/cpf');
            expect(result.status).toEqual(404);
        });
        test('Deve retornar código de erro para erro na busca', async()=>{
            const mockUnidadeNegociosBuscarPorCpf = jest
                .spyOn(PacienteNegocios, 'buscarPorCpf')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/pacientes/cpf/cpf');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('inserirPaciente()', ()=>{
        test('Deve retornar o paciente inserida e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(PacienteNegocios, 'inserir')
                .mockImplementation((paciente:Paciente) =>Promise.resolve(paciente));
            const result = await request(app).post('/api/pacientes').send(mockPacientes[0]);
            result.body.dataNascimento = new Date(result.body.dataNascimento);
            expect(result.status).toEqual(201);
            expect(result.body).toEqual(mockPacientes[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosInserirUnidade = jest
                .spyOn(PacienteNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/pacientes');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atualizarUnidade()', ()=>{
        test('Deve atualizar uma unidade e retorna-la', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(PacienteNegocios, 'atualizarDados')
                .mockImplementation((id:string, p:Paciente)=>Promise.resolve(p));
            const result = await request(app).put('/api/pacientes/id').send(mockPacientes[0]);
            result.body.dataNascimento = new Date(result.body.dataNascimento);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockPacientes[0]);
        })
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosAtualizarUnidade = jest
                .spyOn(PacienteNegocios, 'atualizarDados')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/pacientes/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('deletarPaciente()', ()=>{
        test('Deve remover um objeto e retorná-lo', async()=>{
            const mockUnidadeNegociosRemoverUnidade = jest
                .spyOn(PacienteNegocios, 'deletar')
                .mockImplementation((id:String) => Promise.resolve(mockPacientes[0]));
            const result = await request(app).delete('/api/pacientes/id');
            result.body.dataNascimento = new Date(result.body.dataNascimento);
            expect(result.body).toEqual(mockPacientes[0]);
            expect(result.status).toEqual(200);
            expect(mockUnidadeNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockUnidadeNegociosDeletarUnidade = jest
                .spyOn(PacienteNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/pacientes/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
})