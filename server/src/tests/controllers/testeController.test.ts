import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { TesteNegocios } from '../../negocios/TesteNegocios';
import { Caso } from '../../entidades/Caso';
import { Teste } from '../../entidades/Teste';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

const mockTestes:Teste[] = [
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-02',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-03',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    },
    {
        tipo:'RT-PCR',
        resultado: true,
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        data: '2020-05-01',
        //@ts-ignore
        unidade:'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa'
    }
];

const mockRelatorio = [{
    _id:'2020-01-01',
    count:12
},
{
    _id:'2020-01-02',
    count:13
}]

describe('Teste controller', ()=>{
    describe('listarTestes()', ()=>{
        test('Deve retornar lista de testes', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listar')
                .mockResolvedValue(mockTestes);
            const result = await request(app).get('/api/testes');
            expect(mockTestesNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('buscarTestePorId()', ()=>{
        test('Deve retornar teste com o id buscado', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'buscarPorId')
            .mockResolvedValue(mockTestes[0]);
            const result = await request(app).get('/api/testes/id');
            expect(mockTestesNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockTestesNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'buscarPorId')
            .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
        test('Deve enviar um status 404', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'buscarPorId')
            .mockResolvedValue(null);
            const result = await request(app).get('/api/testes/id');
            expect(result.status).toEqual(404);
        });
    });  
    describe('listarTestesPorPaciente()', ()=>{
        test('Deve retornar lista de testes', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPorPaciente')
                .mockResolvedValue(mockTestes);
            const result = await request(app).get('/api/testes/paciente/p');
            expect(mockTestesNegociosListar).toHaveBeenCalledWith('p');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPorPaciente')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes/paciente/p');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('listarTestesPorUnidade()', ()=>{
        test('Deve retornar lista de testes', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPorUnidade')
                .mockResolvedValue(mockTestes);
            const result = await request(app).get('/api/testes/unidade/u');
            expect(mockTestesNegociosListar).toHaveBeenCalledWith('u');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPorUnidade')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes/unidade/u');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('listarPositivos()', ()=>{
        test('Deve retornar lista de testes', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPositivos')
                .mockResolvedValue(mockTestes);
            const result = await request(app).get('/api/testes/positivos');
            expect(mockTestesNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosListar = jest
                .spyOn(TesteNegocios, 'listarPositivos')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes/positivos');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('TestesPorDia()', ()=>{
        test('Deve retornar um relatorio', async()=>{
            const mockTestesNegociosRelatorio = jest
                .spyOn(TesteNegocios, 'testesPorDia')
                .mockResolvedValue(mockRelatorio);
            const result = await request(app).get('/api/testes/relatorio');
            expect(mockTestesNegociosRelatorio).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockRelatorio);
        });
        test('Deve retornar um status de erro', async()=>{
            const mockTestesNegociosBuscarPorData = jest
                .spyOn(TesteNegocios, 'testesPorDia')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/testes/relatorio');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('inserirTeste()', ()=>{
        test('Deve retornar teste com o id buscado', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'inserir')
            .mockImplementation((t:Teste) => Promise.resolve(t));
            const result = await request(app).post('/api/testes').send(mockTestes[0]);
            expect(mockTestesNegociosBuscarPorId).toHaveBeenCalledWith(mockTestes[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'inserir')
            .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/testes').send(mockTestes[0]);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });  
    describe('atualizarTeste()', ()=>{
        test('Deve retornar caso com o id buscado', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'atualizar')
            .mockImplementation((id:string, t:Teste) => Promise.resolve(t));
            const result = await request(app).put('/api/testes/id').send(mockTestes[0]);
            expect(mockTestesNegociosBuscarPorId).toHaveBeenCalledWith('id', mockTestes[0]);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockTestes[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosBuscarPorId = jest
            .spyOn(TesteNegocios, 'atualizar')
            .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/testes/id').send(mockTestes[0]);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });  
    describe('deletarTeste()', ()=>{
        test('Deve remover um gestor e retorná-lo', async()=>{
            const mockTestesNegociosRemoverUnidade = jest
                .spyOn(TesteNegocios, 'deletar')
                .mockResolvedValue(mockTestes[0]);
            const result = await request(app).delete('/api/testes/id');
          //  expect(result.body).toEqual(mockTestes[0]);
           // expect(result.status).toEqual(200);
            expect(mockTestesNegociosRemoverUnidade).toHaveBeenCalledWith('id');
        });
        test('Deve enviar um status de erro', async()=>{
            const mockTestesNegociosDeletarUnidade = jest
                .spyOn(TesteNegocios, 'deletar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/testes/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
});