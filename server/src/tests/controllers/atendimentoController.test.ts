import request from 'supertest';
import * as dbhandler from '../dbhandler';
import app from '../../app';
import { AtendimentoNegocios } from '../../negocios/AtendimentoNegocios';
import { Paciente } from '../../entidades/Paciente';
import { Comorbidade } from '../../entidades/Comorbidade';
import { Atendimento } from '../../entidades/Atendimento';
import { EtapaAtendimento } from '../../entidades/EtapaAtendimento';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});


const mockAtendimentos:Atendimento[] = [
    {
        ativo:true,
        //@ts-ignore
        horarioInicio: '2020-01-01T12:00:00.000Z',
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
    {
        ativo:true,
        //@ts-ignore
        horarioInicio: '2020-01-01T12:00:00.000Z',
        //@ts-ignore
        paciente: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        //@ts-ignore
        unidadeSaude: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        etapas: [],
        testes: []
    },
];

const mockEtapa:EtapaAtendimento = {
    //@ts-ignore
    horarioInicio: '2020-01-01T12:00:00.000Z',
    //@ts-ignore
    horarioFim: '2020-01-01T12:30:00.000Z',
    //@ts-ignore
    atendimento: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    profissional: 'aaaaaaaaaaaaaaaaaaaaaaaa',
    //@ts-ignore
    descricao:'etapa ...'
}

const MockRelatorio =[
    {
        _id:'2020-01-01',
        count: 12
    },
    {
        _id:'2020-01-02',
        count: 10
    }
]

describe('Comorbidade controller', ()=>{
    describe('getatendimentos()', ()=>{
        test('Deve retornar lista de atendimentos', async()=>{
            const mockAtendimentoNegociosListar = jest
                .spyOn(AtendimentoNegocios, 'listar')
                .mockResolvedValue(mockAtendimentos);
            const result = await request(app).get('/api/atendimentos');
            expect(mockAtendimentoNegociosListar).toHaveBeenCalledTimes(1);
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockAtendimentos);
    
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosListar = jest
                .spyOn(AtendimentoNegocios, 'listar')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/atendimentos');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('getAtendimentoPorId()', ()=>{
        test('Deve retornar atendimento com o id buscado', async()=>{
            const mockAtendimentoNegociosBuscarPorId = jest
                .spyOn(AtendimentoNegocios, 'buscarPorId')
                .mockResolvedValue(mockAtendimentos[0]);
            const result = await request(app).get('/api/atendimentos/id');
            expect(mockAtendimentoNegociosBuscarPorId).toHaveBeenCalledTimes(1);
            expect(mockAtendimentoNegociosBuscarPorId).toHaveBeenCalledWith('id');
            expect(result.body).toEqual(mockAtendimentos[0]);
        });
        test('Deve enviar um status 404', async()=>{
            const mockAtendimentoNegociosBuscarPorId = jest
                .spyOn(AtendimentoNegocios, 'buscarPorId')
                .mockResolvedValue(null);
            const result = await request(app).get('/api/atendimentos/id');
            expect(result.status).toEqual(404);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosBuscarPorId = jest
                .spyOn(AtendimentoNegocios, 'buscarPorId')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/atendimentos/id');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('cadastrarAtendimento()', ()=>{
        test('Deve retornar o atendimento inserido e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(AtendimentoNegocios, 'inserir')
                .mockImplementation((a:Atendimento) =>Promise.resolve(a));
            const result = await request(app).post('/api/atendimentos').send(mockAtendimentos[0]);
            expect(result.status).toEqual(201);
            expect(result.body).toEqual(mockAtendimentos[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosInserir = jest
                .spyOn(AtendimentoNegocios, 'inserir')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/atendimentos');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('cadastrarEtapa()', ()=>{
        test('Deve retornar a etaoa inserida e status 201', async()=>{
            const mockUnidaadeNegociosInserir = jest
                .spyOn(AtendimentoNegocios, 'adicionarEtapa')
                .mockImplementation((id:string, e:EtapaAtendimento) =>Promise.resolve(mockAtendimentos[0]));
            const result = await request(app).post('/api/atendimentos/etapa').send(mockEtapa);
            expect(mockUnidaadeNegociosInserir).toHaveBeenCalledWith(mockEtapa.atendimento, mockEtapa);
            expect(result.status).toEqual(201);
            expect(result.body).toEqual(mockAtendimentos[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosInserir = jest
                .spyOn(AtendimentoNegocios, 'adicionarEtapa')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).post('/api/atendimentos/etapa').send(mockEtapa);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('removerEtapa()', ()=>{
        test('Deve retornar a etaoa inserida e status 201', async()=>{
            const mockUnidaadeNegociosRemoverEtapa = jest
                .spyOn(AtendimentoNegocios, 'removerEtapa')
                .mockResolvedValue(mockEtapa);
            const result = await request(app).delete('/api/atendimentos/etapa/id').send(mockEtapa);
            expect(mockUnidaadeNegociosRemoverEtapa).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockEtapa);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosRemoverEtapa = jest
                .spyOn(AtendimentoNegocios, 'removerEtapa')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).delete('/api/atendimentos/etapa/id').send(mockEtapa);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('encerrarAtendimento()', ()=>{
        test('Deve retornar a etaoa inserida e status 201', async()=>{
            const mockUnidaadeNegociosEncerrar = jest
                .spyOn(AtendimentoNegocios, 'encerrarAtendimento')
                .mockResolvedValue(mockAtendimentos[0]);
            const result = await request(app).put('/api/atendimentos/id/encerrar');
            expect(mockUnidaadeNegociosEncerrar).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockAtendimentos[0]);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosEncerrar = jest
                .spyOn(AtendimentoNegocios, 'encerrarAtendimento')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/atendimentos/id/encerrar').send(mockEtapa);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('encerrarEtapa()', ()=>{
        test('Deve retornar a etaoa inserida e status 201', async()=>{
            const mockUnidaadeNegociosEncerrarEtapa = jest
                .spyOn(AtendimentoNegocios, 'encerrarEtapaAtendimento')
                .mockResolvedValue(mockEtapa);
            const result = await request(app).put('/api/atendimentos/etapa/id/encerrar');
            expect(mockUnidaadeNegociosEncerrarEtapa).toHaveBeenCalledWith('id');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(mockEtapa);
        });
        test('Deve enviar um status de erro', async()=>{
            const mockAtendimentoNegociosEncerrarEtapa = jest
                .spyOn(AtendimentoNegocios, 'encerrarEtapaAtendimento')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).put('/api/atendimentos/etapa/id/encerrar').send(mockEtapa);
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
    describe('atendimentosPorDia()', ()=>{
        test('Deve retornar um relatorio', async()=>{
            const mockAtendimentoNegociosRelatorio = jest
                .spyOn(AtendimentoNegocios, 'atendimentosPorDia')
                .mockResolvedValue(MockRelatorio);
            const result = await request(app).get('/api/atendimentos/relatorio');
            expect(result.status).toEqual(200);
            expect(result.body).toEqual(MockRelatorio);
            expect(mockAtendimentoNegociosRelatorio).toHaveBeenCalledTimes(1);
        });
        test('Deve retornar um status de erro', async()=>{
            const mockAtendimentoNegociosRelatorio = jest
                .spyOn(AtendimentoNegocios, 'atendimentosPorDia')
                .mockRejectedValue(new Error('Erro'));
            const result = await request(app).get('/api/atendimentos/relatorio');
            expect(result.status).toBeGreaterThanOrEqual(300);
        });
    });
});