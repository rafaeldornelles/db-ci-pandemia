import { Document, Schema, SchemaType, SchemaTypes, Model, model } from "mongoose";
import { EtapaAtendimento } from "../entidades/EtapaAtendimento";

export interface etapaAtendimentoDocument extends EtapaAtendimento, Document{};

const etapaAtendimentoSchema = new Schema({
    descricao: {type:String, required: true},
    horarioInicio: {type:Date, required: true, default: Date.now},
    horarioFim: {type: Date, required: false},
    profissional: {type: SchemaTypes.ObjectId, ref:'ProfissionalSaude', required: true},
    atendimento: {type: SchemaTypes.ObjectId, ref:'Atendimento', required: true}
});

export const etapaAtendimentoModel:Model<etapaAtendimentoDocument> = model('EtapaAtendimento', etapaAtendimentoSchema, 'etapasatendimento');