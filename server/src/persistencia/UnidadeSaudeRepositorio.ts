import { unidadeSaudeModel } from "./unidadeSaudeModel";
import { UnidadeSaude } from "../entidades/UnidadeSaude";

export class UnidadeSaudeRepositorio{
    static async listar():Promise<UnidadeSaude[]>{
        return unidadeSaudeModel.find().exec();
    }

    static async listarPorId(id:string):Promise<UnidadeSaude|null>{
        return unidadeSaudeModel.findById(id).exec();
    }

    static async inserir(unidade:UnidadeSaude):Promise<UnidadeSaude>{
        return unidadeSaudeModel.create(unidade);
    }

    static async atualizar(id:string, unidade:UnidadeSaude):Promise<UnidadeSaude>{
        let unidadeExistente = await unidadeSaudeModel.findById(id).exec();
        if(unidadeExistente){
            unidadeExistente.nome = unidade.nome;
            unidadeExistente.endereco = unidade.endereco;
            unidadeExistente.telefone = unidade.telefone;

            return unidadeExistente.save();
        }
        throw new Error('id inexistente');
    }

    static async deletar(id:string):Promise<UnidadeSaude>{
        let unidadeExistente = await unidadeSaudeModel.findById(id).exec();
        if(unidadeExistente){
            return unidadeExistente.remove();
        }
        throw new Error('id inexistente');
    }
}