import { Document, Schema, SchemaTypes, model, Model } from "mongoose";
import { Caso } from "../entidades/Caso";

export interface casoDocument extends Caso, Document{};

const casoSchema = new Schema({
    paciente:{type:SchemaTypes.ObjectId, ref:"Paciente", required:true},
    dataConfirmacao:{type: Date, required:true, default: Date.now},
    ativo:{type:Boolean, required:true, default:true},
    teste:{type:SchemaTypes.ObjectId, ref:"Teste", required:true, unique:true}
});

export const casoModel:Model<casoDocument> = model<casoDocument>("Caso", casoSchema, "casos")