import { etapaAtendimentoModel } from "./etapaAtendimentoModel";
import { EtapaAtendimento } from "../entidades/EtapaAtendimento";

export class EtapaAtendimentoRepositorio{
    static async listar():Promise<EtapaAtendimento[]>{
        return etapaAtendimentoModel.find().populate('atendimento').exec();
    }

    static async listarPorId(id:string):Promise<EtapaAtendimento|null>{
        return etapaAtendimentoModel.findById(id).populate('atendimento').exec();
    }
    
    static async inserir(etapaAtendimento: EtapaAtendimento):Promise<EtapaAtendimento>{
        return etapaAtendimentoModel.create(etapaAtendimento);
    }

    static async atualizar(id:string, etapaAtendimento:EtapaAtendimento):Promise<EtapaAtendimento>{
        let etapaAtendimentoExistente = await etapaAtendimentoModel.findById(id).exec();
        if(etapaAtendimentoExistente){
            etapaAtendimentoExistente.descricao = etapaAtendimento.descricao;
            etapaAtendimentoExistente.horarioInicio = etapaAtendimento.horarioInicio;
            etapaAtendimentoExistente.horarioFim = etapaAtendimento.horarioFim;
            etapaAtendimentoExistente.profissional = etapaAtendimento.profissional;
            etapaAtendimentoExistente.atendimento = etapaAtendimento.atendimento;

            return etapaAtendimentoExistente.save();
        }
        throw new Error('id inexistente');
    }
    
    static async deletar(id:string):Promise<EtapaAtendimento>{
        let etapaAtendimento = await etapaAtendimentoModel.findById(id).exec();
        if(etapaAtendimento){
            return etapaAtendimento.remove();
        }
        throw new Error('id inexistente');
    }

}