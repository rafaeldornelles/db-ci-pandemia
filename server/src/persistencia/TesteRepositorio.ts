import { testeModel } from "./testeModel";
import { Teste } from "../entidades/Teste";

export class TesteRepositorio{
    static async listar():Promise<Teste[]>{
        return testeModel.find().exec();
    }

    static async listarPorId(id:string):Promise<Teste|null>{
        return testeModel.findById(id).exec();
    }

    static async listarPorPaciente(idPaciente:string):Promise<Teste[]>{
        return testeModel.find().where("paciente").equals(idPaciente).exec();
    }

    static async listarPorUnidade(idUnidade:string):Promise<Teste[]>{
        return testeModel.find().where("unidade").equals(idUnidade).exec();
    }

    static async listarPositivos():Promise<Teste[]>{
        return testeModel.find().where("resultado").equals(true).exec();
    }

    static async inserir(teste:Teste):Promise<Teste>{
        return testeModel.create(teste);
    }

    static async atualizar(id:string, teste:Teste):Promise<Teste>{
        let testeExistente = await testeModel.findById(id).exec();
        if(testeExistente){
            testeExistente.tipo = teste.tipo;
            testeExistente.paciente = teste.paciente;
            testeExistente.data = teste.data;
            if(teste.resultado){ 
                testeExistente.resultado = teste.resultado;
            }
            if(teste.unidade !=undefined){
                testeExistente.unidade = teste.unidade;
            }
            if(teste.atendimento !=undefined){
                testeExistente.atendimento = teste.atendimento;
            }
            return testeExistente.save();
        }else{
            throw new Error('id inexistente')
        }
    }

    static async deletar(id:string):Promise<Teste>{
        const teste = await testeModel.findById(id).exec();
        if(teste){
            return teste.remove();
        }else{
            throw new Error('id inexistente');
        }
    }

    static async testesPorDia(){
        return testeModel.aggregate([{ 
            $group : {
                _id: {
                    $dateToString: {date: "$data", format: "%Y-%m-%d"}
                },
                count: { $sum: 1 }
            }
        }]).sort('_id').exec();
    }
}