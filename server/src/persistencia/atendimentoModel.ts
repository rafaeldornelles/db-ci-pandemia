import { Atendimento } from "../entidades/Atendimento";
import { Document, Schema, SchemaTypes, Model, model } from "mongoose";
import { type } from "os";

export interface atendimentoDocument extends Atendimento, Document{};

const atendimentoSchema = new Schema({
    horarioInicio: {type: Date, required: true, default: Date.now},
    horarioFim: {type: Date, required: false},
    ativo: {type: Boolean, default: true},
    paciente: {type: SchemaTypes.ObjectId, ref:'Paciente', required:true},
    unidadeSaude: {type: SchemaTypes.ObjectId, ref:'UnidadeSaude', required:true},
    etapas: [{type: SchemaTypes.ObjectId, ref:'EtapaAtendimento'}],
    testes: [{type: SchemaTypes.ObjectId, ref: 'Teste'}]
});

export const atendimentoModel:Model<atendimentoDocument> = model('Atendimento', atendimentoSchema, 'atendimentos');