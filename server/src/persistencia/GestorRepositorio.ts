import { gestorModel } from "./gestorModel";
import { Gestor } from "../entidades/Gestor";

export class GestorRepositorio{
    static async listar():Promise<Gestor[]>{
        return gestorModel.find().exec();
    }

    static async listarPorId(id:string):Promise<Gestor|null>{
        return gestorModel.findById(id).exec();
    }

    static async inserir(gestor:Gestor):Promise<Gestor>{
        return gestorModel.create(gestor);
    }

    static async atualizar(id:string, gestor:Gestor):Promise<Gestor>{
        let gestorExistente = await gestorModel.findById(id).exec();
        if(gestorExistente){
            gestorExistente.nome = gestor.nome;
            gestorExistente.email = gestor.email;
            gestorExistente.telefone = gestor.telefone;
            gestorExistente.credencial = gestor.credencial;
            return gestorExistente.save();
        }
        throw new Error('id inexistente');
    }

    static async deletar(id:string):Promise<Gestor>{
        let gestorExistente = await gestorModel.findById(id).exec();
        if(gestorExistente){
            return gestorExistente.remove();
        }
        throw new Error('id inexistente');
    }
}