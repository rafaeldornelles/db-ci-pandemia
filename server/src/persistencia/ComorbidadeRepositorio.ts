import { comorbidadeModel } from "./comorbidadeModel";
import { Comorbidade } from "../entidades/Comorbidade";

export class ComorbidadeRepositorio{
    static async listar(): Promise<Comorbidade[]>{
        return comorbidadeModel.find().exec();
    }

    static async listarPorId(id: string): Promise<Comorbidade|null>{
        return comorbidadeModel.findById(id).exec();
    }

    static async inserir(comorbidade: Comorbidade):Promise<Comorbidade>{
        return comorbidadeModel.create(comorbidade);
    }

    static async atualizar(id:string, comorbidade:Comorbidade):Promise<Comorbidade>{
        let comorbidadeExistente = await comorbidadeModel.findById(id).exec();
        if (comorbidadeExistente){
            comorbidadeExistente.descricao = comorbidade.descricao;
            comorbidadeExistente.cid = comorbidade.cid;

            return comorbidadeExistente.save();
        }
        throw new Error('id inexistente');
    }

    static async deletar(id:string):Promise<Comorbidade>{
        let comorbidadeExistente = await comorbidadeModel.findById(id).exec();
        if(comorbidadeExistente){
            return comorbidadeExistente.remove();
        }
        throw new Error('id inexistente')
    }
}