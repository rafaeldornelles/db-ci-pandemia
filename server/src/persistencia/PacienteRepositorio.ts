import { pacienteModel } from "./pacienteModel";
import { Paciente } from "../entidades/Paciente";

export class PacienteRepositorio{
    static async listar():Promise<Paciente[]>{
        return pacienteModel.find().populate('comorbidades').exec();
    }

    static async listarPorId(id:string):Promise<Paciente|null>{
        return pacienteModel.findById(id).populate('comorbidades').exec();
    }

    static async listarPorCpf(cpf:string):Promise<Paciente|null>{
        return pacienteModel.findOne({"cpf":cpf}).populate('comorbidades').exec();
    }

    static async inserir(paciente:Paciente):Promise<Paciente>{
        return pacienteModel.create(paciente);
    }

    static async atualizar(id:string, paciente:Paciente):Promise<Paciente>{
        let pacienteExistente = await pacienteModel.findById(id).exec();
        if(pacienteExistente){
            pacienteExistente.nome = paciente.nome;
            pacienteExistente.email = paciente.email;
            pacienteExistente.telefone = paciente.telefone;
            pacienteExistente.endereco = paciente.endereco;
            pacienteExistente.cpf = paciente.cpf;
            pacienteExistente.dataNascimento = paciente.dataNascimento;
            if (paciente.comorbidades){
                pacienteExistente.comorbidades = paciente.comorbidades;
            }
            return pacienteExistente.save();
        }
        throw new Error('id inexistente');
    }

    static async deletar(id:string):Promise<Paciente>{
        let pacienteExistente = await pacienteModel.findById(id).exec();
        if(pacienteExistente){
            return pacienteExistente.remove();
        }
        throw new Error('id inexistente');
    }
}