import { usuarioModel } from "./usuarioModel";
import { Usuario } from "../entidades/Usuario";

export class UsuarioRepositorio{
    static async listar():Promise<Usuario[]>{
        return usuarioModel.find().exec();
    }
    
    static async listarPorId(id:string):Promise<Usuario|null>{
        return usuarioModel.findById(id).exec();
    }

    static async listarPorEmail(email:string):Promise<Usuario|null>{
        return usuarioModel.findOne({"email": email}).exec();
    }

    static async deletar(id:string):Promise<Usuario>{
        let usuarioExistente = await usuarioModel.findById(id);
        if(usuarioExistente){
            return usuarioExistente.remove();
        }
        throw new Error('id inexistente')
    }

    //Métodos inserir e atualizar são acessados na classe especializada
}