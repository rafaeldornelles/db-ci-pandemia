import { profissionalSaudeModel } from "./profissionalSaudeModel";
import { ProfissionalSaude } from "../entidades/ProfissionalSaude";

export class ProfissionalSaudeRepositorio{
    static async listar():Promise<ProfissionalSaude[]>{
        return profissionalSaudeModel.find().populate('unidadeSaude').exec();
    }

    static async listarPorUnidadeSaude(id:string):Promise<ProfissionalSaude[]>{
        return profissionalSaudeModel.find().where("unidadeSaude").equals(id).populate('unidadeSaude').exec();
    }

    static async listarPorId(id:string):Promise<ProfissionalSaude|null>{
        return profissionalSaudeModel.findById(id).populate('unidadeSaude').exec();
    }

    static async listarPorRegistroProfissional(registro:string):Promise<ProfissionalSaude|null>{
        return profissionalSaudeModel.findOne({"registroProfissional":registro}).populate('unidadeSaude').exec();
    }

    static async inserir(profissional:ProfissionalSaude):Promise<ProfissionalSaude>{
        return profissionalSaudeModel.create(profissional);
    }

    static async atualizar(id:string, profissional:ProfissionalSaude):Promise<ProfissionalSaude>{
        let profissionalExistente = await profissionalSaudeModel.findById(id).exec();
        if(profissionalExistente){
            profissionalExistente.nome = profissional.nome;
            profissionalExistente.email = profissional.email;
            profissionalExistente.telefone = profissional.telefone;
            profissionalExistente.cargo = profissional.cargo;
            profissionalExistente.registroProfissional = profissional.registroProfissional;
            profissionalExistente.unidadeSaude = profissional.unidadeSaude;
            return profissionalExistente.save();
        }
        throw new Error('id inexistente')
    }

    static async deletar(id:string):Promise<ProfissionalSaude>{
        let profissionalExistente = await profissionalSaudeModel.findById(id).exec();
        if(profissionalExistente){
            return profissionalExistente.remove();
        }
        throw new Error('id inexistente');
    }
}