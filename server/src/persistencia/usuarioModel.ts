import { Usuario } from "../entidades/Usuario";
import { Document, Schema, Model, model } from "mongoose";

export interface usuarioDocument extends Usuario, Document{};

const usuarioSchema = new Schema({
    nome: {type: String, required: true, minlength: 2, maxlength: 50},
    email: {type: String, required: true, validate: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, unique:true},
    telefone: {type: String, required: false, validate: /^[\d- ()]+$/}
});

export const usuarioModel: Model<usuarioDocument> = model<usuarioDocument>('Usuario', usuarioSchema, 'usuarios');