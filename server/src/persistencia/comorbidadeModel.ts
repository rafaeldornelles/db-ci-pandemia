import { Comorbidade } from "../entidades/Comorbidade";
import { Schema, Model, model, Document } from "mongoose";

export interface comorbidadeDocument extends Comorbidade, Document{};

const comorbidadeSchema = new Schema({
    descricao: {type:String, required:true, maxlength:50},
    cid: {type:String, required:true, unique: true}
});

export const comorbidadeModel:Model<comorbidadeDocument> = model<comorbidadeDocument>('Comorbidade', comorbidadeSchema, 'comorbidades');