import { Document, Schema, SchemaTypes, Model, model } from "mongoose";
import { Teste } from "../entidades/Teste"

export interface testeDocument extends Teste, Document{};

const testeSchema = new Schema({
    tipo: {type: String, enum:["RT-PCR", "IgM/IgG"], required:true, default:"RT-PCR"}, 
    resultado: {type: Boolean, required:false},
    paciente: {type: SchemaTypes.ObjectId, ref:"Paciente", required:true},
    data: {type: Date, required:true, default:Date.now},
    unidade: {type:SchemaTypes.ObjectId, ref:"UnidadeSaude", required:true},
    atendimento: {type: SchemaTypes.ObjectId, ref:"Atendimento", required:true}
});

export const testeModel:Model<testeDocument> = model<testeDocument>('Teste', testeSchema, 'testes');