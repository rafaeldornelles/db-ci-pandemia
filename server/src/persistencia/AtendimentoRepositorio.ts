import { atendimentoModel } from "./atendimentoModel";
import { Atendimento } from "../entidades/Atendimento";

export class AtendimentoRepositorio{
    static async listar(): Promise<Atendimento[]>{
        return atendimentoModel.find().populate({path:"etapas", populate:"profissional"}).populate('unidadeSaude').populate({path: "paciente", populate:"comorbidades"}).populate('testes').exec();
    }

    static async listarPorUnidade(idUnidade:string):Promise<Atendimento[]>{
        return atendimentoModel.find().where("unidadeSaude").equals(idUnidade)
            .populate({path:"etapas", populate:"profissional"}).populate('unidadeSaude').populate({path: "paciente", populate:"comorbidades"}).populate('testes').exec();
    }

    static async listarPorId(id:string):Promise<Atendimento|null>{
        return atendimentoModel.findById(id).populate({path:"etapas", populate:"profissional"}).populate('unidadeSaude').populate('testes').populate({path: "paciente", populate:"comorbidades"}).exec();
    }

    static async inserir(atendimento: Atendimento):Promise<Atendimento>{
        return atendimentoModel.create(atendimento);
    }

    static async atualizar(id:string, atendimento:Atendimento):Promise<Atendimento>{
        let atendimentoExistente = await atendimentoModel.findById(id).exec();
        if(atendimentoExistente){
            atendimentoExistente.horarioInicio = atendimento.horarioInicio;
            atendimentoExistente.ativo = atendimento.ativo;
            atendimentoExistente.paciente = atendimento.paciente;
            atendimentoExistente.unidadeSaude = atendimento.unidadeSaude;
            atendimentoExistente.etapas = atendimento.etapas;
            if(atendimento.horarioFim) {
                atendimentoExistente.horarioFim = atendimento.horarioFim;
            }
            if (atendimento.testes){
                atendimentoExistente.testes = atendimento.testes;
            }

            return atendimentoExistente.save();
        }
        throw new Error('id inexistente');
    }
    
    static async deletar(id:string):Promise<Atendimento>{
        let atendimento = await atendimentoModel.findById(id).exec();
        if (atendimento){
            return atendimento.remove();
        }      
        throw new Error('id inexistente');
    }

    static async quantidadePorDia():Promise<Object[]>{
        return atendimentoModel.aggregate([{ 
            $group : {
                _id: {
                    $dateToString: {date: "$horarioInicio", format: "%Y-%m-%d"}
                },
                count: { $sum: 1 }
            }
        }]).sort('_id').exec();
    }
    
}

