import { casoModel } from "./casoModel";
import { Caso } from "../entidades/Caso";

export class CasoRepositorio{
    static async listar():Promise<Caso[]>{
        return casoModel.find().exec();
    }

    static async listarPorId(id:string):Promise<Caso|null>{
        return casoModel.findById(id).exec();
    }

    static async listarPorData(data:Date):Promise<Caso[]>{
        const dataMaisUm = new Date(data.getTime() + 1000*60*60*24)
        const casos = await casoModel.find({
            dataConfirmacao: {
                $gte: data,
                $lt: dataMaisUm
            }
        }).exec();
        return casos;
    }

    static async quantidadePorDia():Promise<Object[]>{
        return casoModel.aggregate([{ 
            $group : {
                _id: {
                    $dateToString: {date: "$dataConfirmacao", format: "%Y-%m-%d"}
                },
                count: { $sum: 1 }
            }
        }]).sort("_id").exec();
    }

    static async inserir(caso:Caso):Promise<Caso>{
        return casoModel.create(caso);
    }

    static async atualizar(id:string, caso:Caso):Promise<Caso>{
        const casoExistente = await casoModel.findById(id).exec();
        if(casoExistente){
            casoExistente.ativo = caso.ativo;  
            return casoExistente.save();
        }else{
            throw new Error('id inexistente')
        }
    }

    static async deletar(id:string):Promise<Caso>{
        const caso = await casoModel.findById(id).exec();
        if(caso){
            return caso.remove();
        }else{
            throw new Error('id inexistente')
        }
    }

    static async buscarCasoPorTeste(idTeste:string):Promise<Caso|null>{
        return casoModel.findOne().where("teste").equals(idTeste).exec();
    }
}