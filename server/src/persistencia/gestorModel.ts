import { Gestor } from "../entidades/Gestor";
import { Document, Schema, Model } from "mongoose";
import { usuarioModel } from "./usuarioModel";

export interface gestorDocument extends Gestor, Document{};

const gestorSchema = new Schema({
    credencial: {type:String, required:true}
});

export const gestorModel:Model<gestorDocument> = usuarioModel.discriminator('Gestor', gestorSchema);