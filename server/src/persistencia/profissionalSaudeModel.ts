import { ProfissionalSaude } from "../entidades/ProfissionalSaude";
import { Document, Schema, Model, model, SchemaTypes } from "mongoose";
import { usuarioModel } from "./usuarioModel";

export interface profissionalSaudeDocument extends ProfissionalSaude, Document{};

const profissionalSaudeSchema = new Schema({
    cargo: {type:String, required:true, enum:["enf", "med", "tec", "sec"]},
    registroProfissional: {type:String, required:true, unique:true},
    unidadeSaude: {type:SchemaTypes.ObjectId, ref:'UnidadeSaude', required:true}
});

export const profissionalSaudeModel:Model<profissionalSaudeDocument> = usuarioModel.discriminator<profissionalSaudeDocument>('ProfissionalSaude', profissionalSaudeSchema);