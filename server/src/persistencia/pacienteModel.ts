import { Paciente } from "../entidades/Paciente";
import { Document, Schema, Model, SchemaTypes, model } from "mongoose";
import { usuarioModel } from "./usuarioModel";

export interface pacienteDocument extends Paciente, Document{};

const pacienteSchema = new Schema({
    endereco: {type: String, required: true, maxlength:100},
    cpf: {type:String, required:true, unique:true, validate:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/},
    dataNascimento: {type:Date, required:true},
    comorbidades: [{type: SchemaTypes.ObjectId, ref:'Comorbidade'}]
});

export const pacienteModel: Model<pacienteDocument> = usuarioModel.discriminator<pacienteDocument>('Paciente', pacienteSchema);