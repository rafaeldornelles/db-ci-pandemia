import { UnidadeSaude } from "../entidades/UnidadeSaude";
import { Schema, Document, Model, model } from "mongoose";

export interface unidadeSaudeDocument extends UnidadeSaude, Document{};

const unidadeSaudeSchema = new Schema({
    nome: {type: String, required: true, maxlength: 50, minlength: 3},
    endereco: {type: String, required: true, maxlength: 100, minlength: 3},
    telefone: {type: String, required: true, maxlength: 15, minlength: 3, validate: /^[\d ()-]+$/}
});

export const unidadeSaudeModel:Model<unidadeSaudeDocument> = model<unidadeSaudeDocument>('UnidadeSaude', unidadeSaudeSchema, 'unidadessaude');