import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import atendimentoRouter from './routers/atendimento.routes';
import comorbidadeRouter from './routers/comorbidade.routes';
import gestorRouter from './routers/gestor.routes';
import pacienteRouter from './routers/paciente.routes';
import profissionalSaudeRouter from './routers/profissionalSaude.routes';
import unidadeSaudeRouter from './routers/unidadeSaude.routes';
import usuarioRouter from './routers/usuario.routes';
import testeRouter from './routers/teste.routes';
import casoRouter from './routers/caso.routes';

const app = express();
app.set('port', process.env.PORT||3000);

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/atendimentos', atendimentoRouter);
app.use('/api/comorbidades', comorbidadeRouter);
app.use('/api/gestores', gestorRouter);
app.use('/api/pacientes', pacienteRouter);
app.use('/api/profissionais', profissionalSaudeRouter);
app.use('/api/unidades', unidadeSaudeRouter);
app.use('/api/usuarios', usuarioRouter);
app.use('/api/testes', testeRouter);
app.use('/api/casos', casoRouter);


if (process.env.NODE_ENV === 'development') {
    app.use(errorHandler());
}


export default app;