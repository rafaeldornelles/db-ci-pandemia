import { ProfissionalSaude } from "./ProfissionalSaude";
import { Atendimento } from "./Atendimento";

export interface EtapaAtendimento{
    descricao: string,
    horarioInicio: Date,
    horarioFim?: Date,
    profissional: ProfissionalSaude,
    atendimento: Atendimento
}