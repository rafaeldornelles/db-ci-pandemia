import { Paciente } from "./Paciente";
import { Teste } from "./Teste";

export interface Caso{
    paciente: Paciente,
    dataConfirmacao: Date,
    ativo:boolean, 
    teste: Teste
}