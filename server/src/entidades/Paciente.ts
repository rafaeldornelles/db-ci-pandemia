import { Usuario } from "./Usuario";
import { Comorbidade } from './Comorbidade'

export interface Paciente extends Usuario{
    endereco: string,
    cpf: string,
    dataNascimento: Date,
    comorbidades?: Comorbidade[]
}