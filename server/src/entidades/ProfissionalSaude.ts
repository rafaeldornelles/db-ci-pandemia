import { Usuario } from "./Usuario";
import { UnidadeSaude } from "./UnidadeSaude"

export interface ProfissionalSaude extends Usuario{
    cargo: string
    registroProfissional: string,
    unidadeSaude: UnidadeSaude
}