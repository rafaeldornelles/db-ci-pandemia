export interface UnidadeSaude{
    nome: string,
    endereco: string,
    telefone: string,
    estatisticas?:{
        tempoMinimo:number,
        tempoMedio:number,
        tempoMaximo:number
    },
    atendimentosCount?:number,
    ultimoDiaCount?:number,
    penultimoDiaCount?:number
}