import { Paciente } from "./Paciente";
import { UnidadeSaude } from "./UnidadeSaude";
import { Atendimento } from "./Atendimento";

export interface Teste{
    tipo: string, 
    resultado?: boolean,
    unidade: UnidadeSaude,
    paciente: Paciente,
    data: Date,
    atendimento: Atendimento
}