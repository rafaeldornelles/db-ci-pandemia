import { connect } from "mongoose";
import app from "./app";

async function main(){
    try{
        const url = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`;
        const conection = await connect(url,  { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('conexao bem sucedida');

        const port = app.get('port');
        const env = app.get('env');
        app.listen(port, ()=> {
            console.log(`Rodando na porta ${port}`);
            console.log(`Express no modo: ${env}`);
        })


    }catch{
        console.log('erro');
    }
}

main();