# DBServer + CI Desafio Pandemia
### Sobre
Sistema desenvolvido durante o programa de estágio da DbServer em parceria com o Centro de Inovação da PUCRS.
O objetivo do projeto é oferecer uma solução para permitir que, durante uma epidemia, a população geral e as autoridades de saúde acompanhem a evolução de atendimentos, testes e casos em uma cidade. O programa foi desenvolvido em Javascript utilizando Node e Angular. Integrantes: **Rafael** e **Thomas**

### Como Executar
1. Baixar e instalar as dependências utilizando o comando `npm run install` na pasta raiz. Esse comando irá instalar os pacotes do servidor e do cliente.
1. Abrir dois terminais, um deles irá rodar o servidor e o outro o cliente.
1. No terminal do cliente, utilizar o comando `npm run client`.
1. No terminal do servidor, executar com `nom run server`.
1. O sistema está pronto para ser utilizado em [localhost:4200](http://localhost:4200/)

### Sobre a solução
![Diagrama de Entidades](https://gitlab.com/rafaeldornelles/db-ci-pandemia/-/raw/master/entidades.png)
A solução adotada se baseou na separação das entidades em classes distintas para permitir um armazenamento eficiente das informações utilizando o **MongoDb**.

O servidor foi implementado em **NodeJs** com uma arquitetura baseada em entidades, persistência, negócios, controladores e rotas. 

Os dados da camada de persistência são consumidos pelo cliente, implementado em **Angular**, através de serviços que acessam a API do servidor.

### Referências
###### Sistemas de visualização de dados sobre a pandemia
* [Infográfico covid procempa](https://infografico-covid.procempa.com.br/)
* [Painel do covid no RS](https://ti.saude.rs.gov.br/covid19/)
* [Painel Covid Brasil](https://covid.saude.gov.br/) 
* [Worldometers](https://www.worldometers.info/coronavirus/country/brazil/)
* [Our World in Data](https://ourworldindata.org/coronavirus/country/brazil?country=~BRA)

###### Boletins Epidemiológicos e protocolos
* [Boletim da PMPA](http://lproweb.procempa.com.br/pmpa/prefpoa/sms/usu_doc/2020_06_07_boletim_covid_sms_77.pdf)
* [PROTOCOLO DE MANEJO CLÍNICO DO CORONAVÍRUS (COVID-19) NA ATENÇÃO PRIMÁRIA À SAÚDE](https://coronavirus.ceara.gov.br/wp-content/uploads/2020/03/20200504_ProtocoloManejo_ver09-1.pdf)
* [INFORMAÇÔES SOBRE MANEJO CLINICO](https://coronavirus.saude.gov.br/manejo-clinico-e-tratamento)

### Serviços e Bibliotecas Adicionais Utilizados
* [ApexCharts](https://apexcharts.com/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/components/forms/)
* [Angular Material](https://material.angular.io/)

### Trello da Dupla
[Confira aqui](https://trello.com/b/tEqvl9rX/rafael-e-thomas-pandemia-challenge)