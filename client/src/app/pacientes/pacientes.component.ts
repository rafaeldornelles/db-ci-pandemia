import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../services/paciente.service';
import { Paciente } from '../entidades/Paciente';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {
  public pacientes
  constructor(
    public service: PacienteService
  ) { }

  ngOnInit(): void {
    this.service.getPacientes().subscribe({
      next: pacientes => this.pacientes = pacientes
    });
  }

  getIdade(paciente:Paciente){
    const now = new Date();
    const dataNascimento = new Date(paciente.dataNascimento);
    let idade = now.getFullYear() - dataNascimento.getFullYear();
    if (now.getMonth() < dataNascimento.getMonth()){
      idade = idade - 1
    } else if(now.getMonth() == dataNascimento.getMonth()){
      if(now.getDate() < dataNascimento.getDate()){
        idade = idade - 1
      }
    }

    return idade;
  }

  getComorbidadeString(paciente:Paciente){
    const comorbidades = paciente.comorbidades.map(c=>c.descricao);
    return comorbidades.join(', ')
  }

}
