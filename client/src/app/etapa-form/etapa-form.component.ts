import { Component, OnInit } from '@angular/core';
import { ProfissionalSaude } from '../entidades/ProfissionalSaude';
import { ProfissionalService } from "../services/profissional.service"
import { FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { EtapaAtendimento } from '../entidades/EtapaAtendimento';
import { ActivatedRoute, Router } from '@angular/router';
import { map, concatMap } from 'rxjs/operators';
import { AtendimentoService } from '../services/atendimento.service';
import { Atendimento } from '../entidades/Atendimento';

@Component({
  selector: 'app-etapa-form',
  templateUrl: './etapa-form.component.html',
  styleUrls: ['./etapa-form.component.css']
})
export class EtapaFormComponent implements OnInit {
  public profissionais:ProfissionalSaude[]
  public now:Date = new Date();
  public atendimento:Atendimento
  constructor(
    public profissionalService: ProfissionalService,
    public fb: FormBuilder,
    public datePipe: DatePipe,
    public route: ActivatedRoute,
    public atendimentoService: AtendimentoService,
    public router:Router
  ) { }

  public formGroup = this.fb.group({
    profissional: ['', Validators.required],
    descricao: ["", Validators.required],
    dia: [this.datePipe.transform(this.now, 'yyyy-MM-dd')],
    hora: [this.datePipe.transform(this.now, 'hh:mm')]
  })

  ngOnInit(): void {
    this.profissionalService.getProfissionais().subscribe({
      next: profissionais => this.profissionais = profissionais
    });

    this.route.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      concatMap(id => this.atendimentoService.getAtendimentoById(id))
    ).subscribe({
      next: atendimento => this.atendimento = atendimento
    })
  }

  enviarForm(){
    let etapa:EtapaAtendimento = {
      descricao: this.descricao.value,
      horarioInicio: new Date(`${this.dia.value}T${this.hora.value}`),
      profissional: this.profissional.value,
      atendimento: this.atendimento
    }
    this.atendimentoService.insertEtapa(etapa).subscribe({
      next: etapa => {this.router.navigate(['../atendimento', this.atendimento._id])}
    });
  }

  get profissional(){
    return this.formGroup.get('profissional');
  }

  get descricao(){
    return this.formGroup.get('descricao');
  }

  get dia(){
    return this.formGroup.get('dia');
  }

  get hora(){
    return this.formGroup.get('hora')
  }
}
