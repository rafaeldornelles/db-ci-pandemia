import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../services/paciente.service';
import { ComorbidadeService } from '../services/comorbidade.service'
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Comorbidade } from '../entidades/Comorbidade';
import { Paciente } from '../entidades/Paciente';

@Component({
  selector: 'app-paciente-form',
  templateUrl: './paciente-form.component.html',
  styleUrls: ['./paciente-form.component.css']
})
export class PacienteFormComponent implements OnInit {
  public comorbidadesLista: Comorbidade[]
  constructor(
    public pacienteService:PacienteService,
    public comorbService: ComorbidadeService,
    public router: Router,
    public fb: FormBuilder
  ) { }

  public formGroup = this.fb.group({
    nome: ['', Validators.required],
    endereco: ['', Validators.required],
    cpf: ['', [Validators.required, Validators.pattern(/^[0-9]{3}.[0-9]{3}.[0-9]{3}[-/][0-9]{2}$/)]], 
    dataNascimento: ['', Validators.required],
    comorbidades: [''],
    email: ['', [Validators.required, Validators.email]],
    telefone: ['', Validators.required]
  });

  ngOnInit(): void {
    this.comorbService.getComorbidades().subscribe({
      next: comorb => {
        this.comorbidadesLista = comorb;
      }
    })
  }

  enviarForm(){
    const paciente:Paciente = {
      nome: this.nome.value,
      endereco: this.endereco.value,
      cpf: this.cpf.value,
      dataNascimento: this.dataNascimento.value,
      email:this.email.value,
      telefone:this.telefone.value,
    }
    this.pacienteService.inserirPaciente(paciente).subscribe({
      next: paciente=>{
        this.router.navigate(['../pacientes'])
      }
    })
  }

  get nome(){
    return this.formGroup.get('nome')
  }
  get endereco(){
    return this.formGroup.get('endereco')
  }
  get cpf(){
    return this.formGroup.get('cpf')
  }
  get dataNascimento(){
    return this.formGroup.get('dataNascimento')
  }
  get email(){
    return this.formGroup.get('email')
  }
  get telefone(){
    return this.formGroup.get('telefone')
  }
  get comorbidades(){
    return this.formGroup.get('comorbidades')
  }

  onCheckChange(e){
    let comorbidadesArray = this.comorbidades.value;
    if(e.target.checked){
      comorbidadesArray.push(e.target.textContent)
      this.comorbidades.setValue(comorbidadesArray)
    }
  }
}
