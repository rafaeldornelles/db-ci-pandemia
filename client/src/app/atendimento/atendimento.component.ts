import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AtendimentoService } from "../services/atendimento.service"
import { Atendimento } from '../entidades/Atendimento';
import { Relatorio } from '../entidades/Relatorio';

@Component({
  selector: 'app-atendimento',
  templateUrl: './atendimento.component.html',
  styleUrls: ['./atendimento.component.css']
})
export class AtendimentoComponent implements OnInit {
  public atendimentos: Atendimento[];
  public relatorios: Relatorio[] =[];
  constructor(
    public atendimentoService: AtendimentoService,
    public router:Router
  ) { }

  ngOnInit(): void {
    this.atendimentoService.getAtendimentos().subscribe({
      next: atendimentos => this.atendimentos = atendimentos
    });

    this.atendimentoService.getRelatorio().subscribe({
      next: relatorios => this.relatorios = relatorios
    })
  }

  mostrarDetalhes(id:string){
    this.router.navigate(["atendimento", id]);
  }

}
