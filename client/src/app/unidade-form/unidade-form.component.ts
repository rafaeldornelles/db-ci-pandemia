import { Component, OnInit } from '@angular/core';
import { UnidadeService } from '../services/unidade.service';
import { FormBuilder, Validators } from '@angular/forms';
import { UnidadeSaude } from '../entidades/UnidadeSaude';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unidade-form',
  templateUrl: './unidade-form.component.html',
  styleUrls: ['./unidade-form.component.css']
})
export class UnidadeFormComponent implements OnInit {
  constructor(
    public service: UnidadeService,
    public fb:FormBuilder,
    public router: Router
  ) { }

  public formGroup = this.fb.group({
    nome: ['', Validators.required],
    endereco: ['', Validators.required],
    telefone: ['', Validators.required]
  });

  ngOnInit(): void {
  }

  enviarForm(){
    const unidade:UnidadeSaude = {
      nome: this.nome.value,
      endereco: this.endereco.value,
      telefone: this.telefone.value
    }
    this.service.inserirUnidade(unidade).subscribe({
      next: unidade => {this.router.navigate(['../unidades'])}
    })
  }

  get nome(){
    return this.formGroup.get('nome');
  }
  get endereco(){
    return this.formGroup.get('endereco');
  }
  get telefone(){
    return this.formGroup.get('telefone')
  }

}
