import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineGraficoComponent } from './timeline-grafico.component';

describe('TimelineGraficoComponent', () => {
  let component: TimelineGraficoComponent;
  let fixture: ComponentFixture<TimelineGraficoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineGraficoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineGraficoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
