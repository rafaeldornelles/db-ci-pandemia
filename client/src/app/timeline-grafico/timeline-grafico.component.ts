import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Relatorio } from '../entidades/Relatorio';

@Component({
  selector: 'app-timeline-grafico',
  templateUrl: './timeline-grafico.component.html',
  styleUrls: ['./timeline-grafico.component.css']
})
export class TimelineGraficoComponent implements OnInit, OnChanges {
  @Input() graphData:Relatorio[] = [];
  @Input() seriesName:string = 'Casos';
  @Input() title:string = '';
  options = {
	series: [
	  {
		name: this.seriesName,
		data: this.graphDataTratado
	  }
	],
	chart: {
	  height: 350,
	  type: "area"
	},
	title: {
	  text: this.title
	},
	xaxis: {
	  type: 'datetime'
	},
	yaxis: {
        title: {
          text: "Bytes Received"
        }
      }
  };

  constructor() {
  }
  
  ngOnInit(): void {
  }

  ngOnChanges(){
	  this.options.series = [{
		  name:this.seriesName,
		  data:this.graphDataTratado
	  }];
	  this.options.title = {
		  text:this.title
	  }
  }

  get graphDataTratado(){
	  const returnValue = []
	  if(this.graphData.length>0){
		  const dataInicioString = this.graphData[0]._id;
		  const dataInicio = new Date(dataInicioString + 'T00:00:00.000Z');
		  const dataFim = new Date(Date.now())
		  for(let i = dataInicio.getTime(); i<dataFim.getTime(); i=i+1000*60*60*24){
			  const data = new Date(i);
			  returnValue.push({
				  x: data.toISOString().slice(0,10),
				  y: this.getCountPorData(this.graphData, data)
				});
			}
		}
	  return returnValue
  }

  getCountPorData(relatorio:Relatorio[], data:Date){
	const dataString = data.toISOString().slice(0,10);
	let count = 0
    relatorio.forEach(r=>{
		if(r._id == dataString){
			count = r.count
        	return
      	}
    });
    return count;    
  }
  

}
