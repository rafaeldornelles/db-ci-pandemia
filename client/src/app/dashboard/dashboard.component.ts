import { Component, OnInit } from '@angular/core';
import { Relatorio } from '../entidades/Relatorio';
import { AtendimentoService } from '../services/atendimento.service';
import { CasoService } from '../services/caso.service';
import { TesteService } from '../services/teste.service';
import { UnidadeService } from '../services/unidade.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  atendimentosRelatorio:Relatorio[]=[];
  casosRelatorio:Relatorio[]=[];
  testesRelatorio:Relatorio[]=[];
  totalCasosAtivos:number=0
  totalUnidadesCadastradas:number=0

  constructor(
    public atendimentoService:AtendimentoService,
    public casoService:CasoService,
    public testeService:TesteService,
    public unidadeService:UnidadeService
  ) { }

  ngOnInit(): void {
    this.atendimentoService.getRelatorio().subscribe({
      next: relatorio=>this.atendimentosRelatorio=relatorio
    });

    this.casoService.getRelatorio().subscribe({
      next: relatorio=>this.casosRelatorio=relatorio
    });
    this.casoService.getCasos().subscribe({
      next: casos=>this.totalCasosAtivos = casos.filter(caso=>caso.ativo).length
    });
    this.testeService.getRelatorioTestes().subscribe({
      next:relatorio=>this.testesRelatorio=relatorio
    });
    this.unidadeService.getUnidades().subscribe({
      next:unidades=>this.totalUnidadesCadastradas = unidades.length
    })

  }

  get percentualTestesPositivos(){
    return this.totalCasos/this.totalTestes*100
  }

  get totalCasos(){
    return this.casosRelatorio.reduce((acumulador, atual)=>{
      return acumulador+atual.count
    }, 0)
  }
  get totalAtendimentos(){
    return this.atendimentosRelatorio.reduce((acumulador, atual)=>{
      return acumulador+atual.count
    }, 0)
  }
  get totalTestes(){
    return this.testesRelatorio.reduce((acumulador, atual)=>{
      return acumulador+atual.count
    }, 0)
  }

  getCountPorData(relatorio:Relatorio[], data:Date){
    const dataString = data.toISOString().slice(0,10);
    relatorio.forEach(r=>{
      if(r._id == dataString){
        return r.count
      }
    })
    return 0;    
  }
}
