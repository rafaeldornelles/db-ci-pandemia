import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, concatMap } from 'rxjs/operators';
import { Atendimento } from '../entidades/Atendimento';
import { AtendimentoService } from '../services/atendimento.service';

@Component({
  selector: 'app-atendimento-detalhes',
  templateUrl: './atendimento-detalhes.component.html',
  styleUrls: ['./atendimento-detalhes.component.css']
})
export class AtendimentoDetalhesComponent implements OnInit {
  public atendimento:Atendimento
  constructor(
    public route:ActivatedRoute,
    public service: AtendimentoService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => params.get('id')),
      concatMap(id=>this.service.getAtendimentoById(id))
    ).subscribe({
      next: atendimento => this.atendimento = atendimento
    });
  }

  encerrarAtendimento(){
    this.service.encerrarAtendimento(this.atendimento._id).subscribe({
      next: atendimento => {window.location.reload()}
    })
  }

  get idadePaciente(){
    const now = new Date();
    const dataNascimento = new Date(this.atendimento.paciente.dataNascimento);
    let idade = now.getFullYear() - dataNascimento.getFullYear();
    if (now.getMonth() < dataNascimento.getMonth()){
      idade = idade - 1
    } else if(now.getMonth() == dataNascimento.getMonth()){
      if(now.getDate() < dataNascimento.getDate()){
        idade = idade - 1
      }
    }

    return idade;
  }

}
