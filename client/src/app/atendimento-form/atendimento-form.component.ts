import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UnidadeService } from '../services/unidade.service';
import { PacienteService } from '../services/paciente.service';
import { UnidadeSaude } from '../entidades/UnidadeSaude';
import { DatePipe } from '@angular/common';
import { Paciente } from '../entidades/Paciente';
import { Atendimento } from '../entidades/Atendimento';
import { AtendimentoService } from '../services/atendimento.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-atendimento-form',
  templateUrl: './atendimento-form.component.html',
  styleUrls: ['./atendimento-form.component.css']
})
export class AtendimentoFormComponent implements OnInit {
  @Input() public atendimento:Atendimento
  public unidades: UnidadeSaude[];
  public pacientes: Paciente[];
  public inicio:Date= this.atendimento? this.atendimento.horarioInicio : new Date();
  constructor(
    public fb: FormBuilder,
    public unidadeService: UnidadeService,
    public pacienteService: PacienteService,
    public datePipe: DatePipe,
    public atendimentoService: AtendimentoService,
    public router: Router
  ) { }

  public formGroup = this.fb.group({
    unidadeSaude:[this.atendimento? this.atendimento.unidadeSaude._id:"", Validators.required],
    pacienteId:[this.atendimento? this.atendimento.paciente._id : "", Validators.required],
    dia:[this.datePipe.transform(this.inicio, 'yyyy-MM-dd')],
    hora:[this.datePipe.transform(this.inicio, 'HH:mm')]
  })

  ngOnInit(): void {
    this.unidadeService.getUnidades().subscribe({
      next: unidades => this.unidades = unidades
    });

    this.pacienteService.getPacientes().subscribe({
      next: pacientes => this.pacientes = pacientes
    });
  }

  enviarForm(){
    let atendimento:Atendimento = {
      horarioInicio:new Date(`${this.dia.value}T${this.hora.value}`),
      ativo:true,
      paciente:this.pacienteId.value,
      unidadeSaude: this.unidadeSaude.value,
      testes: []
    }
    this.atendimentoService.insertAtendimento(atendimento).subscribe({
      next:atendimento => {this.router.navigate(['../atendimento'])},
      error:console.warn
    })
  }

  get unidadeSaude(){
    return this.formGroup.get('unidadeSaude');
  }

  get pacienteId(){
    return this.formGroup.get('pacienteId');
  }

  get dia(){
    return this.formGroup.get('dia');
  }

  get hora(){
    return this.formGroup.get('hora');
  }
}
