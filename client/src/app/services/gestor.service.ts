import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Gestor } from '../entidades/Gestor';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class GestorService {

  constructor(
    public http: HttpClient
  ) { }

  public baseUri = `${environment.BASE_URI}/gestores`;

  getGestores():Observable<Gestor[]>{
    return this.http.get<Gestor[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Gestor[]>([], 'Erro ao buscar gestores'))
    );
  }

  getGestoById(id:string):Observable<Gestor>{
    return this.http.get<Gestor>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Gestor>(null, 'Erro ao buscar gestor'))
    );
  }

  inserirGestor(gestor:Gestor):Observable<Gestor>{
    return this.http.post<Gestor>(`${this.baseUri}`, gestor).pipe(
      catchError(errorHandler<Gestor>(null, 'Erro ao inserir gestor'))
    );
  }

  atualizarGestor(id:string, gestor:Gestor):Observable<Gestor>{
    return this.http.put<Gestor>(`${this.baseUri}/${id}`, gestor).pipe(
      catchError(errorHandler<Gestor>(null, 'Erro ao atualizar gestor'))
    );
  }

  removerGestor(id:string):Observable<Gestor>{
    return this.http.delete<Gestor>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Gestor>(null, 'Erro ao remover gestor'))
    );
  }
}
