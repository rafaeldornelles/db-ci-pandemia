import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ProfissionalSaude } from '../entidades/ProfissionalSaude';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class ProfissionalService {

  constructor(
    public http: HttpClient
  ) { }

  public baseUri = `${environment.BASE_URI}/profissionais`

  getProfissionais():Observable<ProfissionalSaude[]>{
    return this.http.get<ProfissionalSaude[]>(this.baseUri).pipe(
      catchError(errorHandler<ProfissionalSaude[]>([], 'Erro ao buscar profissionais de saúde'))
    );
  }

  getProfissionaisPorUnidade(idUnidade:string):Observable<ProfissionalSaude[]>{
    return this.http.get<ProfissionalSaude[]>(`${this.baseUri}/${idUnidade}`).pipe(
      catchError(errorHandler<ProfissionalSaude[]>([], 'Erro ao buscar profissionais de saúde'))
    );
  }

  getProfissionalPorId(id:string):Observable<ProfissionalSaude>{
    return this.http.get<ProfissionalSaude>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<ProfissionalSaude>(null, 'Erro ao buscar profissional de saúde'))
    );
  }

  getProfissionalPorRegistro(registroProfissional:string):Observable<ProfissionalSaude>{
    return this.http.get<ProfissionalSaude>(`${this.baseUri}/reg/${registroProfissional}`).pipe(
      catchError(errorHandler<ProfissionalSaude>(null, 'Erro ao buscar profissional de saúde'))
    );
  }

  inserirProfissional(profissional:ProfissionalSaude):Observable<ProfissionalSaude>{
    return this.http.post<ProfissionalSaude>(`${this.baseUri}`, profissional).pipe(
      catchError(errorHandler<ProfissionalSaude>(null, 'Erro ao inserir profissional de saúde'))
    );
  }

  atualizarProfissional(id:string, profisional:ProfissionalSaude):Observable<ProfissionalSaude>{
    return this.http.put<ProfissionalSaude>(`${this.baseUri}/${id}`, profisional).pipe(
      catchError(errorHandler<ProfissionalSaude>(null, 'Erro ao atualizar profissional de saúde'))
    );
  }

  removerProfissional(id:string):Observable<ProfissionalSaude>{
    return this.http.delete<ProfissionalSaude>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<ProfissionalSaude>(null, 'Erro ao remover profissional de saúde'))
    );
  }
}
