import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Paciente } from '../entidades/Paciente';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  constructor(
    public http: HttpClient
  ) { }

  baseUri = `${environment.BASE_URI}/pacientes`;

  getPacientes():Observable<Paciente[]>{
    return this.http.get<Paciente[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Paciente[]>([], 'Erro ao buscar pacientes'))
    );
  }

  getPacientePorId(id:string):Observable<Paciente>{
    return this.http.get<Paciente>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Paciente>(null, 'Erro ao buscar paciente'))
    );
  }

  getPacientePorCpf(cpf:string):Observable<Paciente>{
    return this.http.get<Paciente>(`${this.baseUri}/cpf/${cpf}`).pipe(
      catchError(errorHandler<Paciente>(null, 'Erro ao buscar paciente'))
    );
  }

  inserirPaciente(paciente:Paciente):Observable<Paciente>{
    return this.http.post<Paciente>(`${this.baseUri}`, paciente).pipe(
      catchError(errorHandler<Paciente>(null, 'Erro ao inserir paciente'))
    );
  }

  atualizarPaciente(id:string, paciente:Paciente):Observable<Paciente>{
    return this.http.put<Paciente>(`${this.baseUri}/${id}`, paciente).pipe(
      catchError(errorHandler<Paciente>(null, 'Erro ao atualizar paciente'))
    );
  }

  deletarPaciente(id:string):Observable<Paciente>{
    return this.http.delete<Paciente>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Paciente>(null, 'Erro ao deletar paciente'))
    );
  }
}
