import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment"
import { Observable } from 'rxjs';
import { Atendimento } from "../entidades/Atendimento"
import { Relatorio } from "../entidades/Relatorio"
import { EtapaAtendimento } from '../entidades/EtapaAtendimento';
import { catchError } from 'rxjs/operators';
import {errorHandler} from './errorHandler'

@Injectable({
  providedIn: 'root'
})
export class AtendimentoService {
  constructor(
    public http: HttpClient
  ) { }

  baseUri = `${environment.BASE_URI}/atendimentos`;

  getAtendimentos():Observable<Atendimento[]>{
    return this.http.get<Atendimento[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Atendimento[]>([], 'Erro ao buscar atendimentos'))
    );
  }

  getRelatorio():Observable<Relatorio[]>{
    return this.http.get<Relatorio[]>(`${this.baseUri}/relatorio`).pipe(
      catchError(errorHandler<Relatorio[]>([], 'Erro ao buscar relatorio'))
    );
  }

  getAtendimentoById(id:string):Observable<Atendimento>{
    console.log(`${this.baseUri}/${id}`)
    return this.http.get<Atendimento>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Atendimento>(null, 'Erro ao buscar atendimento'))
    );
  }

  insertAtendimento(atendimento:Atendimento):Observable<Atendimento>{
    return this.http.post<Atendimento>(`${this.baseUri}`, atendimento).pipe(
      catchError(errorHandler<Atendimento>(null, 'Erro ao inserir atendimento'))
    );
  }

  insertEtapa(etapa:EtapaAtendimento):Observable<Atendimento>{
    return this.http.post<Atendimento>(`${this.baseUri}/etapa`, etapa).pipe(
      catchError(errorHandler<Atendimento>(null, 'Erro ao inserir etapa'))
    );
  }

  removerEtapa(idEtapa:string):Observable<EtapaAtendimento>{
    return this.http.delete<EtapaAtendimento>(`${this.baseUri}/etapa/${idEtapa}`).pipe(
      catchError(errorHandler<EtapaAtendimento>(null, 'Erro ao remover etapa'))
    );
  }

  encerrarAtendimento(idAtendimento:string):Observable<Atendimento>{
    return this.http.put<Atendimento>(`${this.baseUri}/${idAtendimento}/encerrar`, null).pipe(
      catchError(errorHandler<Atendimento>(null, 'Erro ao encerrar atendimento'))
    );
  }

  encerrarEtapa(idEtapa):Observable<EtapaAtendimento>{
    return this.http.put<EtapaAtendimento>(`${this.baseUri}/etapa/${idEtapa}/encerrar`, null).pipe(
      catchError(errorHandler<EtapaAtendimento>(null, 'erro ao encerrar etapa'))
    );
  }
}
