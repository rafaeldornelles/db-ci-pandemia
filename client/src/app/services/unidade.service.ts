import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UnidadeSaude } from '../entidades/UnidadeSaude';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class UnidadeService {

  constructor(
    public http:HttpClient
  ) { }

  baseUri = `${environment.BASE_URI}/unidades`;

  getUnidades():Observable<UnidadeSaude[]>{
    return this.http.get<UnidadeSaude[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<UnidadeSaude[]>([], 'Erro ao buscar unidade de saúde'))
    );
  }

  getUnidadePorId(id:string):Observable<UnidadeSaude>{
    return this.http.get<UnidadeSaude>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<UnidadeSaude>(null, 'Erro ao buscar unidade de saúde'))
    );
  }

  inserirUnidade(unidade:UnidadeSaude):Observable<UnidadeSaude>{
    return this.http.post<UnidadeSaude>(`${this.baseUri}`, unidade).pipe(
      catchError(errorHandler<UnidadeSaude>(null, 'Erro ao inserir unidade de saúde'))
    );
  }

  atualizarUnidade(id:string, unidade:UnidadeSaude):Observable<UnidadeSaude>{
    return this.http.put<UnidadeSaude>(`${this.baseUri}/${id}`, unidade).pipe(
      catchError(errorHandler<UnidadeSaude>(null, 'Erro ao atualizar unidade de saúde'))
    );
  }

  removerUnidade(id:string):Observable<UnidadeSaude>{
    return  this.http.delete<UnidadeSaude>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<UnidadeSaude>(null, 'Erro ao remover unidade de saúde'))
    );
  }
}
