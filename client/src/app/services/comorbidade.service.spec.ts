import { TestBed } from '@angular/core/testing';

import { ComorbidadeService } from './comorbidade.service';

describe('ComorbidadeService', () => {
  let service: ComorbidadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComorbidadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
