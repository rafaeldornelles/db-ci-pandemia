import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import{ Caso } from '../entidades/Caso'
import { Relatorio } from '../entidades/Relatorio';
import { DatePipe } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class CasoService {

  constructor(
    public http: HttpClient,
    public datePipe: DatePipe
  ) { }

  public baseUri = `${environment.BASE_URI}/casos`;

  getCasos():Observable<Caso[]>{
    return this.http.get<Caso[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Caso[]>([], 'Erro ao buscar casos'))
    );
  }

  getRelatorio():Observable<Relatorio[]>{
    return this.http.get<Relatorio[]>(`${this.baseUri}/relatorio`).pipe(
      catchError(errorHandler<Relatorio[]>([], 'Erro ao buscar relatorio'))
    );
  }

  getCasoPorId(id:string):Observable<Caso>{
    return this.http.get<Caso>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Caso>(null, 'Erro ao buscar caso'))
    );
  }

  getCasosByData(data:Date):Observable<Caso[]>{
    const dateString = this.datePipe.transform(data, "yyyy-MM-dd");
    return this.http.get<Caso[]>(`${this.baseUri}/data/${dateString}`).pipe(
      catchError(errorHandler<Caso[]>([], 'Erro ao buscar casos'))
    );
  }

  tornarInativo(idCaso:string):Observable<Caso>{
    return this.http.put<Caso>(`${this.baseUri}/encerrar/${idCaso}`, null).pipe(
      catchError(errorHandler<Caso>(null, 'Erro ao atualizar caso'))
    );
  }

  removerCaso(id:string):Observable<Caso>{
    return this.http.delete<Caso>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Caso>(null, 'Erro ao remover caso'))
    );
  }
}
