import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Teste } from '../entidades/Teste';
import { Relatorio } from '../entidades/Relatorio';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class TesteService {

  constructor(
    public http: HttpClient
  ) { }

  public baseUri = `${environment.BASE_URI}/testes`;

  getTestes():Observable<Teste[]>{
    return this.http.get<Teste[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Teste[]>([], 'Erro ao buscar testes'))
    );
  }

  getTestesPositivos():Observable<Teste[]>{
    return this.http.get<Teste[]>(`${this.baseUri}/positivos`).pipe(
      catchError(errorHandler<Teste[]>([], 'Erro ao buscar testes'))
    );
  }

  getRelatorioTestes():Observable<Relatorio[]>{
    return this.http.get<Relatorio[]>(`${this.baseUri}/relatorio`).pipe(
      catchError(errorHandler<Relatorio[]>([], 'Erro ao buscar relatorio'))
    );
  }

  getTestePorId(id:string):Observable<Teste>{
    return this.http.get<Teste>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Teste>(null, 'Erro ao buscar teste'))
    );
  }

  getTestesPorPaciente(idPaciente:string):Observable<Teste[]>{
    return this.http.get<Teste[]>(`${this.baseUri}/paciente/${idPaciente}`).pipe(
      catchError(errorHandler<Teste[]>([], 'Erro ao buscar testes'))
    );
  }

  getTestesPorUnidade(idUnidade:string):Observable<Teste[]>{
    return this.http.get<Teste[]>(`${this.baseUri}/unidade/${idUnidade}`).pipe(
      catchError(errorHandler<Teste[]>([], 'Erro ao buscar testes'))
    );
  }

  inserirTeste(teste:Teste):Observable<Teste>{
    return this.http.post<Teste>(`${this.baseUri}`, teste).pipe(
      catchError(errorHandler<Teste>(null, 'Erro ao inserir teste'))
    );
  }

  atualizarTeste(id:string, teste:Teste):Observable<Teste>{
    return this.http.put<Teste>(`${this.baseUri}/${id}`, teste).pipe(
      catchError(errorHandler<Teste>(null, 'Erro ao atualizar teste'))
    );
  }

  removerTeste(id:string):Observable<Teste>{
    return this.http.delete<Teste>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Teste>(null, 'Erro ao remover teste'))
    );
  }
}
