import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Usuario } from '../entidades/Usuario';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    public http: HttpClient
  ) { }

  public baseUri = `${environment.BASE_URI}/usuarios`;

  getUsuarios():Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Usuario[]>([], 'Erro ao buscar usuários'))
    );
  }

  getUsuarioPorEmail(email:string):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.baseUri}?${email}`).pipe(
      catchError(errorHandler<Usuario>(null, 'Erro ao buscar usuário'))
    );
  }

  getUsuarioPorId(id:string):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Usuario>(null, 'Erro ao buscar usuário'))
    );
  }

  removerUsuario(id:string):Observable<Usuario>{
    return this.http.delete<Usuario>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Usuario>(null, 'Erro ao remover usuário'))
    );
  }
}
