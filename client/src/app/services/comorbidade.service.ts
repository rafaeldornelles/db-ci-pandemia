import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Comorbidade } from '../entidades/Comorbidade';
import { catchError } from 'rxjs/operators';
import { errorHandler } from './errorHandler';

@Injectable({
  providedIn: 'root'
})
export class ComorbidadeService {

  constructor(
    public http: HttpClient
  ) { }

  public baseUri = `${environment.BASE_URI}/comorbidades`;

  getComorbidades():Observable<Comorbidade[]>{
    return this.http.get<Comorbidade[]>(`${this.baseUri}`).pipe(
      catchError(errorHandler<Comorbidade[]>([], 'Erro ao buscar comorbidades'))
    );
  }

  getComorbidadeById(id:string):Observable<Comorbidade>{
    return this.http.get<Comorbidade>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Comorbidade>(null, 'Erro ao buscar comorbidade'))
    );
  }

  inserirComorbidade(comorbidade:Comorbidade):Observable<Comorbidade>{
    return this.http.post<Comorbidade>(`${this.baseUri}`, comorbidade).pipe(
      catchError(errorHandler<Comorbidade>(null, 'Erro ao inserir comorbidade'))
    );
  }

  atualizarComorbidade(id:string, comorbidade:Comorbidade):Observable<Comorbidade>{
    return this.http.put<Comorbidade>(`${this.baseUri}/${id}`, comorbidade).pipe(
      catchError(errorHandler<Comorbidade>(null, 'Erro ao atualizar comorbidade'))
    );
  }

  removerComorbidade(id:string):Observable<Comorbidade>{
    return this.http.delete<Comorbidade>(`${this.baseUri}/${id}`).pipe(
      catchError(errorHandler<Comorbidade>(null, 'Erro ao deletar comorbidade'))
    );
  }
}
