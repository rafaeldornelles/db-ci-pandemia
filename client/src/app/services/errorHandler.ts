import {HttpErrorResponse} from '@angular/common/http'
import { Observable, throwError, of } from 'rxjs';

export function errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }