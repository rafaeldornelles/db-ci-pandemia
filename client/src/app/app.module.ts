import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AtendimentoComponent } from './atendimento/atendimento.component';
import { AtendimentoFormComponent } from './atendimento-form/atendimento-form.component';
import { DatePipe } from '@angular/common';
import { AtendimentoDetalhesComponent } from './atendimento-detalhes/atendimento-detalhes.component';
import { EtapaFormComponent } from './etapa-form/etapa-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { TimelineGraficoComponent } from './timeline-grafico/timeline-grafico.component';
import { TesteFormComponent } from './teste-form/teste-form.component';
import { TesteResultadoComponent } from './teste-resultado/teste-resultado.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { UnidadeFormComponent } from './unidade-form/unidade-form.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { PacienteFormComponent } from './paciente-form/paciente-form.component'

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    AtendimentoComponent,
    AtendimentoFormComponent,
    AtendimentoDetalhesComponent,
    EtapaFormComponent,
    DashboardComponent,
    TimelineGraficoComponent,
    TesteFormComponent,
    TesteResultadoComponent,
    UnidadesComponent,
    UnidadeFormComponent,
    PacientesComponent,
    PacienteFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgApexchartsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
