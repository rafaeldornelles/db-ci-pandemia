import { ProfissionalSaude } from "./ProfissionalSaude"
import { Atendimento } from "./Atendimento";

export interface EtapaAtendimento{
    _id?:string,
    descricao:string,
    horarioInicio:Date,
    horarioFim?:Date,
    profissional: ProfissionalSaude,
    atendimento: Atendimento
}