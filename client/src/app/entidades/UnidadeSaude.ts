export interface UnidadeSaude{
    _id?:string,
    nome:string,
    endereco:string,
    telefone:string,
    estatisticas?:{
        tempoMinimo:number,
        tempoMaximo:number,
        tempoMedio:number
    }
}