export interface Usuario{
    _id?: string,
    nome: string,
    email:string,
    telefone:string
}