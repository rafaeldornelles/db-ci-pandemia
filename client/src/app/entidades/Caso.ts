import { Paciente } from "./Paciente";
import { Teste } from "./Teste";

export interface Caso{
    _id?:string,
    paciente:Paciente,
    dataConfirmacao:Date,
    ativo:boolean,
    teste:Teste
}