import { UnidadeSaude } from "./UnidadeSaude";
import { Paciente } from "./Paciente";
import { Atendimento } from "./Atendimento";

export interface Teste{
    _id?:string,
    tipo:string,
    resultado?:boolean,
    unidade:UnidadeSaude,
    paciente:Paciente,
    data:Date,
    atendimento:Atendimento
}