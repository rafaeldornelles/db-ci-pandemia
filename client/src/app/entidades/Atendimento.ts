import { Paciente } from "./Paciente";
import { UnidadeSaude } from "./UnidadeSaude";
import { EtapaAtendimento } from "./EtapaAtendimento";
import { Teste } from "./Teste";

export interface Atendimento{
    _id?:string,
    horarioInicio: Date,
    horarioFim?: Date,
    ativo:boolean,
    paciente:Paciente,
    unidadeSaude: UnidadeSaude,
    etapas?: EtapaAtendimento,
    testes: Teste[]
}