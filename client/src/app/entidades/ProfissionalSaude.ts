import { UnidadeSaude } from "./UnidadeSaude";

export interface ProfissionalSaude{
    cargo:string,
    registroProfissional: string,
    unidadeSaude: UnidadeSaude
}