import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TesteResultadoComponent } from './teste-resultado.component';

describe('TesteResultadoComponent', () => {
  let component: TesteResultadoComponent;
  let fixture: ComponentFixture<TesteResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesteResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TesteResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
