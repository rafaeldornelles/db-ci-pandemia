import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TesteService } from '../services/teste.service';
import { Teste } from '../entidades/Teste';
import { map, concatMap } from 'rxjs/operators';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-teste-resultado',
  templateUrl: './teste-resultado.component.html',
  styleUrls: ['./teste-resultado.component.css']
})
export class TesteResultadoComponent implements OnInit {
  public teste: Teste
  public formGroup = this.fb.group({
    id: [{value:'', disabled:true}, Validators.required],
    tipo: [{value:'', disabled:true}, Validators.required],
    data: [{value:'', disabled:true}, Validators.required],
    resultado: ['', Validators.required]
  });

  constructor(
    public route:ActivatedRoute,
    public router: Router,
    public service: TesteService,
    public fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(map=>map.get('id')),
      concatMap(id=>this.service.getTestePorId(id))
    ).subscribe({
      next: teste=> {
        this.teste = teste;
        this.carregarForm();
      }
    });
  }

  carregarForm(){
    this.id.setValue(this.teste._id);
    this.tipo.setValue(this.teste.tipo);
    this.data.setValue(this.teste.data.toString().slice(0,10));
  }

  enviarForm(){
    this.teste.resultado = this.resultado.value;
    this.service.atualizarTeste(this.teste._id, this.teste).subscribe({
      next: teste=> {this.router.navigate(['../atendimento', this.teste.atendimento])}
    })
  }

  get id(){
    return this.formGroup.get('id')
  }
  get tipo(){
    return this.formGroup.get('tipo')
  }
  get data(){
    return this.formGroup.get('data')
  }
  get resultado(){
    return this.formGroup.get('resultado')
  }
}
