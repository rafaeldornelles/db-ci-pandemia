import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { map, concatMap, max } from 'rxjs/operators';
import { AtendimentoService } from '../services/atendimento.service';
import { Atendimento } from '../entidades/Atendimento';
import { FormBuilder, Validators } from '@angular/forms';
import { Teste } from '../entidades/Teste';
import { TesteService } from '../services/teste.service';

@Component({
  selector: 'app-teste-form',
  templateUrl: './teste-form.component.html',
  styleUrls: ['./teste-form.component.css']
})
export class TesteFormComponent implements OnInit {
  public now = new Date(Date.now()).toISOString().slice(0,10)
  public atendimento:Atendimento;
  public formGroup = this.fb.group({
    paciente: [{value:'', disabled:true}, Validators.required],
    atendimentoId: [{value:'', disabled:true}, Validators.required],
    unidade: [{value:'', disabled:true}, Validators.required],
    tipo: ['', Validators.required],
    data: [this.now, Validators.required]
  })
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public atendimentoService: AtendimentoService,
    public fb: FormBuilder,
    public testeService: TesteService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      concatMap(id=>this.atendimentoService.getAtendimentoById(id))
    ).subscribe({
      next: atendimento => {
        this.atendimento = atendimento;
        this.carregarForm();
      }
    });
  }

  carregarForm(){
    this.paciente.setValue(this.atendimento.paciente._id);
    this.atendimentoId.setValue(this.atendimento._id);
    this.unidade.setValue(this.atendimento.unidadeSaude._id)
  }

  enviarForm(){
    const teste:Teste = {
      paciente:this.paciente.value,
      tipo: this.tipo.value,
      atendimento: this.atendimentoId.value,
      unidade: this.unidade.value,
      data:this.data.value
    }
    this.testeService.inserirTeste(teste).subscribe({
      next: teste => this.router.navigate(['../atendimento', this.atendimento._id])
    })
  }

  get paciente(){
    return this.formGroup.get('paciente')
  }
  get atendimentoId(){
    return this.formGroup.get('atendimentoId')
  }
  get unidade(){
    return this.formGroup.get('unidade')
  }
  get tipo(){
    return this.formGroup.get('tipo')
  }
  get data(){
    return this.formGroup.get('data')
  }

}
