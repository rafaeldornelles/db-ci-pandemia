import { Component, OnInit } from '@angular/core';
import { UnidadeService } from '../services/unidade.service';
import { UnidadeSaude } from '../entidades/UnidadeSaude';

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit {
  public unidades:UnidadeSaude[]
  constructor(
    public service: UnidadeService
  ) { }

  ngOnInit(): void {
    this.service.getUnidades().subscribe({
      next: unidades => this.unidades = unidades
    })
  }

}
