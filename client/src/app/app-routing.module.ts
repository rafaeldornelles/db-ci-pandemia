import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtendimentoComponent } from './atendimento/atendimento.component';
import { AtendimentoFormComponent } from './atendimento-form/atendimento-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AtendimentoDetalhesComponent } from './atendimento-detalhes/atendimento-detalhes.component';
import { EtapaFormComponent } from './etapa-form/etapa-form.component';
import { TesteFormComponent } from './teste-form/teste-form.component';
import { TesteResultadoComponent } from './teste-resultado/teste-resultado.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { UnidadeFormComponent } from './unidade-form/unidade-form.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { PacienteFormComponent } from './paciente-form/paciente-form.component';

const routes: Routes = [
  {path:"", component:DashboardComponent},
  {path:"atendimento", component:AtendimentoComponent},
  {path:"atendimento/novo", component:AtendimentoFormComponent},
  {path:"atendimento/:id", component:AtendimentoDetalhesComponent},
  {path:"atendimento/:id/novoTeste", component:TesteFormComponent},
  {path:"atendimento/:id/novaEtapa", component:EtapaFormComponent},
  {path:"testes/:id/resultado", component:TesteResultadoComponent},
  {path:"unidades", component:UnidadesComponent},
  {path:"unidades/nova", component:UnidadeFormComponent},
  {path:"pacientes", component:PacientesComponent},
  {path:"pacientes/novo", component:PacienteFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
